(function () {

	if ('undefined' != typeof Validation) {

		Validation
			.addAllThese (
				[


					[
						'df.validate.firstName'
						,
						'Имя должно состоять только из букв.'
						,


						/**
						 * @param {String} value
						 * @returns {Boolean}
						 */
						function (value) {

							/** @type {Boolean} */
							var result =
								/^[a-zA-Zа-яА-ЯёЁ]*$/.test(value)
							;

							return result;

						}
					]



					,
					[
						'df.validate.lastName'
						,
						'Фамилия должна состоять только из буквы, дефиса («-») и пробелов'
						,


						/**
						 * @param {String} value
						 * @returns {Boolean}
						 */
						function (value) {

							/** @type {Boolean} */
							var result =
								/^[a-zA-Zа-яА-ЯёЁ\-\s]*$/.test(value)
							;

							return result;

						}
					]



					,
					[
						'df.validate.patronymic'
						,
						'Отчество должно состоять только из буквы, дефиса («-») и пробелов'
						,


						/**
						 * @param {String} value
						 * @returns {Boolean}
						 */
						function (value) {

							/** @type {Boolean} */
							var result =
								/^[a-zA-Zа-яА-ЯёЁ\-\s]*$/.test(value)
							;

							return result;

						}
					]


					,
					[
						'df.validate.postalCode'
						,
						'Данное поле должно содержать 6 цифр.'
						,

						/**
						 * @param {String} value
						 * @returns {Boolean}
						 */
						function (value) {

							/** @type {Boolean} */
							var result =
									Validation.get('IsEmpty').test(value)
								||
									/^[\d]{6}$/.test(value)
							;

							return result;

						}
					]




					,
					[
						'df.validate.phone'
						,
						'Укажите действующий телефонный номер'
						,


						/**
						 * @param {String} value
						 * @returns {Boolean}
						 */
						function (value) {

							/** @type {Boolean} */
							var result =
									Validation.get('IsEmpty').test(value)
								||
									/^[\d\-\(\)\+\s]{5,20}$/.test(value)
							;

							return result;

						}
					]




					,
					[
						'df.validate.city'
						,
						'Название города должно состоять только из букв, дефиса («-») и пробелов'
						,


						/**
						 * @param {String} value
						 * @returns {Boolean}
						 */
						function (value) {

							/** @type {Boolean} */
							var result =
									Validation.get('IsEmpty').test(value)
								||
									/^[a-zA-Zа-яА-ЯёЁ\-\s]*$/.test(value)
							;

							return result;

						}
					]


					,
					[
						'df.validate.region.text'
						,
						'Название области должно состоять только из букв, дефиса («-») и пробелов'
						,


						/**
						 * @param {String} value
						 * @returns {Boolean}
						 */
						function (value) {

							/** @type {Boolean} */
							var result =
									Validation.get('IsEmpty').test(value)
								||
									/^[a-zA-Zа-яА-ЯёЁ\-\s]+$/.test(value)
							;

							return result;

						}
					]



					,
					[
						'df.validate.urlKey'
						,
						'Уникальная часть веб-адреса должна начинаться с буквы или цифры, '
						+ 'затем допустимы буквы, цифры, символ пути («/»), дефис («-»), символ подчёркивания («_»). '
						+ 'В конце допустимо расширение, как у имён файлов: '
						+ 'точка («.») и после неё: буквы, цифры, дефис («-») и символ подчёркивания («_») .'
						,


						/**
						 * @param {String} value
						 * @returns {Boolean}
						 */
						function (value) {

							/** @type {Boolean} */
							var result =
									Validation.get('IsEmpty').test(value)
								||
									/^[a-zа-яА-ЯёЁ0-9][a-zа-яА-ЯёЁ0-9_\/-]+(\.[a-zа-яА-ЯёЁ0-9_-]+)?$/.test(value)
							;

							return result;

						}
					]


				]
			)
		;

	}

})();

