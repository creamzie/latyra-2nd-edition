(function ($) {

	'use strict';

	df.namespace ('df.server.license');


	df.server.license.Form = {

		construct: function (_config) { var _this = {

			init: function () {

				if (0 < this.getElement().size()) {

					this
						.subscribeChange (
							this.getInputLifeLong()
							,
							this.onChangeLifeLong
						)
					;


					this
						.subscribeChange (
							this.getInputPromo()
							,
							this.onChangePromo
						)
					;

				}

			}


			,
			/**
			 * @private
			 * @returns {jQuery} HTMLElement
			 */
			getElement: function () {
				return _config.element;
			}



			,
			/**
			 * @private
			 * @returns {jQuery} HTMLInputElement
			 */
			getInputLifeLong:
				function () {

					if ('undefined' == typeof this._inputLifeLong) {

						/** @type {jQuery} HTMLElement */
						var result =
							$('#lifelong', this.getElement())
						;

						this._inputLifeLong = result;
					}

					return this._inputLifeLong;
				}



			,
			/**
			 * @private
			 * @returns {jQuery} HTMLInputElement
			 */
			getInputPromo:
				function () {

					if ('undefined' == typeof this._inputPromo) {

						/** @type {jQuery} HTMLElement */
						var result =
							$('#promo', this.getElement())
						;

						this._inputPromo = result;
					}

					return this._inputPromo;
				}



			,
			/**
			 * @private
			 * @returns {jQuery} HTMLInputElement
			 */
			getRowDateExpiration:
				function () {

					if ('undefined' == typeof this._rowDateExpiration) {

						/** @type {jQuery} HTMLElement */
						var result =
							$('#date_expiration', this.getElement()).closest ('tr')
						;

						this._rowDateExpiration = result;
					}

					return this._rowDateExpiration;
				}



			,
			/**
			 * @private
			 * @returns {jQuery} HTMLInputElement
			 */
			getRowPaymentAmount:
				function () {

					if ('undefined' == typeof this._rowPaymenAmount) {

						/** @type {jQuery} HTMLElement */
						var result =
							$('#payment_amount', this.getElement()).closest ('tr')
						;

						this._rowPaymenAmount = result;
					}

					return this._rowPaymenAmount;
				}



			,
			/**
			 * @private
			 * @returns {jQuery} HTMLInputElement
			 */
			getRowPaymentCurrency:
				function () {

					if ('undefined' == typeof this._rowPaymentCurrency) {

						/** @type {jQuery} HTMLElement */
						var result =
							$('#payment_currency', this.getElement()).closest ('tr')
						;

						this._rowPaymentCurrency = result;
					}

					return this._rowPaymentCurrency;
				}



			,
			/**
			 * @private
			 * @returns {jQuery} HTMLInputElement
			 */
			getRowPaymentDate:
				function () {

					if ('undefined' == typeof this._rowPaymentDate) {

						/** @type {jQuery} HTMLElement */
						var result =
							$('#payment_date', this.getElement()).closest ('tr')
						;

						this._rowPaymentDate = result;
					}

					return this._rowPaymentDate;
				}



			,
			/**
			 * @private
			 * @returns {jQuery} HTMLInputElement
			 */
			getRowPaymentMethod:
				function () {

					if ('undefined' == typeof this._rowPaymenMethod) {

						/** @type {jQuery} HTMLElement */
						var result =
							$('#payment_method', this.getElement()).closest ('tr')
						;

						this._rowPaymenMethod = result;
					}

					return this._rowPaymenMethod;
				}



			,
			/**
			 * @private
			 * @returns {jQuery} HTMLInputElement
			 */
			getRowPromo:
				function () {

					if ('undefined' == typeof this._rowPromo) {

						/** @type {jQuery} HTMLElement */
						var result =
							this.getInputPromo().closest ('tr')
						;

						this._rowPromo = result;
					}

					return this._rowPromo;
				}



			,
			/**
			 * @private
			 * @returns {df.server.license.Form}
			 */
			onChangeLifeLong: function () {

				/** @type {Boolean} */
				var isLifeLong = (1 === parseInt (this.getInputLifeLong().val()));

				/** @type {Boolean} */
				var isPromo = (1 === parseInt (this.getInputPromo().val()));


				this.getRowDateExpiration()
					.toggle (
						!isLifeLong
					)
				;

				this.getRowPaymentAmount()
					.toggle (
						isLifeLong && !isPromo
					)
				;

				this.getRowPaymentCurrency()
					.toggle (
						isLifeLong && !isPromo
					)
				;

				this.getRowPaymentDate()
					.toggle (
						isLifeLong && !isPromo
					)
				;

				this.getRowPaymentMethod()
					.toggle (
						isLifeLong && !isPromo
					)
				;

				/**
				 * Этот код должен выполняться до аналогичного кода полей оплаты,
				 * потому что этот код влияет на видимость полей оплаты
				 */
				this.getRowPromo()
					.toggle (
						isLifeLong
					)
				;

				return this;
			}



			,
			/**
			 * @private
			 * @returns {df.server.license.Form}
			 */
			onChangePromo: function () {

				/** @type {Boolean} */
				var isLifeLong = (1 === parseInt (this.getInputLifeLong().val()));

				/** @type {Boolean} */
				var isPromo = (1 === parseInt (this.getInputPromo().val()));


				this.getRowPaymentAmount()
					.toggle (
						isLifeLong && !isPromo
					)
				;

				this.getRowPaymentCurrency()
					.toggle (
						isLifeLong && !isPromo
					)
				;

				this.getRowPaymentDate()
					.toggle (
						isLifeLong && !isPromo
					)
				;

				this.getRowPaymentMethod()
					.toggle (
						isLifeLong && !isPromo
					)
				;

				return this;
			}



			,
			/**
			 * @private
			 * @param {jQuery} HTMLInputElement
			 * @param {function}
			 * @returns {df.server.license.Form}
			 */
			subscribeChange: function ($input, $function) {

				$input.change(function() {
					$function.call (_this)
				});

				$function.call (this);

				return this;
			}




		}; _this.init (); return _this; }


	};





})(jQuery);