/**
 * Программный код,
 * который надо выполнить сразу после загрузки страницы
 */

(function ($) { $(function () {

	'use strict';

	df.server.license.Form
		.construct (
			{
				element: $('body.df-server-admin-license-edit #edit_form')
			}
		)
	;


}); })(jQuery);

