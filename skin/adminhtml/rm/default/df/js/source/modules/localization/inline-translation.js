(function ($) {

	'use strict';

	$(function () {

		/** @type {jQuery} HTMLSelectElement */
		var $locale = $('#interface_locale');

		/**
		 * Применяем заплатки перевода только для русскоязычного административного интерфейса
		 */
		if ('ru_RU' === $locale.val()) {

			$(window)
				.bind (
					'bundle.product.edit.bundle.option.selection'
					,
					function () {
						if ('undefined' !== typeof bSelection) {

							if ('undefined' !== typeof bSelection.templateBox) {

								/** @type {jQuery}  */
								var $template =
									$('<div/>')
										.append (
											$(bSelection.templateBox)
										)
								;


								(function () {
									/** @type {jQuery} HTMLElement[] */
									var $headers = $('th.type-price', $template);

									$headers.each (function () {
										/** @type {jQuery} HTMLElement*/
										var $this = $(this);

										if ('Цена' === $this.text()) {
											$this.text ('наценка');
										}
									});
								})();


								(function () {
									/** @type {jQuery} HTMLElement[] */
									var $headers = $('th', $template);

									$headers.each (function () {
										/** @type {jQuery} HTMLElement*/
										var $this = $(this);
										$this.text ($this.text().toLowerCase());
									});
								})();

								bSelection.templateBox = $template.html();
							}


							if ('undefined' !== typeof bSelection.templateRow) {
								bSelection.templateRow =
									bSelection.templateRow.replace ('конкретно указанный', 'абсолютная')
								;
							}
						}
					}
				)
			;
		}
	});

})(jQuery);
