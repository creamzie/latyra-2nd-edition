/**
 * Программный код,
 * который надо выполнить сразу после загрузки страницы
 */

(function ($) { $(function () {

	df.localization.verification.FileList
		.construct (
			{
				elementSelector: '.df .df-localization .df-verification .df-files .df-files-body'
			}
		)
	;


	df.localization.verification.FileWorkspace
		.construct (
			{
				elementSelector: '.df .df-localization .df-verification .df-fileDetails'
			}
		)
	;


}); })(jQuery);