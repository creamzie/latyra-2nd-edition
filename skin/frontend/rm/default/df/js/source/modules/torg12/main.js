(function ($) {

	$(function () {


		/** @type {HTMLElement} */
		var $container = $('.df-shipping-torg12');

		/** @type {Number} */
		var $containerWidthInMm = 275.4;


		/** @type {Number} */
		var dotsInMm = $container.width () / $containerWidthInMm;


		/** @type {Number} */
		var mmsInDot = $containerWidthInMm / $container.width ();


		/** @type {Number} */
		var pageHeightInMm = 210;

		var currentPageOrdering = 1;



		(function () {

			/** @type {Array} */
			var engines = ['webkit', 'opera', 'msie', 'mozilla'];


			$.each (engines, function (index, engine) {

				if ($.browser[engine]) {
					$container
						.addClass ('df-shipping-torg12-' + engine)
					;
				}

			});

		})();




		(function () {

			/** @type {jQuery} HTMLTableRowElement[] */
			var $rows = $('.df-orderItems tbody tr.df-orderItem');

			$rows.first().addClass ('df-first');

			$rows.last().addClass ('df-last');

		})();



		(function () {


			var $row3Input =
				$('.df-heading-left .df-row-3 .df-input .df-text', $container)
			;

			var $row5Input =
				$('.df-heading-left .df-row-5 .df-input .df-text', $container)
			;


			//$row3Input.alignBottom ();

			//$row5Input.alignBottom ();

		})();



//		(function () {
//
//			/** @type {jQuery} HTMLTableRowElement[] */
//			var $rows = $('.df-orderItems tbody tr.df-orderItem');
//
//			/** @type {?jQuery} HTMLTableRowElement */
//			var $prevRow = null;
//
//			$rows.each (function () {
//
//				/** @type {jQuery} HTMLTableRowElement */
//				var $row = $(this);
//
//				/** @type {Number} */
//				var rowBottom = $row.offset().top + $row.outerHeight(false);
//
//
//				if (rowBottom > pageHeightInMm * dotsInMm * currentPageOrdering) {
//
//					$row.addClass ('df-breakBefore');
//
//					$row.addClass ('df-firstRowOnPage');
//
//					if ($prevRow) {
//						$prevRow.addClass ('df-lastRowOnPage');
//					}
//
//					currentPageOrdering++;
//				}
//
//				$prevRow = $row;
//
//			});
//
//		})();


	});

})(jQuery);