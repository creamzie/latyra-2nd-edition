(function ($) {

	df.namespace ('df.customer.address');


	/**
	 * @param {jQuery} element
	 */
	df.customer.address.Field = {construct: function (_config) { var _this = {


		init: function () {

			$(function () {

			});
		}


		,
		/**
		 * @public
		 * @returns {jQuery} HTMLElement
		 */
		getElement: function () {
			return _config.element;
		}



		,
		/**
		 * @public
		 * @returns {Boolean}
		 */
		isExist: function () {

			if ('undefined' === typeof this._exist) {

				/**
				 * @type {Boolean}
				 */
				this._exist = (0 < this.getElement().size());
			}

			return this._exist;
		}



		,
		/**
		 * @public
		 * @returns {Boolean}
		 */
		isRequired: function () {

			if ('undefined' === typeof this._required) {

				/**
				 * @type {Boolean}
				 */
				this._required =
					0 < $('label.required', this.getElement().closest ('.field')).size()
				;
			}

			return this._required;
		}



		,
		/**
		 * @public
		 * @returns {String}
		 */
		getShortName: function () {

			if ('undefined' === typeof this._shortName) {

				/**
				 * @type {String}
				 */
				this._shortName = this.getElement().attr('name')
					.replace (/\w+\[(\w+)\]/, '$1')
				;
			}

			return this._shortName;
		}




	}; _this.init (); return _this; } };


})(jQuery);