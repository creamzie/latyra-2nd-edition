(function ($) {

	df.namespace ('df.checkout.ergonomic.method');


	df.checkout.ergonomic.method.Shipping = {

		construct: function (_config) { var _this = {


			init: function () {

				this.getShippingMethod().onComplete = this.onComplete.bindAsEventListener (this);

				this.handleSingleMethodCase ();

				this.handleSelection ();

			}



			,
			/**
			 * @public
			 * @returns {df.checkout.ergonomic.method.Shipping}
			 */
			handleSingleMethodCase: function () {

				/**
				 * @function
				 */
				var checker = function () {
					if (1 === $('input[name=shipping_method]', _this.getElement()).size ()) {

						_this.save ();

					}

				};


				$(window)
					.bind (
						df.checkout.Ergonomic.interfaceUpdated
						,
						/**
						 * @param {jQuery.Event} event
						 */
						function (event) {

							if (
								/**
								 *  Данное ограничение — не просто прихоть ради ускорения.
								 *  Без этого ограничения система зависнет,
								 *  потому что система постоянно будет выполнять метод save/
								 *  Причём возможны два вида бесконечных циклов:
								 *  1) прямой (shipping.save(),  shipping.save(), shipping.save())
								 *  2) косвенный (shipping.save(),  billing.save(), shipping.save())
								 */
									-1
								===
									['shippingMethod', 'paymentMethod'].indexOf(event.updateType)
							) {
								checker ();
							}
							else if (_this.needSave()) {
								_this.save ();
							}

						}
					)
				;

				checker ();

				return this;
			}




			,
			/**
			 * @public
			 * @returns {df.checkout.ergonomic.method.Shipping}
			 */
			handleSelection: function () {

				$(document.getElementById (_this.getShippingMethod().form))
					.change (
						function () {
							if (false === _this.getCheckout().loadWaiting) {
								_this.save ();
							}
							else {
								/**
								 * Вызывать save() пока бесполезно, потому что система занята.
								 * Поэтому вместо прямого вызова save планируем этот вызов на будущее.
								 */
								_this.needSave (true);
							}
						}
					)
				;

				return this;
			}






			,
			/**
			 * @public
			 * @returns {df.checkout.ergonomic.method.Shipping}
			 */
			save: function () {

				this.getValidator().dfValidateFilledFieldsOnly();

				if (this.getValidator().dfValidateSilent()) {
					this.needSave (false);
					this.getShippingMethod().save();
				}

				return this;
			}




			,
			/**
			 * @public
			 * @param {Object} transport
			 * @returns {df.checkout.ergonomic.method.Shipping}
			 */
			onComplete: function (transport) {

				this.getShippingMethod().resetLoadWaiting (transport);

				df.checkout.ergonomic.helperSingleton
					.updateSections (
						response
					)
				;

				$(window)
					.trigger (
						{
							/** @type {String} */
							type: df.checkout.Ergonomic.interfaceUpdated

							,
							/** @type {String} */
							updateType: 'shippingMethod'
						}
					)
				;

				return this;
			}


			,
			/** @function */
			_standardOnSaveHandler: shippingMethod.onSave





			,
			/**
			 * @private
			 * @returns {ShippingMethod}
			 */
			getShippingMethod: function () {
				return shippingMethod;
			}



			,
			/**
			 * @private
			 * @returns {Checkout}
			 */
			getCheckout: function () {
				return checkout;
			}




			,
			/**
			 * @public
			 * @param {Boolean}
			 * @returns {df.checkout.ergonomic.method.Shipping}
			 */
			needSave: function (value) {

				if ('undefined' !== typeof value) {
					this._needSave = value;
				}

				return this._needSave;
			}

			,
			/** @type {Boolean} */
			_needSave: false




			,
			/**
			 * @private
			 * @returns {jQuery} HTMLElement
			 */
			getElement: function () {
				return _config.element;
			}


			,
			/**
			 * @private
			 * @returns {Validation}
			 */
			getValidator: function () {

				if ('undefined' === typeof this._validator) {

					/**
					 * @type {Validation}
					 */
					this._validator =
						new Validation (
							_this.getShippingMethod().form
						)
					;
				}

				return this._validator;
			}


		}; _this.init (); return _this; }


	};





})(jQuery);