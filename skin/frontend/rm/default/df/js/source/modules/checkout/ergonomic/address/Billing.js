(function ($) {

	df.namespace ('df.checkout.ergonomic.address');


	/**
	 * @param {jQuery} element
	 */
	df.checkout.ergonomic.address.Billing = {

		shippingAddressIsTheSame: 'df.checkout.ergonomic.address.Billing.shippingAddressIsTheSame'

		,
		construct: function (_config) { var _this = {


			init: function () {

				this.getBilling().onComplete = this.onComplete.bindAsEventListener (this);

				this.handleShippingAddressHasNoFields ();

				this.listenForSelection ();

				this.handleUseForShipping ();

				this.addFakeRegionFieldsIfNeeded ();

			}




			,
			/**
			 * @public
			 * @returns {df.checkout.ergonomic.address.Shipping}
			 */
			save: function () {

				_this.needSave (false);


				var $regionAsText = this.getAddress().getFieldRegionText().getElement();

				var $regionAsSelect = this.getAddress().getFieldRegionSelect().getElement();


				var regionAsText = $regionAsText.get(0);

				var regionAsSelect = $regionAsSelect.get(0);

				if (regionAsText && regionAsSelect) {

					if ('none' === regionAsText.style.display) {

						regionAsText.value = $('option:selected', $regionAsSelect).text();

					}
				}


				_this.getBilling().save();

				return this;
			}




			,
			/**
			 * @public
			 * @returns {df.checkout.ergonomic.address.Billing}
			 */
			addFakeRegionFieldsIfNeeded: function () {

				df.checkout.ergonomic.helperSingleton.addFakeInputIfNeeded ('billing:region');
				df.checkout.ergonomic.helperSingleton.addFakeInputIfNeeded ('billing:region_id');

				return this;
			}





			,
			/**
			 * @public
			 * @returns {df.checkout.ergonomic.address.Billing}
			 */
			listenForSelection: function () {

				_this.getAddress().getFields()
					.change (
						function () {

							_this.handleSelection ();

						}
					)
				;

				if (document.getElementById ('billing-address-select')) {
					this.handleSelection ();
				}

				return this;
			}




			,
			/**
			 * @public
			 * @returns {df.checkout.ergonomic.address.Billing}
			 */
			handleSelection: function () {

				this.getValidator().dfValidateFilledFieldsOnly();

				if (this.getValidator().dfValidateSilent()) {

					if (false === this.getCheckout().loadWaiting) {
						this.save ();
					}
					else {
						/**
						 * Вызывать save() пока бесполезно, потому что система занята.
						 * Поэтому вместо прямого вызова save планируем этот вызов на будущее.
						 */
						this.needSave (true);
					}

				}
				return this;
			}




			,
			/**
			 * @public
			 * @returns {df.checkout.ergonomic.method.Shipping}
			 */
			handleShippingAddressHasNoFields: function () {

				$(window)
					.bind (
						df.checkout.ergonomic.address.Shipping.hasNoFields
						,
						/**
						 * @param {jQuery.Event} event
						 */
						function (event) {

							$(_this.getAddress().getField ('use_for_shipping').getElement())
								.closest ('li.control')
									.hide ()
							;

						}
					)
				;

				return this;
			}




			,
			/**
			 * @public
			 * @param {Object} transport
			 * @returns {df.checkout.ergonomic.address.Billing}
			 */
			onComplete: function (transport) {

				this.getBilling().resetLoadWaiting (transport);

				$(window)
					.trigger (
						{
							/** @type {String} */
							type: df.checkout.Ergonomic.interfaceUpdated

							,
							/** @type {String} */
							updateType: 'billingAddress'
						}
					)
				;

				return this;
			}





			,
			/**
			 * @public
			 * @returns {df.checkout.ergonomic.address.Billing}
			 */
			handleUseForShipping: function () {

				_this.getAddress().getField ('use_for_shipping').getElement()
					.change (
						function () {
							_this.onUseForShipping ();
						}
					)
				;

				_this.onUseForShipping ();

				return this;
			}




			,
			/**
			 * @public
			 * @returns {df.checkout.ergonomic.address.Billing}
			 */
			onUseForShipping: function () {

				/** @type {Boolean} */
				var shippingAddressIsTheSame = document.getElementById ('billing:use_for_shipping_yes').checked;

				$(window)
					.trigger (
						{
							/** @type {String} */
							type: df.checkout.ergonomic.address.Billing.shippingAddressIsTheSame

							,
							/** @type {df.checkout.ergonomic.address.Shipping} */
							value: shippingAddressIsTheSame
						}
					)
				;

				return this;
			}



			,
			/**
			 * @public
			 * @returns {df.customer.Address}
			 */
			getAddress: function () {

				if ('undefined' === typeof this._address) {

					/**
					 * @type {df.customer.Address}
					 */
					this._address =
						df.customer.Address
							.construct (
								{
									element: $('#co-billing-form', _this.getElement())
									,
									type: 'billing'
								}
							)
					;
				}

				return this._address;
			}




			,
			/**
			 * @private
			 * @returns {Billing}
			 */
			getBilling: function () {
				return billing;
			}




			,
			/**
			 * @private
			 * @returns {Checkout}
			 */
			getCheckout: function () {
				return checkout;
			}




			,
			/**
			 * @public
			 * @param {Boolean}
			 * @returns {df.checkout.ergonomic.address.Billing}
			 */
			needSave: function (value) {

				if ('undefined' !== typeof value) {
					this._needSave = value;
				}

				return this._needSave;
			}

			,
			/** @type {Boolean} */
			_needSave: false





			,
			/**
			 * @private
			 * @returns {jQuery} HTMLElement
			 */
			getElement: function () {
				return _config.element;
			}



			,
			/**
			 * @private
			 * @returns {Validation}
			 */
			getValidator: function () {

				if ('undefined' === typeof this._validator) {

					/**
					 * @type {Validation}
					 */
					this._validator =
						new Validation (
							_this.getBilling().form
						)
					;
				}

				return this._validator;
			}



		}; _this.init (); return _this; }


	};





})(jQuery);