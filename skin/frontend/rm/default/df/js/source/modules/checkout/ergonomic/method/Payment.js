(function ($) {

	df.namespace ('df.checkout.ergonomic.method');


	df.checkout.ergonomic.method.Payment = {

		construct: function (_config) { var _this = {


			init: function () {

				this.getPaymentMethod().onComplete = this.onComplete.bindAsEventListener (this);

				this.handleSingleMethodCase ();

				this.handleSelection ();

				this.subscribeToSectionUpdate();

				this.handleNoMethods();
			}




			,
			/**
			 * @public
			 * @returns {df.checkout.ergonomic.method.Payment}
			 */
			handleNoMethods: function () {

				/** @type {jQuery} HTMLDListElement */
				var $methodsContainer = $('#checkout-payment-method-load');

				if (0 === $('dt', $methodsContainer).size()) {

					var $parent = $methodsContainer.parent();

					/**
					 * Отсутствуют способы оплаты
					 */
					$methodsContainer
						.replaceWith (
							$('<div id="checkout-payment-method-load" />')
								.html (
"<p><span class='p'>Чтобы узнать доступные для Вашего заказа варианты оплаты — подробно заполните предыдущие блоки анкеты (платёжные реквизиты, адрес доставки, способ доставки).</span>"
+
"<span class='p'>Если Вы уже заполнили подробно предыдущие блоки анкеты, но варианты доставки так и не появились — пожалуйста, позвоните нам по телефону, и мы подберём индивидуальный вариант оплаты Вашего заказа.</span></p>"
								)
						)
					;
				}

				return this;
			}




			,
			/**
			 * @public
			 * @returns {df.checkout.ergonomic.method.Payment}
			 */
			subscribeToSectionUpdate: function () {

				$(window)
					.bind (
						df.checkout.Ergonomic.sectionUpdated
						,
						/**
						 * @param {jQuery.Event} event
						 */
						function (event) {
							if ('payment-method' === event.section) {
								_this.handleNoMethods();
							}
						}
					)
				;

				return this;
			}






			,
			/**
			 * @public
			 * @returns {df.checkout.ergonomic.method.Payment}
			 */
			handleSingleMethodCase: function () {

				/**
				 * @function
				 */
				var checker = function () {
					if (1 === $('input[name=payment\\[method\\]]', _this.getElement()).size ()) {

						_this.save ();

					}
				};


				$(window)
					.bind (
						df.checkout.Ergonomic.interfaceUpdated
						,
						/**
						 * @param {jQuery.Event} event
						 */
						function (event) {

							if (
								/**
								 *  Данное ограничение — не просто прихоть ради ускорения.
								 *  Без этого ограничения система зависнет,
								 *  потому что система постоянно будет выполнять метод save/
								 *  Причём возможны два вида бесконечных циклов:
								 *  1) прямой (shipping.save(),  shipping.save(), shipping.save())
								 *  2) косвенный (shipping.save(),  billing.save(), shipping.save())
								 */
									-1
								===
									['shippingMethod', 'paymentMethod'].indexOf(event.updateType)
							) {
								checker ();
							}
							else if (_this.needSave()) {
								_this.save ();
							}

						}
					)
				;

				checker ();

				return this;
			}



			,
			/**
			 * @public
			 * @returns {df.checkout.ergonomic.method.Payment}
			 */
			handleSelection: function () {

				$(document.getElementById (_this.getPaymentMethod().form))
					.change (
						function () {
							if (false === _this.getCheckout().loadWaiting) {
								_this.save ();
							}
							else {
								/**
								 * Вызывать save() пока бесполезно, потому что система занята.
								 * Поэтому вместо прямого вызова save планируем этот вызов на будущее.
								 */
								_this.needSave (true);
							}
						}
					)
				;

				return this;
			}




			,
			/**
			 * @public
			 * @returns {df.checkout.ergonomic.method.Payment}
			 */
			save: function () {

				this.getValidator().dfValidateFilledFieldsOnly();

				if (this.getValidator().dfValidateSilent()) {
					this.needSave (false);
					this.getPaymentMethod().save();
				}

				return this;
			}



			,
			/**
			 * @public
			 * @param {Object} transport
			 * @returns {df.checkout.ergonomic.method.Payment}
			 */
			onComplete: function (transport) {

				this.getPaymentMethod().resetLoadWaiting (transport);

				df.checkout.ergonomic.helperSingleton
					.updateSections (
						response
					)
				;

				$(window)
					.trigger (
						{
							/** @type {String} */
							type: df.checkout.Ergonomic.interfaceUpdated

							,
							/** @type {String} */
							updateType: 'paymentMethod'
						}
					)
				;

				return this;
			}


			,
			/** @function */
			_standardOnSaveHandler: payment.onSave




			,
			/**
			 * @private
			 * @returns {Payment}
			 */
			getPaymentMethod: function () {
				return payment;
			}




			,
			/**
			 * @private
			 * @returns {Checkout}
			 */
			getCheckout: function () {
				return checkout;
			}




			,
			/**
			 * @public
			 * @param {Boolean}
			 * @returns {df.checkout.ergonomic.method.Payment}
			 */
			needSave: function (value) {

				if ('undefined' !== typeof value) {
					this._needSave = value;
				}

				return this._needSave;
			}

			,
			/** @type {Boolean} */
			_needSave: false




			,
			/**
			 * @private
			 * @returns {jQuery} HTMLElement
			 */
			getElement: function () {
				return _config.element;
			}


			,
			/**
			 * @private
			 * @returns {Validation}
			 */
			getValidator: function () {

				if ('undefined' === typeof this._validator) {

					/**
					 * @type {Validation}
					 */
					this._validator =
						new Validation (
							_this.getPaymentMethod().form
						)
					;
				}

				return this._validator;
			}


		}; _this.init (); return _this; }


	};





})(jQuery);