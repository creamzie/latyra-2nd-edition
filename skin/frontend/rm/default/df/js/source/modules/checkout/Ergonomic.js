(function ($) {

	df.namespace ('df.checkout');


	/**
	 * @param {String} elementSelector
	 */
	df.checkout.Ergonomic = {

		interfaceUpdated: 'df.checkout.Ergonomic.interfaceUpdated'

		,
		loadWaiting: 'df.checkout.Ergonomic.loadWaiting'

		,
		sectionUpdated: 'df.checkout.Ergonomic.sectionUpdated'

		,
		construct: function (_config) { var _this = {


			init: function () {


				$(function () {

					if (0 < _this.getElement().size()) {

						/**
						 * Иногда нам требуется задать стили блоков вне текущего блока
						 * (в частности, блока .col-mail в теме Blanco).
						 */
						$('div.page').addClass ('df-checkout-ergonomic-page');

						checkout.reloadProgressBlock = function () {};


						_this.loadWaiting_adjust();



						$('a.df-login', _this.getElement ())
							.fancybox ({
								'titlePosition' : 'inside'
								,
								'transitionIn' : 'none'
								,
								'transitionOut' : 'none'
							})
						;


						/**
						 * @type {df.checkout.ergonomic.address.Billing}
						 */
						df.checkout.ergonomic.billingAddressSingleton =
							df.checkout.ergonomic.address.Billing
								.construct (
									{
										element: $('.df-block-address-billing', _this.getElement())
									}
								)
						;


						/**
						 * @type {df.checkout.ergonomic.address.Shipping}
						 */
						df.checkout.ergonomic.shippingAddressSingleton =
							df.checkout.ergonomic.address.Shipping
								.construct (
									{
										element: $('.df-block-address-shipping', _this.getElement())
									}
								)
						;


						/**
						 * @type {df.checkout.ergonomic.method.Shipping}
						 */
						df.checkout.ergonomic.shippingMethodSingleton =
							df.checkout.ergonomic.method.Shipping
								.construct (
									{
										element: $('.df-block-method-shipping', _this.getElement())
									}
								)
						;


						/**
						 * @type {df.checkout.ergonomic.method.Payment}
						 */
						df.checkout.ergonomic.paymentMethodSingleton =
							df.checkout.ergonomic.method.Payment
								.construct (
									{
										element: $('.df-block-method-payment', _this.getElement())
									}
								)
						;


						/**
						 * @type {df.checkout.ergonomic.Review}
						 */
						df.checkout.ergonomic.reviewSingleton =
							df.checkout.ergonomic.Review
								.construct (
									{
										element: $('.order-review', _this.getElement())
									}
								)
						;


					}

				});

			}



			,
			/**
			 * @private
			 * @returns {df.checkout.Ergonomic}
			 */
			loadWaiting_adjust: function () {

				/** @function */
				var originalFunction = this.getCheckout().setLoadWaiting;

				this.getCheckout().setLoadWaiting = function (step, keepDisabled) {

					originalFunction.call (_this.getCheckout(), step, keepDisabled);


					if (false !== step) {
						_this.loadWaiting_enable();
					}
					else {
						_this.loadWaiting_disable();
					}

				};

				return this;
			}



			,
			/**
			 * @private
			 * @returns {df.checkout.Ergonomic}
			 */
			loadWaiting_disable: function () {

				$.unblockUI();

				return this;

			}




			,
			/**
			 * @private
			 * @returns {df.checkout.Ergonomic}
			 */
			loadWaiting_enable: function () {

				$.blockUI({
					message: $('#df-loading-mask')
					,
					css: {
						border: 0
					}
					,
					overlayCSS:  {
						opacity: 0
					}
				});

				return this;
			}



			,
			/**
			 * @private
			 * @returns {Checkout}
			 */
			getCheckout: function () {
				return checkout;
			}


			,
			/**
			 * @private
			 * @returns {jQuery} HTMLElement
			 */
			getElement: function () {

				if ('undefined' == typeof this._element) {

					/** @type {jQuery} HTMLElement */
					var result =
						$(this.getElementSelector ())
					;

					this._element = result;

				}

				return this._element;
			}


			,
			/**
			 * @private
			 * @returns {String}
			 */
			getElementSelector: function () {
				return _config.elementSelector;
			}


		}; _this.init (); return _this; }


	};





})(jQuery);