(function () {

	/**
	 * Наша задача:
	 * 		[*]	назначить чётным подаркам класс df-even
	 * 		[*] назначить нечётным подаркам класс df-odd
	 * 		[*] назначить первому подарку класс df-first
	 * 		[*] назначить последнему подарку класс df-last
	 */
	document.observe("dom:loaded", function() {

		var odd = true;

		var $products = $$ (".df-promo-gift .df-gift-chooser .df-side li.df-product");


		/**
		 * Обратите внимание, что $products.first() может вернуть undefined,
		 * если массив $products пуст.
		 * Аналогично и $products.last().
		 *
		 * @link http://www.prototypejs.org/api/array/first
		 */
		if ($products.first ()) {
			$products.first ().addClassName ('df-first');
		}

		if ($products.last ()) {
			$products.last ().addClassName ('df-last');
		}

		$products
			.each (
				function (product) {
					$(product).addClassName (odd ? 'df-odd' : 'df-even');
					odd = !odd;
				}
			)
		;



	});

})();