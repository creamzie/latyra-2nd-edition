(function ($) {

	df.namespace ('df.tweaks');


	df.tweaks.ThemeDetector = {

		construct: function (_config) { var _this = {

			init: function () {

				var found = false;

				$('link').each (function () {

					if (found) {
						return false;
					}

					/** @type string */
					var href = $(this).attr ('href');

					$.each (_this.getDictionary(), function (indexInArray, valueOfElement) {
						if (-1 !== href.indexOf (indexInArray)) {
							$('body').addClass (valueOfElement);
							found = true;
							return false;
						}
					});


				});

			}


			,
			/**
			 * @returns {Object}
			 */
			getDictionary: function () {
				return {
					'/default/blanco/': 'df-theme-8theme-blanco'
					,
					'/default/gadget/': 'df-theme-8theme-gadget'
					,
					'/default/MAG080119/': 'df-theme-templatemela-beauty'
					,
					'/default/theme043k/': 'df-theme-templatemonster-34402'
				};
			}

		}; _this.init (); return _this; }


	};





})(jQuery);