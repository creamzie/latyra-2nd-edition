<?php
/**
*	Author		: 	Cybernetikz
*	Author Email:   info@cybernetikz.com
*	Blog		: 	http://blog.cybernetikz.com
*	Website		: 	http://www.cybernetikz.com
*/

class Cybernetikz_Cnslider_Block_Adminhtml_Cnslider_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Load Wysiwyg on demand and Prepare layout
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }
	
	/**
     * Prepare form action
     *
     * @return Cybernetikz_Cnslider_Block_Adminhtml_Cnslider_Edit_Form
     */
    protected function _prepareForm()
    {
        $model = Mage::helper('cybernetikz_cnslider')->getSliderItemInstance();
		
		$form = new Varien_Data_Form(array(
            'id'      => 'edit_form',
            'action'  => $this->getUrl('*/*/save'),
            'method'  => 'post',
            'enctype' => 'multipart/form-data'
        ));
		
		/**
         * Checking if user have permissions to save information
         */
        if (Mage::helper('cybernetikz_cnslider/admin')->isActionAllowed('save')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }

        //$form = new Varien_Data_Form();

        $form->setHtmlIdPrefix('cnslider_content_');

        $fieldset = $form->addFieldset('content_fieldset', array(
            'legend' => Mage::helper('cybernetikz_cnslider')->__('Slider Content'),
            'class'  => 'fieldset-medium'
        ));
		
		if ($model->getId()) {
            $fieldset->addField('slider_id', 'hidden', array(
                'name' => 'slider_id',
            ));
        }
		
				
		$fieldset->addField('name', 'text', array(
            'name'      => 'name',
            'title'     => Mage::helper('cybernetikz_cnslider')->__('Title'),
            'label'     => Mage::helper('cybernetikz_cnslider')->__('Title'),
            'maxlength' => '250',
            'required'  => true,
			'disabled'  => $isElementDisabled
        ));
 		
		/**
		 * Active or Deactive
		 */
    	$fieldset->addField('is_active', 'select', array(
            'label'     => Mage::helper('cybernetikz_cnslider')->__('Status'),
            'title'     => Mage::helper('cybernetikz_cnslider')->__('Status'),
            'name'      => 'is_active',
            'required'  => true,
            'options'   => array(
                '1' => Mage::helper('cybernetikz_cnslider')->__('Enabled'),
                '0' => Mage::helper('cybernetikz_cnslider')->__('Disabled'),
            ),
         
        ));
		
		/**
         * Store ID
         */
        if (!Mage::app()->isSingleStoreMode()) {
        	$fieldset->addField('store_id', 'select', array(
                'name'      => 'store_id',
                'label'     => Mage::helper('cybernetikz_cnslider')->__('Store View'),
                'title'     => Mage::helper('cybernetikz_cnslider')->__('Store View'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));
        }
        else {
            $fieldset->addField('store_id', 'hidden', array(
                'name'      => 'store_id',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
            $model->setStoreId(Mage::app()->getStore(true)->getId());
        }
		
		$this->_addElementTypes($fieldset);
		$fieldset->addField('slider_image', 'image', array(
            'name'      => 'slider_image',
            'title'     => Mage::helper('cybernetikz_cnslider')->__('Slider Image'),
            'label'     => Mage::helper('cybernetikz_cnslider')->__('Slider Image'),
            'required'  => true,
			'disabled'  => $isElementDisabled
        ));
		
		if(Mage::helper('cybernetikz_cnslider')->isEnableLink()){
				
			$fieldset->addField('link_url', 'text', array(
				'name'      => 'link_url',
				'title'     => Mage::helper('cybernetikz_cnslider')->__('Web URL'),
				'label'     => Mage::helper('cybernetikz_cnslider')->__('Web URL'),
				'maxlength' => '250',
				'required'  => false,
				'disabled'  => $isElementDisabled
			));
		}
		
		$fieldset->addField('sort_order', 'text', array(
            'name'      => 'sort_order',
            'title'     => Mage::helper('cybernetikz_cnslider')->__('Order'),
            'label'     => Mage::helper('cybernetikz_cnslider')->__('Order'),
			'class'		=> 'validate-number',
            'maxlength' => '20',
            'required'  => false,
			'disabled'  => $isElementDisabled
        ));
		
        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig(array(
            //'tab_id' => $this->getTabId()
        ));

       	if(Mage::helper('cybernetikz_cnslider')->isEnableContent()){	
				$contentField = $fieldset->addField('content', 'editor', array(
					'name'     => 'content',
					'style'     => 'height:30em; width:50em;',
					'title'     => Mage::helper('cybernetikz_cnslider')->__('Content'),
            		'label'     => Mage::helper('cybernetikz_cnslider')->__('Content'),
					'required' => true,
					'disabled' => $isElementDisabled,
					'config'   => $wysiwygConfig
				));
	
			// Setting custom renderer for content field to remove label column
			/*$renderer = $this->getLayout()->createBlock('adminhtml/widget_form_renderer_fieldset_element')
				->setTemplate('cms/page/edit/form/renderer/content.phtml');
			$contentField->setRenderer($renderer);*/
		}

        $form->setValues($model->getData());
		$form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
	
	/**
     * Retrieve predefined additional element types
     *
     * @return array
     */
     protected function _getAdditionalElementTypes()
     {
         return array(
            'image' => Mage::getConfig()->getBlockClassName('cybernetikz_cnslider/adminhtml_cnslider_edit_form_element_image')
        );
     }
}