<?php
/**
*	Author		: 	Cybernetikz
*	Author Email:   info@cybernetikz.com
*	Blog		: 	http://blog.cybernetikz.com
*	Website		: 	http://www.cybernetikz.com
*/


class Cybernetikz_Cnslider_Block_Adminhtml_Cnslider_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Initialize edit form container
     *
     */
    public function __construct()
    {
        $this->_objectId   = 'id';
        $this->_blockGroup = 'cybernetikz_cnslider';
        $this->_controller = 'adminhtml_cnslider';

        parent::__construct();

        if (Mage::helper('cybernetikz_cnslider/admin')->isActionAllowed('save')) {
            $this->_updateButton('save', 'label', Mage::helper('cybernetikz_cnslider')->__('Save Slider'));
            $this->_addButton('saveandcontinue', array(
                'label'   => Mage::helper('adminhtml')->__('Save and Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
            ), -100);
        } else {
            $this->_removeButton('save');
        }

        if (Mage::helper('cybernetikz_cnslider/admin')->isActionAllowed('delete')) {
            $this->_updateButton('delete', 'label', Mage::helper('cybernetikz_cnslider')->__('Delete Slider'));
        } else {
            $this->_removeButton('delete');
        }

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('page_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'page_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'page_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * Retrieve text for header element depending on loaded page
     *
     * @return string
     */
    public function getHeaderText()
    {
        $model = Mage::helper('cybernetikz_cnslider')->getSliderItemInstance();
		///print_r($model);
        if ($model->getId()) {
            return Mage::helper('cybernetikz_cnslider')->__("Edit Slider '%s'",
                 $this->escapeHtml($model->getName()));
        } else {
            return Mage::helper('cybernetikz_cnslider')->__('New Slider');
        }
    }
}