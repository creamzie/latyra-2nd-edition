<?php

class Df_Tweaks_Model_Handler_ProductBlock_Recent_Compared extends Df_Tweaks_Model_Handler_Remover {


	/**
	 * @override
	 * @return string
	 */
	protected function getBlockName () {
		return 'catalog.compare.sidebar';
	}




	/**
	 * @override
	 * @return Df_Tweaks_Model_Settings_Remove
	 */
	protected function getSettings() {
		return df_cfg()->tweaks()->recentlyComparedProducts();
	}




	/**
	 * @override
	 * @return bool
	 */
	protected function hasDataToShow () {

		/** @var Mage_Catalog_Helper_Product_Compare $helper */
		$helper = Mage::helper ('catalog/product_compare');
		df_assert ($helper instanceof Mage_Catalog_Helper_Product_Compare);

		/** @var bool $result */
		$result = (0 !== $helper->getItemCount());

		df_result_boolean ($result);

		return $result;
	}




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Tweaks_Model_Handler_ProductBlock_Recent_Compared';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}


