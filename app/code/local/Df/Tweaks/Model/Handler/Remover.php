<?php

abstract class Df_Tweaks_Model_Handler_Remover extends Df_Core_Model_Handler {

	/**
	 * @abstract
	 * @return bool
	 */
	abstract protected function hasDataToShow();


	/**
	 * @abstract
	 * @return bool
	 */
	abstract protected function getBlockName();


	/**
	 * @abstract
	 * @return Df_Tweaks_Model_Settings_Remove
	 */
	abstract protected function getSettings();




	/**
	 * Метод-обработчик события
	 *
	 * @override
	 * @return void
	 */
	public function handle () {
		if ($this->needToRemove()) {
			df()->layout()
				->removeBlock (
					$this->getBlockName()
				)
			;
		}
	}



	/**
	 * Объявляем метод заново, чтобы IDE знала настоящий тип объекта-события
	 *
	 * @override
	 * @return Df_Core_Model_Event_Controller_Action_Layout_GenerateBlocksAfter
	 */
	protected function getEvent () {

		/** @var Df_Core_Model_Event_Controller_Action_Layout_GenerateBlocksAfter $result  */
		$result = parent::getEvent();

		df_assert ($result instanceof Df_Core_Model_Event_Controller_Action_Layout_GenerateBlocksAfter);

		return $result;
	}




	/**
	 * Класс события (для валидации события)
	 *
	 * @override
	 * @return string
	 */
	protected function getEventClass () {

		/** @var string $result  */
		$result = Df_Core_Model_Event_Controller_Action_Layout_GenerateBlocksAfter::getClass();

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string[]
	 */
	private function getApplicableConfigValues () {

		/** @var string[] $result  */
		$result =
			array (
				$this->getSettings()->getRemoveFromAll()
				,
				$this->getConfigValueForCurrentPage()
			)
		;

		df_result_array ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	private function getConfigValueForCurrentPage () {

		if (!isset ($this->_configValueForCurrentPage)) {

			/** @var string $result */
			$result = $this->getSettings()->getRemoveFromAll();

			if (df_handle_presents(Df_Cms_Const::LAYOUT_HANDLE__INDEX_INDEX)) {
				$result =
					$this->getSettings()->getRemoveFromFrontpage()
				;
			}
			else if (df_handle_presents(Df_Catalog_Const::LAYOUT_HANDLE__CATEGORY_VIEW)) {
				$result =
					$this->getSettings()->getRemoveFromCatalogProductList()
				;
			}
			else if (df_handle_presents(Df_Catalog_Const::LAYOUT_HANDLE__PRODUCT_VIEW)) {
				$result =
					$this->getSettings()->getRemoveFromCatalogProductView()
				;
			}
			else if (df_handle_presents(Df_Customer_Const::LAYOUT_HANDLE__ACCOUNT)) {
				$result =
					$this->getSettings()->getRemoveFromAccount()
				;
			}
			else if (df_handle_presents(Df_CatalogSearch_Const::LAYOUT_HANDLE__RESULT_INDEX)) {
				$result =
					$this->getSettings()->getRemoveFromCatalogSearchResult()
				;
			}

			df_assert_string ($result);

			$this->_configValueForCurrentPage = $result;
		}

		df_result_string ($this->_configValueForCurrentPage);

		return $this->_configValueForCurrentPage;
	}


	/**
	* @var string
	*/
	private $_configValueForCurrentPage;




	/**
	 * @return string[]
	 */
	private function getInvisibleStates () {

		/** @var string[] $result  */
		$result =
			array (
				Df_Admin_Model_Config_Source_RemoveIfEmpty::VALUE__REMOVE
			)
		;

		if (!$this->hasDataToShow()) {
			$result []= Df_Admin_Model_Config_Source_RemoveIfEmpty::VALUE__REMOVE_IF_EMPTY;
		}

		df_result_array ($result);

		return $result;
	}





	/**
	 * @return bool
	 */
	private function needToRemove () {

		/** @var bool $result  */
		$result =
			!df_empty (
				array_intersect (
					$this->getApplicableConfigValues()
					,
					$this->getInvisibleStates()
				)
			)
		;

		df_result_boolean ($result);

		return $result;
	}





	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Tweaks_Model_Handler_Remover';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}

