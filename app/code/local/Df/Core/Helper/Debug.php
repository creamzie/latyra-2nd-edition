<?php

class Df_Core_Helper_Debug extends Mage_Core_Helper_Abstract {


	/**
	 * @param  int $levelsToSkip	<p>
	 * Позволяет при записи стека вызовов пропустить несколько последних вызовов функций,
	 * которые и так очевидны (например, вызов данной функции, вызов df_bt () и т.п.)
	 * </p>
	 *                          
	 * @param int $levelsToSkip [optional]
	 * @param array $bt [optional]
	 * @return Df_Core_Helper_Debug
	 */
	public function logCompactBacktrace ($levelsToSkip = 0, array $bt = array ()) {

		/** @var array $bt */
		$bt = !empty ($bt) ? $bt : debug_backtrace ();

		$bt =
			array_slice (
				$bt
				,
				$levelsToSkip
			)
		;

		/** @var array $compactBT */
		$compactBT = array ();

		/** @var int $traceLength */
		$traceLength = count ($bt);


		for ($traceIndex = 0; $traceIndex < $traceLength; $traceIndex++) {

			/** @var array $currentState */
			$currentState = df_a ($bt, $traceIndex);

			/** @var array|null $nextState */
			$nextState = df_a ($bt, 1 + $traceIndex);

			$compactBT []=
				array (
					'Файл' => df_a ($currentState, 'file')
					,
					'Строка' => df_a ($currentState, 'line')
					,
					'Субъект' =>
							is_null ($nextState)
						?
							Df_Core_Const::T_EMPTY
						:
							implode (
								'::'
								,
								df_clean (
									array (
										df_a ($nextState, 'class')
										,
										df_a ($nextState, 'function')
									)
								)
							)
					,
					'Объект' =>
						implode (
							'::'
							,
							df_clean (
								array (
									df_a ($currentState, 'class')
									,
									df_a ($currentState, 'function')
								)
							)
						)
				)
			;
		}

		Mage::log ($compactBT);

	    return $this;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Debug';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}