<?php



class Df_Core_Helper_Lib extends Df_Core_Helper_Lib_Abstract {

	/**
	 * @return array
	 */
	protected function getScriptsToInclude () {

		return
			array (

				implode (
					DS
					,
					array ('fp', 'array')
				)

				,
				implode (
					DS
					,
					array ('fp', 'validation')
				)


				,
				implode (
					DS
					,
					array ('fp', 'text')
				)


				,
				implode (
					DS
					,
					array ('fp', 'other')
				)


				,
				implode (
					DS
					,
					array ('fp', 'licensor')
				)

			)
		;
	}
}
