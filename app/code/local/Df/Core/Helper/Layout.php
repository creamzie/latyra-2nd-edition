<?php

class Df_Core_Helper_Layout extends Mage_Core_Helper_Abstract {


	/**
	 * @param string|Mage_Core_Block_Abstract $block
	 * @return Df_Core_Helper_Layout
	 */
	public function removeBlock ($block) {

		/**
		 * Что интересно, мы можем эффективно (быстро)
		 * удалять блоки и по их типу.
		 *
		 * Layout, при создании блока, устанавливает блоку его тип в формате Magento:
		 * $block->setType($type);
		 *
		 * Поэтому, по событию controller_action_layout_generate_blocks_after
		 * мы можем сформировать единократно карту тип блока => блок,
		 * и затем быстро находить блоки по их типу.
		 */

		if (is_string ($block)) {
			$block = df_mage()->core()->layout()->getBlock ($block);
		}

		if ($block instanceof Mage_Core_Block_Abstract) {

			df_mage()->core()->layout()
				->unsetBlock (
					$block->getNameInLayout()
				)
			;


			/** @var Mage_Core_Block_Abstract|null $parent */
			$parent = $block->getParentBlock();

			/**
			 * Как ни странно — родительского блока может не быть.
			 * Такое происходит, когда в файлах layout уже вызвали unsetChild.
			 * Непонятно, почему после unset блок всё-таки доходит сюда.
			 */
			if ($parent) {

				$parent
					->unsetChild (
						$block->getBlockAlias()
					)
				;



				/**
				 * Заплатка для Magento CE 1.6.2.0 и более ранних версий
				 * Метод unsetChild в этих версиях дефектен
				 */
				if (df_magento_version('1.6.2.0', '<=')) {
					$parent
						->unsetChild (
							$block->getNameInLayout()
						)
					;
				}



				/**
				 * После unsetChild надо обязательно вызывать sortChildren,
				 * иначе блоки дублируются: http://magento-forum.ru/topic/1491/
				 *
				 * Дублирование происходит, потому что unsetChild приводит к вызову PHP unset,
				 * а unset не обновляет натуральные индексы массива:
				 * 'It should be noted that unset() will keep indexes untouched,
				 * which is what you'd expect when using string indexes (array as hashtable),
				 * but can be quite surprising when dealing with integer indexed arrays:'
				 *
				 * Это приводить потом к неправильному поведению sortChildren
				 * (и array_splice внутри него)
				 * при отображении страницы
				 */

				if (method_exists($parent, 'sortChildren')) {
					/**
					 * В Magento 1.4 - 1.5 метод Mage_Core_Block_Abstract::sortChildren отсутствует
					 */
					$parent->sortChildren();
				}
			}
		}

		return $this;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Layout';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


