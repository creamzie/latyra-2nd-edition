<?php

class Df_Core_Helper_Mage extends Mage_Core_Helper_Abstract {



	/**
	 * @return Df_Core_Helper_Mage_Admin
	 */
	public function admin () {

		/** @var Df_Core_Helper_Mage_Admin $result  */
		$result = Mage::helper (Df_Core_Helper_Mage_Admin::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Core_Helper_Mage_Admin);

		return $result;
	}



	/**
	 * @return Mage_Admin_Helper_Data
	 */
	public function adminHelper () {

		/** @var Mage_Admin_Helper_Data $result  */
		$result = Mage::helper ('admin');

		df_assert ($result instanceof Mage_Admin_Helper_Data);

		return $result;
	}



	/**
	 * @return Df_Core_Helper_Mage_Adminhtml
	 */
	public function adminhtml () {

		/** @var Df_Core_Helper_Mage_Adminhtml $result  */
		$result = Mage::helper (Df_Core_Helper_Mage_Adminhtml::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Core_Helper_Mage_Adminhtml);

		return $result;
	}



	/**
	 * @return Mage_Adminhtml_Helper_Data
	 */
	public function adminhtmlHelper () {

		/** @var Mage_Adminhtml_Helper_Data $result  */
		$result = Mage::helper ('adminhtml');

		df_assert ($result instanceof Mage_Adminhtml_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_AdminNotification_Helper_Data
	 */
	public function adminNotificationHelper () {

		/** @var Mage_AdminNotification_Helper_Data $result  */
		$result = Mage::helper ('adminnotification');

		df_assert ($result instanceof Mage_AdminNotification_Helper_Data);

		return $result;
	}




	/**
	 * @return Mage_Api_Helper_Data
	 */
	public function apiHelper () {

		/** @var Mage_Api_Helper_Data $result  */
		$result = Mage::helper ('api');

		df_assert ($result instanceof Mage_Api_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Authorizenet_Helper_Data
	 */
	public function authorizenetHelper () {

		/** @var Mage_Authorizenet_Helper_Data $result  */
		$result = Mage::helper ('authorizenet');

		df_assert ($result instanceof Mage_Authorizenet_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Backup_Helper_Data
	 */
	public function backupHelper () {

		/** @var Mage_Backup_Helper_Data $result  */
		$result = Mage::helper ('backup');

		df_assert ($result instanceof Mage_Backup_Helper_Data);

		return $result;
	}



	
	
	/**
	 * @return Mage_Bundle_Helper_Data
	 */
	public function bundleHelper () {

		/** @var Mage_Bundle_Helper_Data $result  */
		$result = Mage::helper ('bundle');

		df_assert ($result instanceof Mage_Bundle_Helper_Data);

		return $result;
	}



	/**
	 * @return Df_Core_Helper_Mage_Catalog
	 */
	public function catalog () {

		/** @var Df_Core_Helper_Mage_Catalog $result  */
		$result = Mage::helper (Df_Core_Helper_Mage_Catalog::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Core_Helper_Mage_Catalog);

		return $result;
	}




	/**
	 * @return Mage_Catalog_Helper_Data
	 */
	public function catalogHelper () {

		/** @var Mage_Catalog_Helper_Data $result  */
		$result = Mage::helper ('catalog');

		df_assert ($result instanceof Mage_Catalog_Helper_Data);

		return $result;
	}




	/**
	 * @return Mage_CatalogInventory_Helper_Data
	 */
	public function catalogInventoryHelper () {

		/** @var Mage_CatalogInventory_Helper_Data $result  */
		$result = Mage::helper ('cataloginventory');

		df_assert ($result instanceof Mage_CatalogInventory_Helper_Data);

		return $result;
	}




	/**
	 * @return Mage_Catalog_Helper_Image
	 */
	public function catalogImageHelper () {

		/** @var Mage_Catalog_Helper_Image $result  */
		$result = Mage::helper ('catalog/image');

		df_assert ($result instanceof Mage_Catalog_Helper_Image);

		return $result;
	}
	
	


	
	/**
	 * @return Mage_CatalogRule_Helper_Data
	 */
	public function catalogRuleHelper () {

		/** @var Mage_CatalogRule_Helper_Data $result  */
		$result = Mage::helper ('catalogrule');

		df_assert ($result instanceof Mage_CatalogRule_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_CatalogSearch_Helper_Data
	 */
	public function catalogSearchHelper () {

		/** @var Mage_CatalogSearch_Helper_Data $result  */
		$result = Mage::helper ('catalogsearch');

		df_assert ($result instanceof Mage_CatalogSearch_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Centinel_Helper_Data
	 */
	public function centinelHelper () {

		/** @var Mage_Centinel_Helper_Data $result  */
		$result = Mage::helper ('centinel');

		df_assert ($result instanceof Mage_Centinel_Helper_Data);

		return $result;
	}




	/**
	 * @return Df_Core_Helper_Mage_Checkout
	 */
	public function checkout () {

		/** @var Df_Core_Helper_Mage_Checkout $result  */
		$result = Mage::helper (Df_Core_Helper_Mage_Checkout::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Core_Helper_Mage_Checkout);

		return $result;
	}



	/**
	 * @return Mage_Checkout_Helper_Data
	 */
	public function checkoutHelper () {

		/** @var Mage_Checkout_Helper_Data $result  */
		$result = Mage::helper ('checkout');

		df_assert ($result instanceof Mage_Checkout_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Cms_Helper_Data
	 */
	public function cmsHelper () {

		/** @var Mage_Cms_Helper_Data $result  */
		$result = Mage::helper ('cms');

		df_assert ($result instanceof Mage_Cms_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Cms_Helper_Wysiwyg_Images
	 */
	public function cmsWysiwygImagesHelper () {

		/** @var Mage_Cms_Helper_Wysiwyg_Images $result  */
		$result = Mage::helper ('cms/wysiwyg_images');

		df_assert ($result instanceof Mage_Cms_Helper_Wysiwyg_Images);

		return $result;
	}




	/**
	 * @return Mage_Compiler_Helper_Data
	 */
	public function compilerHelper () {

		/** @var Mage_Compiler_Helper_Data $result  */
		$result = Mage::helper ('compiler');

		df_assert ($result instanceof Mage_Compiler_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Connect_Helper_Data
	 */
	public function connectHelper () {

		/** @var Mage_Connect_Helper_Data $result  */
		$result = Mage::helper ('connect');

		df_assert ($result instanceof Mage_Connect_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Contacts_Helper_Data
	 */
	public function contactsHelper () {

		/** @var Mage_Contacts_Helper_Data $result  */
		$result = Mage::helper ('contacts');

		df_assert ($result instanceof Mage_Contacts_Helper_Data);

		return $result;
	}




	/**
	 * @return Df_Core_Helper_Mage_Core
	 */
	public function core () {

		/** @var Df_Core_Helper_Mage_Core $result  */
		$result = Mage::helper (Df_Core_Helper_Mage_Core::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Core_Helper_Mage_Core);

		return $result;
	}



	/**
	 * @return Mage_Core_Helper_Data
	 */
	public function coreHelper () {

		/** @var Mage_Core_Helper_Data $result  */
		$result = Mage::helper ('core');

		df_assert ($result instanceof Mage_Core_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Cron_Helper_Data
	 */
	public function cronHelper () {

		/** @var Mage_Cron_Helper_Data $result  */
		$result = Mage::helper ('cron');

		df_assert ($result instanceof Mage_Cron_Helper_Data);

		return $result;
	}



	/**
	 * @return Df_Core_Helper_Mage_Customer
	 */
	public function customer () {

		/** @var Df_Core_Helper_Mage_Customer $result  */
		$result = Mage::helper (Df_Core_Helper_Mage_Customer::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Core_Helper_Mage_Customer);

		return $result;
	}



	/**
	 * @return Mage_Customer_Helper_Data
	 */
	public function customerHelper () {

		/** @var Mage_Customer_Helper_Data $result  */
		$result = Mage::helper ('customer');

		df_assert ($result instanceof Mage_Customer_Helper_Data);

		return $result;
	}




	/**
	 * @return Df_Core_Helper_Mage_Dataflow
	 */
	public function dataflow () {

		/** @var Df_Core_Helper_Mage_Dataflow $result  */
		$result = Mage::helper (Df_Core_Helper_Mage_Dataflow::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Core_Helper_Mage_Dataflow);

		return $result;
	}



	/**
	 * @return Mage_Dataflow_Helper_Data
	 */
	public function dataflowHelper () {

		/** @var Mage_Dataflow_Helper_Data $result  */
		$result = Mage::helper ('dataflow');

		df_assert ($result instanceof Mage_Dataflow_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Directory_Helper_Data
	 */
	public function directoryHelper () {

		/** @var Mage_Directory_Helper_Data $result  */
		$result = Mage::helper ('directory');

		df_assert ($result instanceof Mage_Directory_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Downloadable_Helper_Data
	 */
	public function downloadableHelper () {

		/** @var Mage_Downloadable_Helper_Data $result  */
		$result = Mage::helper ('downloadable');

		df_assert ($result instanceof Mage_Downloadable_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Eav_Helper_Data
	 */
	public function eavHelper () {

		/** @var Mage_Eav_Helper_Data $result  */
		$result = Mage::helper ('eav');

		df_assert ($result instanceof Mage_Eav_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_GiftMessage_Helper_Data
	 */
	public function giftMessageHelper () {

		/** @var Mage_GiftMessage_Helper_Data $result  */
		$result = Mage::helper ('giftmessage');

		df_assert ($result instanceof Mage_GiftMessage_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_GoogleAnalytics_Helper_Data
	 */
	public function googleAnalyticsHelper () {

		/** @var Mage_GoogleAnalytics_Helper_Data $result  */
		$result = Mage::helper ('googleanalytics');

		df_assert ($result instanceof Mage_GoogleAnalytics_Helper_Data);

		return $result;
	}




	/**
	 * @return Mage_GoogleCheckout_Helper_Data
	 */
	public function googleCheckoutHelper () {

		/** @var Mage_GoogleCheckout_Helper_Data $result  */
		$result = Mage::helper ('googlecheckout');

		df_assert ($result instanceof Mage_GoogleCheckout_Helper_Data);

		return $result;
	}



	/**
	 * @return Df_Core_Helper_Mage_Helper
	 */
	public function helper () {

		/** @var Df_Core_Helper_Mage_Helper $result  */
		$result = Mage::helper (Df_Core_Helper_Mage_Helper::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Core_Helper_Mage_Helper);

		return $result;
	}



	/**
	 * @return Mage_ImportExport_Helper_Data
	 */
	public function importExportHelper () {

		/** @var Mage_ImportExport_Helper_Data $result  */
		$result = Mage::helper ('importexport');

		df_assert ($result instanceof Mage_ImportExport_Helper_Data);

		return $result;
	}



	/**
	 * @return Df_Core_Helper_Mage_Index
	 */
	public function index () {

		/** @var Df_Core_Helper_Mage_Index $result  */
		$result = Mage::helper (Df_Core_Helper_Mage_Index::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Core_Helper_Mage_Index);

		return $result;
	}



	/**
	 * @return Mage_Index_Helper_Data
	 */
	public function indexHelper () {

		/** @var Mage_Index_Helper_Data $result  */
		$result = Mage::helper ('index');

		df_assert ($result instanceof Mage_Index_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Install_Helper_Data
	 */
	public function installHelper () {

		/** @var Mage_Install_Helper_Data $result  */
		$result = Mage::helper ('install');

		df_assert ($result instanceof Mage_Install_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Log_Helper_Data
	 */
	public function logHelper () {

		/** @var Mage_Log_Helper_Data $result  */
		$result = Mage::helper ('log');

		df_assert ($result instanceof Mage_Log_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Media_Helper_Data
	 */
	public function mediaHelper () {

		/** @var Mage_Media_Helper_Data $result  */
		$result = Mage::helper ('media');

		df_assert ($result instanceof Mage_Media_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Newsletter_Helper_Data
	 */
	public function newsletterHelper () {

		/** @var Mage_Newsletter_Helper_Data $result  */
		$result = Mage::helper ('newsletter');

		df_assert ($result instanceof Mage_Newsletter_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Page_Helper_Data
	 */
	public function pageHelper () {

		/** @var Mage_Page_Helper_Data $result  */
		$result = Mage::helper ('page');

		df_assert ($result instanceof Mage_Page_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_PageCache_Helper_Data
	 */
	public function pageCacheHelper () {

		/** @var Mage_PageCache_Helper_Data $result  */
		$result = Mage::helper ('pagecache');

		df_assert ($result instanceof Mage_PageCache_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Paygate_Helper_Data
	 */
	public function paygateHelper () {

		/** @var Mage_Paygate_Helper_Data $result  */
		$result = Mage::helper ('paygate');

		df_assert ($result instanceof Mage_Paygate_Helper_Data);

		return $result;
	}




	/**
	 * @return Mage_Payment_Helper_Data
	 */
	public function paymentHelper () {

		/** @var Mage_Payment_Helper_Data $result  */
		$result = Mage::helper ('payment');

		df_assert ($result instanceof Mage_Payment_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Paypal_Helper_Data
	 */
	public function paypalHelper () {

		/** @var Mage_Paypal_Helper_Data $result  */
		$result = Mage::helper ('paypal');

		df_assert ($result instanceof Mage_Paypal_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_PaypalUk_Helper_Data
	 */
	public function paypalUkHelper () {

		/** @var Mage_PaypalUk_Helper_Data $result  */
		$result = Mage::helper ('paypaluk');

		df_assert ($result instanceof Mage_PaypalUk_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Persistent_Helper_Data
	 */
	public function persistentHelper () {

		/** @var Mage_Persistent_Helper_Data $result  */
		$result = Mage::helper ('persistent');

		df_assert ($result instanceof Mage_Persistent_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Poll_Helper_Data
	 */
	public function pollHelper () {

		/** @var Mage_Poll_Helper_Data $result  */
		$result = Mage::helper ('poll');

		df_assert ($result instanceof Mage_Poll_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_ProductAlert_Helper_Data
	 */
	public function productAlertHelper () {

		/** @var Mage_ProductAlert_Helper_Data $result  */
		$result = Mage::helper ('productalert');

		df_assert ($result instanceof Mage_ProductAlert_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Rating_Helper_Data
	 */
	public function ratingHelper () {

		/** @var Mage_Rating_Helper_Data $result  */
		$result = Mage::helper ('rating');

		df_assert ($result instanceof Mage_Rating_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Reports_Helper_Data
	 */
	public function reportsHelper () {

		/** @var Mage_Reports_Helper_Data $result  */
		$result = Mage::helper ('reports');

		df_assert ($result instanceof Mage_Reports_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Review_Helper_Data
	 */
	public function reviewHelper () {

		/** @var Mage_Review_Helper_Data $result  */
		$result = Mage::helper ('review');

		df_assert ($result instanceof Mage_Review_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Rss_Helper_Data
	 */
	public function rssHelper () {

		/** @var Mage_Rss_Helper_Data $result  */
		$result = Mage::helper ('rss');

		df_assert ($result instanceof Mage_Rss_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Rule_Helper_Data
	 */
	public function ruleHelper () {

		/** @var Mage_Rule_Helper_Data $result  */
		$result = Mage::helper ('rule');

		df_assert ($result instanceof Mage_Rule_Helper_Data);

		return $result;
	}




	/**
	 * @return Mage_Sales_Helper_Data
	 */
	public function salesHelper () {

		/** @var Mage_Sales_Helper_Data $result  */
		$result = Mage::helper ('sales');

		df_assert ($result instanceof Mage_Sales_Helper_Data);

		return $result;
	}




	/**
	 * @return Mage_SalesRule_Helper_Data
	 */
	public function salesRuleHelper () {

		/** @var Mage_SalesRule_Helper_Data $result  */
		$result = Mage::helper ('salesrule');

		df_assert ($result instanceof Mage_SalesRule_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Sendfriend_Helper_Data
	 */
	public function sendfriendHelper () {

		/** @var Mage_Sendfriend_Helper_Data $result  */
		$result = Mage::helper ('sendfriend');

		df_assert ($result instanceof Mage_Sendfriend_Helper_Data);

		return $result;
	}




	/**
	 * @return Mage_Shipping_Helper_Data
	 */
	public function shippingHelper () {

		/** @var Mage_Shipping_Helper_Data $result  */
		$result = Mage::helper ('shipping');

		df_assert ($result instanceof Mage_Shipping_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Sitemap_Helper_Data
	 */
	public function sitemapHelper () {

		/** @var Mage_Sitemap_Helper_Data $result  */
		$result = Mage::helper ('sitemap');

		df_assert ($result instanceof Mage_Sitemap_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Tag_Helper_Data
	 */
	public function tagHelper () {

		/** @var Mage_Tag_Helper_Data $result  */
		$result = Mage::helper ('tag');

		df_assert ($result instanceof Mage_Tag_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Usa_Helper_Data
	 */
	public function usaHelper () {

		/** @var Mage_Usa_Helper_Data $result  */
		$result = Mage::helper ('usa');

		df_assert ($result instanceof Mage_Usa_Helper_Data);

		return $result;
	}




	/**
	 * @return Mage_Weee_Helper_Data
	 */
	public function weeeHelper () {

		/** @var Mage_Weee_Helper_Data $result  */
		$result = Mage::helper ('weee');

		df_assert ($result instanceof Mage_Weee_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Widget_Helper_Data
	 */
	public function widgetHelper () {

		/** @var Mage_Widget_Helper_Data $result  */
		$result = Mage::helper ('widget');

		df_assert ($result instanceof Mage_Widget_Helper_Data);

		return $result;
	}



	/**
	 * @return Mage_Wishlist_Helper_Data
	 */
	public function wishlistHelper () {

		/** @var Mage_Wishlist_Helper_Data $result  */
		$result = Mage::helper ('wishlist');

		df_assert ($result instanceof Mage_Wishlist_Helper_Data);

		return $result;
	}




	/**
	 * @return Mage_XmlConnect_Helper_Data
	 */
	public function xmlConnectHelper () {

		/** @var Mage_XmlConnect_Helper_Data $result  */
		$result = Mage::helper ('xmlconnect');

		df_assert ($result instanceof Mage_XmlConnect_Helper_Data);

		return $result;
	}







	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Mage';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}