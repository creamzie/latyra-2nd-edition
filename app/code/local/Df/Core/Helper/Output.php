<?php

class Df_Core_Helper_Output extends Mage_Core_Helper_Abstract {



	/**
	 * @param bool $boolean
	 * @return string
	 */
	public function convertBooleanToString ($boolean) {

		df_param_boolean ($boolean, 0);

		/** @var string $result  */
		$result = $boolean ? 'true' : 'false';

		df_result_string ($result);

		return $result;
	}




	/**
	 * @param array $cssClasses
	 * @return string
	 */
	public function getCssClassesAsString (array $cssClasses) {

		df_param_array ($cssClasses, 0);

		/** @var string $result  */
		$result =
			implode (
				Df_Core_Const::T_SPACE
				,
				$cssClasses
			)
		;


		df_result_string ($result);

		return $result;
	}



	/**
	 * @param string $xml
	 * @return string
	 */
	public function formatXml ($xml) {

		df_param_string ($xml, 0);


		/** @var DOMDocument $domDocument  */
		$domDocument = new DOMDocument ();

		df_assert ($domDocument instanceof DOMDocument);



		/** @var bool $r  */
		$r = $domDocument->loadXML ($xml);

		df_assert (TRUE === $r);



		$domDocument->formatOutput = true;



		/** @var string $result  */
		$result =
			$domDocument->saveXML()
		;


		df_result_string ($result);

		return $result;
	}





	/**
	 * @return Df_Core_Helper_Output_Element
	 */
	public function element () {

		/** @var Df_Core_Helper_Output_Element $result */
		$result = Mage::helper (Df_Core_Helper_Output_Element::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Core_Helper_Output_Element);

		return $result;
	}



	/**
	 * @param  array $params
	 * @return Mage_Core_Model_Abstract
	 */
	public function a (array $params) {
		return df_model (Df_Core_Model_Output_Html_A::getNameInMagentoFormat(), $params);
	}


	/**
	 * @param string $string
	 * @param string $delimiter [optional]
	 * @return array
	 */
	public function parseCsv ($string, $delimiter = Df_Core_Const::T_COMMA) {

		/** @var $result  */
		$result =
				df_empty ($string)
			?
				array ()
			:
				array_map (
					'df_trim'
					,
					explode (
						$delimiter
						,
						$string
					)
				)
		;

		return $result;
	}


	/**
	 * @return string
	 */
	public function getXmlHeader () {
		return '<'.'?xml version="1.0" encoding="UTF-8"?'.'>'."\r\n";
	}


	/**
	 * @param  string $string
	 * @return string
	 */
	public function formatUrlKeyPreservingCyrillic ($string) {
		return trim (preg_replace('/[^\pL\pN]+/u','-', mb_strtolower ($string)),'-');
	}


	/**
	 * @param  string $string
	 * @return string
	 */
	public function transliterate ($string) {
		return
			trim (
				strtolower (
					preg_replace(
						'#[^0-9a-z]+#i'
						,
						'-'
						,
						df_mage()->catalog()->product()->urlHelper()->format($string)
					)
				)
				,
				'-'
			)
		;
	}


	/**
	 * @param  string $text
	 * @return string
	 */
	public function _ ($text) {
		return
			df_escape ($text)
		;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Output';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}
