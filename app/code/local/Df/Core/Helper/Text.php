<?php

class Df_Core_Helper_Text extends Mage_Core_Helper_Abstract {


	/**
	 * @param  string $json
	 * @return string
	 */
	public function adjustCyrillicInJson ($json) {
		$trans = array(
		'\u0430'=>'а', '\u0431'=>'б', '\u0432'=>'в', '\u0433'=>'г',
		'\u0434'=>'д', '\u0435'=>'е', '\u0451'=>'ё', '\u0436'=>'ж',
		'\u0437'=>'з', '\u0438'=>'и', '\u0439'=>'й', '\u043a'=>'к',
		'\u043b'=>'л', '\u043c'=>'м', '\u043d'=>'н', '\u043e'=>'о',
		'\u043f'=>'п', '\u0440'=>'р', '\u0441'=>'с', '\u0442'=>'т',
		'\u0443'=>'у', '\u0444'=>'ф', '\u0445'=>'х', '\u0446'=>'ц',
		'\u0447'=>'ч', '\u0448'=>'ш', '\u0449'=>'щ', '\u044a'=>'ъ',
		'\u044b'=>'ы', '\u044c'=>'ь', '\u044d'=>'э', '\u044e'=>'ю',
		'\u044f'=>'я',
		'\u0410'=>'А', '\u0411'=>'Б', '\u0412'=>'В', '\u0413'=>'Г',
		'\u0414'=>'Д', '\u0415'=>'Е', '\u0401'=>'Ё', '\u0416'=>'Ж',
		'\u0417'=>'З', '\u0418'=>'И', '\u0419'=>'Й', '\u041a'=>'К',
		'\u041b'=>'Л', '\u041c'=>'М', '\u041d'=>'Н', '\u041e'=>'О',
		'\u041f'=>'П', '\u0420'=>'Р', '\u0421'=>'С', '\u0422'=>'Т',
		'\u0423'=>'У', '\u0424'=>'Ф', '\u0425'=>'Х', '\u0426'=>'Ц',
		'\u0427'=>'Ч', '\u0428'=>'Ш', '\u0429'=>'Щ', '\u042a'=>'Ъ',
		'\u042b'=>'Ы', '\u042c'=>'Ь', '\u042d'=>'Э', '\u042e'=>'Ю',
		'\u042f'=>'Я',
		'\u0456'=>'і', '\u0406'=>'І', '\u0454'=>'є', '\u0404'=>'Є',
		'\u0457'=>'ї', '\u0407'=>'Ї', '\u0491'=>'ґ', '\u0490'=>'Ґ');
		return strtr($json, $trans);
	}



	/**
	 * @param string $string1
	 * @param string $string2
	 * @return bool
	 */
	public function areEqualCI ($string1, $string2) {

		/** @var bool $result  */
		$result =
			(
				0
					===
				strcmp (
					mb_strtolower ($string1)
					,
					mb_strtolower ($string2)
				)
			)
		;

		df_result_boolean ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	public function bom () {

		/** @var string $result  */
		$result =
			pack ('CCC',0xef,0xbb,0xbf)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @param string $text
	 * @return string
	 */
	public function bomAdd ($text) {

		df_param_string ($text, 0);

		/** @var string $result  */
		$result =
				(
						substr ($text, 0, 3)
					===
						$this->bom()
				)
			?
				$text
			:
				$this->bom() . $text
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @param string $text
	 * @return string
	 */
	public function bomRemove ($text) {

		df_param_string ($text, 0);

		/** @var string $result  */
		$result =
				(
						substr ($text, 0, 3)
					===
						$this->bom()
				)
			?
				substr ($text, 3)
			:
				$text
		;

		if (false === $result) {
			$result = Df_Core_Const::T_EMPTY;
		}

		df_result_string ($result);

		return $result;
	}




	/**
	 * @param string $text
	 * @return string
	 */
	public function camelize ($text) {

		df_param_string ($text, 0);

		/** @var string $result  */
		$result =
			implode (
				Df_Core_Const::T_EMPTY
				,
				array_map (
					array ($this, 'ucfirst')
					,
					explode (
						Df_Core_Const::T_CONFIG_WORD_SEPARATOR
						,
						df_trim (
							$text
						)
					)
				)
			)
		;


		df_result_string ($result);

		return $result;
	}



	/**
	 * @param string $text
	 * @param int $requiredLength
	 * @param bool $addDots [optional]
	 * @return string
	 */
	public function chop ($text, $requiredLength, $addDots = true) {

		df_param_string ($text, 0);

		df_param_integer ($requiredLength, 1);
		df_param_between ($requiredLength, 1, 0);

		df_param_boolean ($addDots, 2);


		/** @var int $fullLength  */
		$fullLength = mb_strlen ($text);

		df_assert_integer ($fullLength);


		/** @var string $result  */
		$result =
				($fullLength <= $requiredLength)
			?
				$text
			:
				implode (
					Df_Core_Const::T_EMPTY
					,
					df_clean (
						array (
							$this->trim (
								mb_substr (
									$text
									,
									0
									,
									$requiredLength
								)
							)
							,
							$addDots ? '...' : null
						)
					)
				)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @param string $string
	 * @return string
	 */
	public function convertUtf8ToWindows1251 ($string) {

		/** @var string $result  */
		$result =
			/**
			 * Насколько я понимаю, данному вызову равноценно:
			 * iconv('utf-8', 'windows-1251', $string)
			 */
			mb_convert_encoding ($string, 'Windows-1251', 'UTF-8')
		;


		df_result_string ($result);

		return $result;
	}



	/**
	 * @param string $string
	 * @return string
	 */
	public function convertWindows1251ToUtf8 ($string) {

		/** @var string $result  */
		$result =
			/**
			 * Насколько я понимаю, данному вызову равноценно:
			 * iconv('windows-1251', 'utf-8', $string)
			 */
			mb_convert_encoding ($string, 'UTF-8', 'Windows-1251')
		;


		df_result_string ($result);

		return $result;
	}




	/**
	 * Escape html entities
	 *
	 * @param mixed $data
	 * @param array $allowedTags [optional]
	 * @return mixed
	 */
	public function escapeHtml ($data, $allowedTags = null) {

		/** @var mixed $result  */
		$result =
				method_exists (df_mage()->coreHelper(), 'escapeHtml')
			?
				call_user_func (
					array (
						df_mage()->coreHelper()
						,
						'escapeHtml'
					)
					,
					$data
					,
					$allowedTags
				)
			:
				df_mage()->coreHelper()->htmlEscape($data, $allowedTags)
		;

		return $result;
	}




	/**
	 * @param string $text
	 * @param string $format
	 * @return string
	 */
	public function formatCase ($text, $format) {

		/** @var string $result  */
		$result = $text;


		switch ($format) {

			case Df_Admin_Model_Config_Source_Format_Text_LetterCase::LOWERCASE:
				$result = mb_strtolower ($result);
				break;

			case Df_Admin_Model_Config_Source_Format_Text_LetterCase::UPPERCASE:
				$result = mb_strtoupper ($result);
				break;

			case Df_Admin_Model_Config_Source_Format_Text_LetterCase::UCFIRST:
				$result = df_text()->ucfirst (mb_strtolower (df_trim ($result)));
				break;
		}


		df_result_string ($result);

		return $result;
	}



	/**
	 * @param string|float $money
	 * @return string
	 */
	public function formatMoney ($money) {
		/** @var string $result  */
		$result =
			sprintf (
				floatval ($money)
				,
				'.2f'
			)
		;
		df_result_string ($result);
		return $result;
	}

	
	
	
	/**
	 * @param int $amount
	 * @param array $forms
	 * @return string
	 */
	public function getNounForm ($amount, array $forms) {
		
		df_param_integer ($amount, 0);

		/** @var string $result  */
		$result =
			$this->getNounFormatter()->getForm ($amount, $forms)
		;

		df_result_string ($result);

		return $result;
	}

	
	
	/**
	 * @return Df_Core_Model_Format_NounForAmounts
	 */
	private function getNounFormatter () {
	
		if (!isset ($this->_nounFormatter)) {
	
			/** @var Df_Core_Model_Format_NounForAmounts $result  */
			$result = 
				df_model (
					Df_Core_Model_Format_NounForAmounts::getNameInMagentoFormat()
				)
			;
	
	
			df_assert ($result instanceof Df_Core_Model_Format_NounForAmounts);
	
			$this->_nounFormatter = $result;
		}
	
		df_assert ($this->_nounFormatter instanceof Df_Core_Model_Format_NounForAmounts);
	
		return $this->_nounFormatter;
	}
	
	
	/**
	* @var Df_Core_Model_Format_NounForAmounts
	*/
	private $_nounFormatter;	
	



	/**
	 * @param string $text
	 * @return string
	 */
	public function htmlspecialchars ($text) {
		$filter = new Df_Zf_Filter_HtmlSpecialChars ();
		return $filter->filter ($text);
	}



	/**
	 * @param  string $text
	 * @return array
	 */
	public function parseTextarea ($text) {

		df_param_string ($text, 0);

		$result =
			df_clean (
				array_map (
					"df_trim"
					,
					explode (
						"\r"
						,
						df_trim (
							$text
						)
					)
				)
			)
		;

		df_result_array ($result);

	    return $result;
	}



	/**
	 * @param string $text
	 * @return string
	 */
	public function quote ($text) {

		df_param_string ($text, 0);

		$result =
			sprintf (
				df_helper()->admin()->__ (self::TEMPLATE_QUOTED_TEXT)
				,
				$text
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @see http://www.php.net/str_ireplace
	 * @param string $search
	 * @param string $replace
	 * @param string $subject
	 * @param int|null $count [optional]
	 * @return string
	 */
	public function replaceCI ($search, $replace, $subject, $count = null) {

		if ( !is_array($search) ) {

			$slen = strlen($search);
			if (0 === $slen) {
				return $subject;
			}

			$lendif = strlen($replace) - strlen($search);
			$search = mb_strtolower($search);

			$search = preg_quote($search);
			$lstr = mb_strtolower($subject);
			$i = 0;
			$matched = 0;
			while ( preg_match('/(.*)'.$search.'/Us',$lstr, $matches) ) {
				if ( $i === $count ) {
					break;
				}
				$mlen = strlen($matches[0]);
				$lstr = substr($lstr, $mlen);
				$subject =
					substr_replace (
						$subject, $replace, $matched+strlen($matches[1]), $slen
					)
				;
				$matched += $mlen + $lendif;
				$i++;
			}
			return $subject;

		}

		else {
			foreach ( array_keys($search) as $k ) {

				if ( is_array($replace) ) {

					if ( array_key_exists($k,$replace) ) {

						$subject =
							$this->replaceCI (
								$search[$k], $replace[$k], $subject, $count
							)
						;

					} else {
						$subject = $this->replaceCI ($search[$k], '', $subject, $count);
					}
				} else {
					$subject = $this->replaceCI ($search[$k], $replace, $subject, $count);
				}
			}
			return $subject;
		}
	}




	/**
	 *
	 * @param string $text
	 * @param string $charlist [optional]
	 * @return string
	 */
	public function trim ($text, $charlist = null) {

		/**
		 * Обратите внимание, что класс Zend_Filter_StringTrim может работать некорректно
		 * для строк, заканчивающихся заглавной кириллической буквой «Р».
		 *
		 * @link http://framework.zend.com/issues/browse/ZF-11223
		 */
		$filter = new Df_Zf_Filter_StringTrim ($charlist);
		/** @var Df_Zf_Filter_StringTrim $filter */


		/** @var string $result */
		$result =  $filter->filter ($text);


		/**
		 * Zend_Filter_StringTrim::filter теоретически может вернуть null,
		 * потому что этот метод зачастую перепоручает вычисление результата функции preg_replace
		 * @url http://php.net/manual/en/function.preg-replace.php
		 */
		$result = df_convert_null_to_empty_string ($result);

		df_result_string ($result);

		return $result;
	}





	/**
	 * @param  string $string
	 * @return string
	 */
    public function ucfirst ($string) {
		return
				mb_strtoupper (
					mb_substr (
						$string, 0, 1
					)
				)
			.
				mb_substr (
					$string, 1
				)
		;
    }





	const TEMPLATE_QUOTED_TEXT = '«%s»';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Text';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}