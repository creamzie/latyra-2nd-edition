<?php

class Df_Core_Helper_Mage_Index extends Mage_Core_Helper_Abstract {

	/**
	 * @return Mage_Index_Model_Indexer
	 */
	public function indexer () {

		$result = Mage::getSingleton ('index/indexer');

		df_assert ($result instanceof Mage_Index_Model_Indexer);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Mage_Index';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}