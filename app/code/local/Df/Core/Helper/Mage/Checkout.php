<?php

class Df_Core_Helper_Mage_Checkout extends Mage_Core_Helper_Abstract {



	/**
	 * @return Mage_Checkout_Helper_Cart
	 */
	public function cartHelper () {

		/** @var Mage_Checkout_Helper_Cart $result  */
		$result = Mage::helper ('checkout/cart');

		df_assert ($result instanceof Mage_Checkout_Helper_Cart);

		return $result;
	}



	/**
	 * @return Mage_Checkout_Helper_Data
	 */
	public function helper () {

		/** @var Mage_Checkout_Helper_Data $result  */
		$result = Mage::helper ('checkout');

		df_assert ($result instanceof Mage_Checkout_Helper_Data);

		return $result;
	}




	/**
	 * @return Mage_Checkout_Model_Type_Onepage
	 */
	public function onePageSingleton () {

		/** @var Mage_Checkout_Model_Type_Onepage $result */
		static $result;

		if (!isset ($result)) {

			/** @var Mage_Checkout_Model_Type_Onepage $result  */
			$result = Mage::getSingleton('checkout/type_onepage');

			df_assert ($result instanceof Mage_Checkout_Model_Type_Onepage);
		}

		return $result;
	}




	/**
	 * @return Mage_Checkout_Model_Session
	 */
	public function sessionSingleton () {

		/** @var Mage_Checkout_Model_Session $result  */
		$result = Mage::getSingleton ('checkout/session');

		df_assert ($result instanceof Mage_Checkout_Model_Session);

		return $result;
	}



	/**
	 * @return Mage_Checkout_Helper_Url
	 */
	public function urlHelper () {

		/** @var Mage_Checkout_Helper_Url $result */
		static $result;

		if (!isset ($result)) {

			/** @var Mage_Checkout_Helper_Url $result  */
			$result = Mage::helper ('checkout/url');

			df_assert ($result instanceof Mage_Checkout_Helper_Url);
		}

		return $result;
	}




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Mage_Checkout';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}