<?php

class Df_Core_Helper_Units extends Mage_Core_Helper_Data {



	/**
	 * @return Df_Core_Model_Units_Length
	 */
	public function length () {

		if (!isset ($this->_length)) {

			/** @var Df_Core_Model_Units_Length $result  */
			$result =
				df_model (
					Df_Core_Model_Units_Length::getNameInMagentoFormat()
				)
			;


			df_assert ($result instanceof Df_Core_Model_Units_Length);

			$this->_length = $result;
		}


		df_assert ($this->_length instanceof Df_Core_Model_Units_Length);

		return $this->_length;

	}


	/**
	* @var Df_Core_Model_Units_Length
	*/
	private $_length;



	/**
	 * @return Df_Core_Model_Units_Weight
	 */
	public function weight () {

		if (!isset ($this->_weight)) {

			/** @var Df_Core_Model_Units_Weight $result  */
			$result =
				df_model (
					Df_Core_Model_Units_Weight::getNameInMagentoFormat()
				)
			;


			df_assert ($result instanceof Df_Core_Model_Units_Weight);

			$this->_weight = $result;
		}


		df_assert ($this->_weight instanceof Df_Core_Model_Units_Weight);

		return $this->_weight;

	}


	/**
	* @var Df_Core_Model_Units_Weight
	*/
	private $_weight;




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Units';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


