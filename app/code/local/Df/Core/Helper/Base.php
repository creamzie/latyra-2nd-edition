<?php



class Df_Core_Helper_Base extends Mage_Core_Helper_Abstract {


	/**
	 * @param boolean $condition
	 * @param string|Mage_Core_Exception $errorMessage [optional]
	 * @return Df_Core_Helper_Base
	 */
	public function assert ($condition, $errorMessage = Df_Core_Const::T_EMPTY) {

		if (!$condition) {
			$this->error ($errorMessage);
		}
	    return $this;

	}




	/**
	 * @param string|null|Exception $message
	 * @throws Df_Core_Exception_Client
	 */
	public function error ($message = null) {
		if ($message instanceof Exception) {
			/** @var Exception $message */
			throw $message;
		}
		else {
			throw new Df_Core_Exception_Client ($message);
		}
	}



	/**
	 * @param string|null|Exception $message
	 * @throws Df_Core_Exception_Internal
	 */
	public function errorInternal ($message = null) {
		if ($message instanceof Exception) {
			/** @var Exception $message */
			throw $message;
		}
		else {
			throw new Df_Core_Exception_Internal ($message);
		}
	}



	/**
	 * @param string $paramName
	 * @param mixed $default [optional]
	 * @return mixed
	 */
	public function getRequestParam ($paramName, $default = null) {
		return Mage::app()->getRequest()->getParam ($paramName, $default);
	}
}
