<?php

class Df_Core_Helper_Assert extends Mage_Core_Helper_Abstract {



	/**
	 * @var Varien_Data_Collection_Db $collection
	 * @return Df_Core_Helper_Assert
	 */
	public function resourceDbCollectionAbstract (Varien_Data_Collection_Db $collection) {
			df_assert (
				df()->check()->resourceDbCollectionAbstract ($collection)
			)
		;

		return $this;
	}



	/**
	 * @var int|string|null|Mage_Core_Model_Store $store
	 * @return Df_Core_Helper_Assert
	 */
	public function storeAsParameterForGettingConfigValue ($store) {

			df_assert (
				df()->check()->storeAsParameterForGettingConfigValue (
					$store
				)
			)
		;

		return $this;
	}



	/**
	 * @var Varien_Data_Collection_Db $collection
	 * @return void
	 */
	public function storeCollection (Varien_Data_Collection_Db $collection) {
			df_assert (
				df()->check()->storeCollection ($collection)
			)
		;
	}



	/**
	 * @var Varien_Data_Collection_Db $collection
	 * @return void
	 */
	public function websiteCollection (Varien_Data_Collection_Db $collection) {
			df_assert (
				df()->check()->websiteCollection ($collection)
			)
		;
	}




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Assert';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}