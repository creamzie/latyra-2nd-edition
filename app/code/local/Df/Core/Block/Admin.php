<?php

abstract class Df_Core_Block_Admin extends Mage_Adminhtml_Block_Abstract {


	/**
	 * @override
	 * @return string|null
	 */
    public function getTemplate() {

		/** @var string|null $result  */
        $result =
				!$this->needToShow()
			?
				null
			:
				(
						!is_null (parent::getTemplate())
					?
						parent::getTemplate()
					:
						$this->getDefaultTemplate ()
				)
		;


		if (!is_null($result)) {
			df_result_string ($result);
		}

		return $result;
    }
	
	


	/**
	 * @return bool
	 */
	protected function needToShow () {

		/** @var bool $result  */
		$result = true;

		df_result_boolean ($result);

		return $result;
	}



	/**
	 * @override
	 * @return string|null
	 */
	protected function getDefaultTemplate () {
		return null;
	}


	

	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Block_Admin';
	}



	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


