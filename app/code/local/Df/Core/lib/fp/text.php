<?php


/**
 * Иногда я для разработки использую заплатку ядра для xDebug —
 * отключаю set_error_handler для режима разработчика.
 *
 * Так вот, xDebug при обработке фатальных сбоев (в том числе и E_RECOVERABLE_ERROR),
 * выводит на экран диагностическое сообщение, и после этого останавливает работу интерпретатора.
 *
 * Конечно, если у нас сбой типов E_ERROR, E_PARSE, E_CORE_ERROR, E_CORE_WARNING,
 * E_COMPILE_ERROR, E_COMPILE_WARNING, то и set_error_handler не поможет
 * (не обрабатывает эти типы сбоев, согласно официальной документации PHP).
 *
 * Однако сбои типа E_RECOVERABLE_ERROR обработик сбоев Magento,
 * установленный посредством set_error_handler, переводит в исключительние ситуации.
 *
 * xDebug же при E_RECOVERABLE_ERROR останавивает работу интерпретатора, что нехорошо.
 *
 * Поэтому для функций, которые могут привести к E_RECOVERABLE_ERROR,
 * пишем обёртки, которые вместо E_RECOVERABLE_ERROR возбуждают исключительную ситуацию.
 * Одна из таких функций — df_string.
 *
 * @param mixed $value
 * @return string
 */
function df_string ($value) {

	if (is_object ($value)) {
		if (!method_exists ($value, '__toString')) {
			df_error (
				sprintf (
					'Программист ошибочно пытается трактовать объект класса %s как строку.'
					,
					get_class ($value)
				)
			);
		}
	}
	else if (is_array ($value)) {
		df_error ('Программист ошибочно пытается трактовать массив как строку.');
	}

	/** @var string $result */
	$result = strval ($value);

	return $result;
}



/**
 * @param string $text
 * @return string
 */
function df_quote ($text) {

	$result = df_text()->quote ($text);

	return $result;
}



/**
 * @param string $text
 * @return string
 */
function df_tab ($text) {

	$result =
		implode (
			"\n"
			,
			array_map (
				'df_prepend_tab'
				,
				explode (
					"\n"
					,
					$text
				)
			)
		)
	;
	return $result;
}


/**
 * @param string $text
 * @return string
 */
function df_prepend_tab ($text) {

	$result = "\t" . $text;
	return $result;
}




/**
 * @param string $string
 * @return string
 */
function df_lcfirst ($string) {

	/** @var string $result  */
	$result =
		(string)
			(
					mb_strtolower (
						mb_substr ($string,0,1)
					)
				.
					mb_substr($string,1)
			)
	;

	return $result;
}


/**
 * @param string|null $string
 * @return string|null
 */
function df_convert_null_to_empty_string ($string) {

	if (!is_null ($string)) {
		df_param_string ($string, 0);
	}

	/** @var string|null $result */
	$result =
			!is_null ($string)
		?
			$string
		:
			Df_Core_Const::T_EMPTY
	;


	if (!is_null ($result)) {
		df_result_string ($result);
	}

	return $result;

}



/**
 * @param  string $string
 * @param string $delimiter [optional]
 * @return array
 */
function df_parse_csv ($string, $delimiter = Df_Core_Const::T_COMMA) {

	/** @var array $result  */
	$result = df_output()->parseCsv ($string, $delimiter);

	df_result_array ($result);

	return $result;
}



/**
 * @param $string1
 * @param $string2
 * @return bool
 */
function df_strings_are_equal_ci ($string1, $string2) {

	/** @var bool $result  */
	$result =
		(
				0
			===
				strcmp (
					mb_strtolower ($string1)
					,
					mb_strtolower ($string2)
				)
		)
	;

	df_result_boolean ($result);

	return $result;
}



/**
 * @return Df_Core_Helper_Text
 */
function df_text () {
	/** @var Df_Core_Helper_Text $result */
	static $result;
	if (!isset ($result)) {
		$result = df_helper()->core ()->text();
		df_assert ($result instanceof Df_Core_Helper_Text);
	}
	return $result;
}




/**
 * Обратите внимание, что иногда вместо данной функции надо применять trim.
 * Например, df_trim не умеет отсекать нулевые байты,
 * которые могут образовываться на конце строки
 * в результате шифрации, передачи по сети прямо в двоичном формате, и затем обратной дешифрации
 * посредством Varien_Crypt_Mcrypt.
 *
 * @see Df_Core_Model_RemoteControl_Coder::decode
 * @see Df_Core_Model_RemoteControl_Coder::encode
 *
 * @param string $string
 * @param string $charlist  [optional]
 * @return string
 */
function df_trim ($string, $charlist = null) {
	return df_text()->trim ($string, $charlist);
}



/**
 * @param string $string
 * @param string $suffix
 * @return string
 */
function df_trim_suffix ($string, $suffix) {

	df_param_string ($string, 0);
	df_param_string ($suffix, 1);

	/** @var string $result */
	$result =
		preg_replace (
			sprintf (
				'#%s$#mui'
				,
				preg_quote ($suffix, '#')
			)
			,
			''
			,
			$string
		)
	;

	df_result_string ($result);

	return $result;
}






/**
 * @return Df_Core_Helper_Output
 */
function df_output () {

	/** @var Df_Core_Helper_Output $result */
	$result = Mage::helper ('df_core/output');

	df_assert ($result instanceof Df_Core_Helper_Output);

	return $result;

}



/**
 * @param  string $text
 * @return string
 */
function df_escape ($text) {
	return df_text ()->htmlspecialchars($text);
}



