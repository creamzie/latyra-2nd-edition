<?php


/**
 * @return Df_Core_Helper_Data
 */
function df () {

	/** @var Df_Core_Helper_Data $result */
	static $result;

	if (!isset ($result)) {
		$result = Mage::helper ('df_core');
	}

	return $result;

}



/**
 * @param mixed $frontendResult
 * @param mixed $adminResult
 * @return mixed
 */
function df_area ($frontendResult, $adminResult) {

	/** @var mixed $result  */
	$result =
			df_is_admin ()
		?
			$adminResult
		:
			$frontendResult
	;

	return $result;

}



/**
 * @return bool
 */
function df_is_admin () {

	/** @var bool $result */
	static $result;

	if (!isset ($result)) {

		$result =
			/**
			 * Раньше тут стояло Mage::app()->getStore()->isAdmin(),
			 * однако, isAdmin проверяет, является ли магазин административным,
			 * более наивным способом: сравнивая идентификатор магазина с нулем
			 * (подразумевая, что 0 — идентификатор административного магазина).
			 *
			 * Как оказалось, у некоторых клиентов идентификатор административного магазина
			 * не равен нулю (видимо, что-то не то делали с базой данных).
			 *
			 * Поэтому используем более надёжную проверку — кода магазина.
			 */
			('admin' === Mage::app()->getStore()->getCode())
		;
	}

	return $result;

}



/**
 * @param $type
 * @param string $name
 * @param array $attributes
 * @return Mage_Core_Block_Abstract|bool
 */
function df_block ($type, $name=Df_Core_Const::T_EMPTY, array $attributes = array()) {

	/** @var Mage_Core_Model_Layout $layout */
	static $layout;

	if (!isset ($layout)) {
		$layout = df_mage()->core()->layout();
	}

	/** @var Mage_Core_Block_Abstract $result  */
	$result = $layout->createBlock ($type, $name, $attributes);

	df_assert ($result instanceof Mage_Core_Block_Abstract);

	return $result;
}





/**
 * @param  int $levelsToSkip
 * Позволяет при записи стека вызовов пропустить несколько последних вызовов функций,
 * которые и так очевидны (например, вызов данной функции, вызов df_bt () и т.п.)
 *
 *
 * @return void
 */
function df_bt ($levelsToSkip = 0) {
	df()->debug()->logCompactBacktrace ($levelsToSkip + 1);
}



/**
 * @return Df_Admin_Helper_Settings
 */
function df_cfg () {

	/** @var Df_Admin_Helper_Settings $result */
	$result = Mage::helper ('df_admin/settings');

	df_assert ($result instanceof Df_Admin_Helper_Settings);

	return $result;

}



/**
 * @return string
 */
function df_current_url () {
	return df_mage()->core()->url()->getCurrentUrl();
}




/**
 * Создаёт обертку нужного класса $class для системного события $observer.
 * Вы также можете передать в конструктор обёртки дополнительные параметры $additionalParams
 *
 * @static
 * @param string $class
 * @param Varien_Event_Observer $observer
 * @param array $additionalParams
 * @return Df_Core_Model_Event
 */
function df_event ($class, Varien_Event_Observer $observer, $additionalParams = array ()) {
	return Df_Core_Model_Event::create ($class, $observer, $additionalParams);
}




/**
 * @param  mixed $value
 * @return bool
 */
function df_empty ($value) {
	return
		empty ($value)
	;
}



/**
 * @param Exception $exception
 * @return void
 */
function df_exception_add_to_session (Exception $exception) {

	if (!($exception instanceof Mage_Core_Exception)) {
		rm_session()
			->addError (
				nl2br (
					df_exception_get_trace ($exception)
				)
			)
		;
	}
	else {
		/** @var Mage_Core_Exception $exception */

		if ($exception->getMessage()) {
			rm_session()
				->addError (
				$exception->getMessage()
				)
			;
		}
		else if (0 === count ($exception->getMessages())) {
			/**
			 * Надо хоть какое-то сообщение показать
			 */
			rm_session()
				->addError (
					nl2br (
						df_exception_get_trace ($exception)
					)
				)
			;
		}
		else {
			foreach ($exception->getMessages() as $message) {
				/** @var Mage_Core_Model_Message_Abstract $message */
				rm_session()
					->addError (
						$message->getText()
					)
				;
			}
		}
	}
}




/**
 * К сожалению, не можем перекрыть Exception::getTraceAsString(),
 * потому что этот метод — финальный
 *
 * @param Exception $exception
 * @param bool $showCodeContext [optional]
 * @return string
 */
function df_exception_get_trace (Exception $exception, $showCodeContext = false) {

	/** @var Df_Qa_Model_Message_Failure_Exception $log */
	$log =
		df_model (
			Df_Qa_Model_Message_Failure_Exception::getNameInMagentoFormat()
			,
			array (
				Df_Qa_Model_Message_Failure_Exception::PARAM__EXCEPTION => $exception
				,
				Df_Qa_Model_Message_Failure_Exception::PARAM__NEED_LOG_TO_FILE => false
				,
				Df_Qa_Model_Message_Failure_Exception::PARAM__NEED_NOTIFY_DEVELOPER => false
				,
				Df_Qa_Model_Message_Failure_Exception::PARAM__SHOW_CODE_CONTEXT => $showCodeContext
			)
		)
	;

	df_assert ($log instanceof Df_Qa_Model_Message_Failure_Exception);

	return $log->getTraceAsText();
}





/**
 * Обработка исключительных ситуаций в точках сочленения моих модулей и ядра
 *
 * ($rethrow === true) => перевозбудить исключительную ситуацию
 * ($rethrow === false) => не перевозбуждать исключительную ситуацию
 * ($rethrow === null) =>  перевозбудить исключительную ситуацию, если включен режим разработчика
 *
 * @param Exception $e
 * @param bool|null $rethrow
 * @throws Exception
 * @return void
 */
function df_handle_entry_point_exception (Exception $e, $rethrow = null) {

	/**
	 * Надо учесть, что исключительная ситуация могла произойти при асинхронном запросе,
	 * и в такой ситуации echo () неэффективно.
	 */
	df_log_exception ($e);

	/**
	 * В режиме разработчика
	 * по умолчанию выводим диагностическое сообщение на экран
	 * (но это можно отключить посредством $rethrow = false).
	 *
	 * При отключенном режиме разработчика
	 * по умолчанию не выводим диагностическое сообщение на экран
	 * (но это можно отключить посредством $rethrow = true).
	 */
	if ((Mage::getIsDeveloperMode() && (false !== $rethrow)) || (true === $rethrow)) {

		/**
		 * Чтобы кириллица отображалась в верной кодировке —
		 * пробуем отослать браузеру заголовок Content-Type.
		 *
		 * Обратите внимание, что такой подход не всегда корректен:
		 * ведь нашу исключительную ситуацию может поймать и обработать
		 * ядро Magento или какой-нибудь сторонний модуль, и они затем могут
		 * захотеть вернуть браузеру документ другого типа (не text/html).
		 * Однако, по-правильному они должны при этом сами установить свой Content-type/
		 */
		if (!headers_sent ()) {
			header ('Content-Type: text/html; charset=UTF-8');
		}

		throw $e;
	}
}



/**
 * @static
 * @param string $handlerClass
 * @param string $eventClass
 * @param Varien_Event_Observer $observer
 * @return void
 */
function df_handle_event ($handlerClass, $eventClass, Varien_Event_Observer $observer) {

	Df_Core_Model_Handler
		::create (
			$handlerClass
			,
			df_event (
				$eventClass
				,
				$observer
			)
		)
			->handle ()
	;
}



/**
 * @param  string $handle
 * @return bool
 */
function df_handle_presents ($handle) {

	/** @var bool[] $cache */
	static $cache;

	/** @var Mage_Core_Model_Layout_Update $update */
	static $update;

	if (!isset ($cache [$handle])) {

		if (!isset ($update)) {
			$update = df_mage()->core ()->layout()->getUpdate();
			df_assert ($update instanceof Mage_Core_Model_Layout_Update);
		}


		/** @var bool $result */
		$result =
			in_array (
				$handle
				,
				$update->getHandles()
			)
		;

		$cache [$handle] = $result;
	}

	return $cache [$handle];

}



/**
 * @return Df_Core_Helper_Df_Helper
 */
function df_helper () {

	/**
	 * Использование статической переменной
	 * вместо постоянных вызовов Mage::helper ('df_core/df_helper')
	 * ускоряет загрузку главной страницы на эталонном тесте
	 * с 2.074 сек. до 1.938 сек.!
	 */

	/** @var Df_Core_Helper_Df_Helper $result  */
	static $result;

	if (!isset ($result)) {
		$result = Mage::helper ('df_core/df_helper');
	}

	return $result;
}






/**
 * @param string $message
 * @param string $filename [optional]
 * @return void
 */
function df_log ($message, $filename = Df_Qa_Model_Message_Notification::DEFAULT__FILE_NAME) {

	/** @var Df_Qa_Model_Message_Notification $log */
	$log =
		df_model (
			Df_Qa_Model_Message_Notification::getNameInMagentoFormat()
			,
			array (
				Df_Qa_Model_Message_Notification::PARAM__NOTIFICATION => $message
				,
				Df_Qa_Model_Message_Notification::PARAM__NEED_LOG_TO_FILE => true
				,
				Df_Qa_Model_Message_Notification::PARAM__FILE_NAME => $filename
				,
				Df_Qa_Model_Message_Notification::PARAM__NEED_NOTIFY_DEVELOPER => true
			)
		)
	;

	df_assert ($log instanceof Df_Qa_Model_Message_Notification);

	$log->log();

}





/**
 * Задача данного метода — ясно и доступно объяснить разработчику причину исключительной ситуации
 * и состояние системы в момент возникновения исключительной ситуации.
 *
 * Если у вас нет объекта класса Exception, то используйте df_log
 *
 * @param Exception $exception
 * @return void
 */
function df_log_exception ($exception) {

	/** @var Df_Qa_Model_Message_Failure_Exception $log */
	$log =
		df_model (
			Df_Qa_Model_Message_Failure_Exception::getNameInMagentoFormat()
			,
			array (
				Df_Qa_Model_Message_Failure_Exception::PARAM__EXCEPTION => $exception
				,
				Df_Qa_Model_Message_Failure_Exception::PARAM__NEED_LOG_TO_FILE => true
				,
				Df_Qa_Model_Message_Failure_Exception::PARAM__NEED_NOTIFY_DEVELOPER => true
			)
		)
	;

	df_assert ($log instanceof Df_Qa_Model_Message_Failure_Exception);

	$log->log();
}



/**
 * @return Df_Core_Helper_Mage
 */
function df_mage () {

	/**
	 * static тут не даёт выигрыша в скорости
	 */

	/** @var Df_Core_Helper_Mage $result */
	$result = Mage::helper ('df_core/mage');

	return $result;

}



/**
 * @param string $param1 [optional]
 * @param string $param2 [optional]
 * @return string|boolean
 */
function df_magento_version ($param1 = null, $param2 = null) {
	return df()->version ()->magentoVersion ($param1, $param2);
}





/**
 * Retrieve model object
 *
 * @link    Mage_Core_Model_Config::getModelInstance
 * @param   string $modelClass
 * @param   mixed $arguments
 * @return  Mage_Core_Model_Abstract
 * @throws Exception
 */
function df_model ($modelClass = '', $arguments = array()) {

	/**
	 * Удаление df_param_string
	 * ускорило загрузку главной страницы на эталонном тесте
	 * с 1.501 сек. до 1.480 сек.
	 */

	/** @var Mage_Core_Model_Abstract $result */
	$result = null;


	try {
		$result = Mage::getModel ($modelClass, $arguments);
	}
	catch (Exception $e) {

		$bt = debug_backtrace ();
		/** @var array $bt */

		$caller = df_a ($bt, 1);
		/** @var array $caller */

		$className = df_a ($caller, "class");
		/** @var string $className  */

		$methodName = df_a ($caller, "function");
		/** @var string $methodName  */


		$methodNameWithClassName =
			implode (
				"::"
				,
				array (
					$className
					,
					$methodName
				)
			)
		;
			/** @var string $methodNameWithClassName */


		df_error (
			strtr (
					"%method% [%line%]\nНе могу создать модель класса «%modelClass%»."
				.
					"\nСообщение системы: «%message%»"
				,
				array (
					'%method%' => $methodNameWithClassName
					,
					'%line%' => df_a (df_a ($bt, 0), "line")
					,
					'%modelClass%' => $modelClass
					,
					'%message%' => $e->getMessage ()
				)
			)
		)
		;
	}
	return $result;
}



/**
 * @param string $moduleName
 * @return bool
 */
function df_module_enabled ($moduleName) {

	/** @var array $cachedResult */
	static $cachedResult = array ();

	/** @var bool $result  */
	$result = df_a ($cachedResult, $moduleName);

	if (is_null ($result)) {

		/** @var Varien_Simplexml_Element|null $moduleConfig  */
		$moduleConfig = Mage::app()->getConfig()->getModuleConfig ($moduleName);

		if (
				($moduleConfig instanceof Varien_Simplexml_Element)
			&&
				!df_empty ($moduleConfig->asXML ())
		) {
			/** @var array|string $moduleConfigAsCanonicalArray */
			$moduleConfigAsCanonicalArray = $moduleConfig->asCanonicalArray();

			if (is_array ($moduleConfigAsCanonicalArray)) {

				/** @var string $isActiveAsString  */
				$isActiveAsString = df_a ($moduleConfigAsCanonicalArray, 'active');

				$result = ('true' === $isActiveAsString);
			}
		}
		$cachedResult [$moduleName] = $result;
	}

	return $result;
}





/**
 * @param mixed $argument
 * @return mixed
 */
function df_nop ($argument) {
	return $argument;
}



/**
 * @param string $message
 * @return void
 */
function df_notify_me ($message) {

	/** @var Df_Qa_Model_Message_Notification $log */
	$log =
		df_model (
			Df_Qa_Model_Message_Notification::getNameInMagentoFormat()
			,
			array (
				Df_Qa_Model_Message_Notification::PARAM__NOTIFICATION => $message
				,
				Df_Qa_Model_Message_Notification::PARAM__NEED_LOG_TO_FILE => false
				,
				Df_Qa_Model_Message_Notification::PARAM__NEED_NOTIFY_DEVELOPER => true
			)
		)
	;

	df_assert ($log instanceof Df_Qa_Model_Message_Notification);

	$log->log();

}




/**
 * @param mixed $value
 * @return bool
 */
function df_parse_boolean ($value) {

	/** @var bool $result */
	$result =
			!is_null ($value)
		&&
			(0 !== intval ($value))
	;

	return $result;

}




/**
 * @return Df_Core_Helper_Path
 */
function df_path () {
	return df()->path ();
}



/**
 * Shortcut to phpQuery::pq($arg1, $context)
 * Chainable.
 *
 * @param $arguments
 * @param $context [optional]
 * @return phpQueryObject
 */
function df_pq ($arguments, $context = null) {

	/** @var bool $initialized  */
	static $initialized = false;

	if (false === $initialized) {
		df_helper()->phpquery()->lib()->init();
		$initialized = true;
	}

	$args = func_get_args();

	/** @var phpQueryObject|bool $result */
	$result =
		call_user_func_array (
			array ('phpQuery', 'pq')
			,
			$args
		)
	;

	df_assert ($result instanceof phpQueryObject);

	return $result;
}



/**
 * @param string $key
 * @param string $default [optional]
 * @return string
 */
function df_request ($key, $default = null) {
	return df()->request()->getParam ($key, $default);
}




/**
 * @return string
 */
function df_t () {
	$fa = func_get_args ();
	return
		Mage::app()->getTranslator()->translate(
			array_merge (
				array (
					new Mage_Core_Model_Translate_Expr (df_a ($fa, 1), df_a ($fa, 0))
				)
				,
				array_slice (
					$fa
					,
					2
				)
			)
		)
	;
}




/**
 * @return Df_Core_Helper_Url
 */
function df_url () {
	return df_helper()->core ()->url ();
}



/**
 * @returns Mage_Core_Model_Session_Abstract
 */
function rm_session () {

	/** @var Mage_Core_Model_Session_Abstract $result */
	static $result;

	if (!isset ($result)) {
		$result =
				df_is_admin()
			?
				df_mage()->adminhtml()->session()
			:
				df_mage()->core()->session()
		;

		df_assert ($result instanceof Mage_Core_Model_Session_Abstract);
	}

	return $result;
}


/**
 * @return string
 */
function rm_version () {
	/** @var string $result */
	static $result;
	if (!isset ($result)) {
		/** @var string $result  */
		$result = (string)(Mage::getConfig()->getNode('df/version'));
		if (df()->version()->isItFree()) {
			$result .= '-free';
		}
		else {
			if (df()->version()->isItAdmin()) {
				$result .= '-admin';
			}
		}
	}
	return $result;
}


/**
 * @return string
 */
function rm_version_full () {
	/** @var string $result  */
	$result =
		sprintf ('%s (%s)', rm_version(), Mage::getVersion())
	;

	return $result;
}



