<?php


/**
 * @param  string $feature
 * @param  int|Mage_Core_Model_Store $store [optional]
 * @return bool
 */
function df_enabled ($feature, $store = null) {

	/** @var bool $result */
	static $result;

	if (!isset ($result)) {
		if (!df_module_enabled(Df_Core_Module::LICENSOR)) {
			$result = true;
		}
		else {
			/** @var Df_Licensor_Helper_Data $licensorHelper */
			static $licensorHelper = null;

			if (!isset ($licensorHelper)) {
				$licensorHelper = df_helper()->licensor();
			}

			$result = $licensorHelper->getCachedSingleton()->isEnabled ($feature, $store);
		}
	}

	return $result;
}




/**
 * @param  string $code
 * @return Df_Licensor_Model_Feature
 */
function df_feature ($code) {
	return df_helper()->licensor()->getFeatureByCode ($code);
}




/**
 * @return bool
 */
function df_is_it_my_local_pc () {
	return ('DFEDIUK-PC' === df_a ($_SERVER, 'COMPUTERNAME'));
}



/**
 * @return bool
 */
function df_is_it_my_sever () {

	/** @var bool $result  */
	$result =
			('server.magento-pro.ru' === Mage::app()->getRequest()->getHttpHost())
		&&
			('176.9.3.77' === df_a ($_SERVER, 'SERVER_ADDR'))
	;

	df_result_boolean ($result);

	return $result;
}



