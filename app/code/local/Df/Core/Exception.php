<?php

class Df_Core_Exception extends Mage_Core_Exception {

	/**
	 * @return int
	 */
	public function getStackLevelsCountToSkip () {
		if (!isset ($this->_stackLevelsCountToSkip)) {
			$this->_stackLevelsCountToSkip = 0;
		}
		return $this->_stackLevelsCountToSkip;
	}



	/**
	 * К сожалению, не можем перекрыть Exception::getTraceAsString(),
	 * потому что этот метод — финальный
	 * @return string
	 */
	public function getTraceAsText () {

		/** @var Df_Qa_Model_Message_Failure_Exception $log */
		$log =
			df_model (
				Df_Qa_Model_Message_Failure_Exception::getNameInMagentoFormat()
				,
				array (
					Df_Qa_Model_Message_Failure_Exception::PARAM__EXCEPTION => $this
					,
					Df_Qa_Model_Message_Failure_Exception::PARAM__NEED_LOG_TO_FILE => false
					,
					Df_Qa_Model_Message_Failure_Exception::PARAM__NEED_NOTIFY_DEVELOPER => false
				)
			)
		;

		df_assert ($log instanceof Df_Qa_Model_Message_Failure_Exception);

		/** @var string $result */
		$result =
			$log->getTraceAsText()
		;
		df_result_string ($result);
		return $result;
	}




	/**
	 * @param  int $count
	 * @return Df_Core_Exception
	 */
	public function setStackLevelsCountToSkip ($count) {
		$this->_stackLevelsCountToSkip = $count;
		return $this;
	}

	/** @var int */
	private $_stackLevelsCountToSkip;

}


