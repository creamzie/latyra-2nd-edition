<?php

class Df_Core_Model_InputRequest extends Df_Core_Model_Abstract {



	/**
	 * @param string $paramName
	 * @param string $defaultValue [optional]
	 * @return string|null
	 */
	public function getParam ($paramName, $defaultValue = null) {

		/** @var string|null $result  */
		$result = $this->getRequest()->getParam($paramName, $defaultValue);

		if (!is_null ($result)) {
			df_result_string ($result);
		}

		return $result;
	}



	/**
	 * @return Mage_Core_Controller_Request_Http
	 */
	protected function getRequest () {
		return $this->cfg (self::PARAM__REQUEST);
	}



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM__REQUEST, 'Mage_Core_Controller_Request_Http'
			)
		;
	}


	const PARAM__REQUEST = 'request';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_InputRequest';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


