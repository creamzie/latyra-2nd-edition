<?php

class Df_Core_Model_Entity_Dependency extends Df_Core_Model_Abstract {

	/**
	 * @return string
	 */
	public function getActionSaveClassNameMf () {
		return $this->cfg (self::PARAM__ACTION_SAVE__CLASS_NAME_MF);
	}


	/**
	 * @return string
	 */
	public function getEntityClassNameMf () {
		return $this->cfg (self::PARAM__CLASS_NAME_MF);
	}


	/**
	 * @return string
	 */
	public function getEntityIdFieldName () {
		return $this->cfg (self::PARAM__ID_FIELD_NAME);
	}


	/**
	 * @return string
	 */
	public function getId () {
		return $this->getName();
	}


	/**
	 * @return string
	 */
	public function getName () {
		return $this->cfg (self::PARAM__NAME);
	}


	/**
	 * @return bool
	 */
	public function needDeleteCascade () {
		return $this->cfg (self::PARAM__DELETE_CASCADE, false);
	}



	/**
	 * @override
	 * @return void
	 */
    protected function _construct() {
		parent::_construct();
		$this
			->addValidator (self::PARAM__ACTION_SAVE__CLASS_NAME_MF, new Df_Zf_Validate_String())
			->addValidator (self::PARAM__CLASS_NAME_MF, new Df_Zf_Validate_String())
			->addValidator (
				self::PARAM__DELETE_CASCADE, new Df_Zf_Validate_Boolean(), $isRequired = false
			)
			->addValidator (self::PARAM__ID_FIELD_NAME, new Df_Zf_Validate_String())
			->addValidator (self::PARAM__NAME, new Df_Zf_Validate_String())
		;
    }

	const PARAM__ACTION_SAVE__CLASS_NAME_MF = 'action_save__class_name_mf';
	const PARAM__CLASS_NAME_MF = 'class_name_mf';
	const PARAM__DELETE_CASCADE = 'delete_cascade';
	const PARAM__ID_FIELD_NAME = 'id_field_name';
	const PARAM__NAME = 'name';


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Entity_Dependency';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


