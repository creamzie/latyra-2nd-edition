<?php

class Df_Core_Model_Entity_Dependency_Collection extends Df_Varien_Data_Collection {


	/**
	 * @param string $name
	 * @param string $classNameMf
	 * @param string $actionSaveClassNameMf
	 * @param string $idFieldName
	 * @param bool $deleteCascade [optional]
	 * @return Df_Core_Model_Entity_Dependency_Collection
	 */
	public function addDependency (
		$name, $classNameMf, $actionSaveClassNameMf, $idFieldName, $deleteCascade = false
	) {
		/** @var Df_Core_Model_Entity_Dependency $dependency */
		$dependency =
			df_model (
				Df_Core_Model_Entity_Dependency::getNameInMagentoFormat()
				,
				array (
					Df_Core_Model_Entity_Dependency
						::PARAM__ACTION_SAVE__CLASS_NAME_MF => $actionSaveClassNameMf
					,
					Df_Core_Model_Entity_Dependency::PARAM__CLASS_NAME_MF => $classNameMf
					,
					Df_Core_Model_Entity_Dependency::PARAM__DELETE_CASCADE => $deleteCascade
					,
					Df_Core_Model_Entity_Dependency::PARAM__ID_FIELD_NAME => $idFieldName
					,
					Df_Core_Model_Entity_Dependency::PARAM__NAME => $name
				)
			)
		;
		df_assert ($dependency instanceof Df_Core_Model_Entity_Dependency);

		$this->addItem ($dependency);

		return $this;
	}



	/**
	 * @override
	 * @return string
	 */
	protected function getItemClass () {
		return Df_Core_Model_Entity_Dependency::getClass();
	}


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Entity_Dependency_Collection';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


