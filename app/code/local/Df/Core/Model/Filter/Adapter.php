<?php

class Df_Core_Model_Filter_Adapter
	extends Df_Core_Model_Abstract
	implements Zend_Filter_Interface {



	/**
	 * @override
	 * @param mixed $value
	 * @return $value
	 */
	public function filter ($value) {

		$this->getAdapteeInstance()
			->setData (
			    $this->getParamNameForFilteredValue()
				,
				$value
			)
		;


		/** @var mixed $result  */
		$result =
			call_user_func (
				array ($this->getAdapteeInstance(), $this->getAdapteeMethod())
			)
		;

		return $result;
	}



	/**
	 * @return Varien_Object
	 */
	private function getAdapteeInstance () {

		/** @var Varien_Object $result  */
		$result = $this->cfg (self::PARAM__ADAPTEE_INSTANCE);

		df_assert ($result instanceof Varien_Object);

		return $result;
	}




	/**
	 * @return string
	 */
	private function getAdapteeMethod () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM__ADAPTEE_METHOD);

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	private function getParamNameForFilteredValue () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM__PARAM_NAME_FORM_FILTERED_VALUE);

		df_result_string ($result);

		return $result;
	}




 	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM__ADAPTEE_INSTANCE, Df_Varien_Const::OBJECT_CLASS
			)
			->addValidator (
				self::PARAM__ADAPTEE_METHOD, new Df_Zf_Validate_String()
			)
			->addValidator (
				self::PARAM__PARAM_NAME_FORM_FILTERED_VALUE
				,
				new Df_Zf_Validate_String()
			)
		;
	}



	const PARAM__ADAPTEE_INSTANCE = 'adaptee_instance';
	const PARAM__ADAPTEE_METHOD = 'adaptee_method';
	const PARAM__PARAM_NAME_FORM_FILTERED_VALUE = 'param_name_form_filtered_value';




	/**
	 * @static
	 * @param Varien_Object $adapteeInstance
	 * @param string $adapteeMethod
	 * @param string $paramNameForFilteredValue
	 * @return Df_Core_Model_Filter_Adapter
	 */
	public static function create (
		Varien_Object $adapteeInstance
		,
		$adapteeMethod
		,
		$paramNameForFilteredValue
	) {

		/** @var Df_Core_Model_Filter_Adapter $result  */
		$result =
			df_model (
				self::getNameInMagentoFormat()
				,
				array (
					self::PARAM__ADAPTEE_INSTANCE => $adapteeInstance
					,
					self::PARAM__ADAPTEE_METHOD => $adapteeMethod
					,
					self::PARAM__PARAM_NAME_FORM_FILTERED_VALUE => $paramNameForFilteredValue
				)
			)
		;

		df_assert ($result instanceof Df_Core_Model_Filter_Adapter);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Filter_Adapter';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


