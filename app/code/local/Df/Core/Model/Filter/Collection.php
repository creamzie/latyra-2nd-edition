<?php


abstract class Df_Core_Model_Filter_Collection
	extends Df_Core_Model_Abstract
	implements Zend_Filter_Interface {



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Filter_Collection';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}







    /**
     *
     * @param  mixed $value
     * @throws Zend_Filter_Exception If filtering $value is impossible
     * @return Df_Varien_Data_Collection
     */
    public function filter ($value) {

		/*************************************
		 * Проверка входных параметров метода
		 */
		df_param_collection ($value, $this->getItemClass (), 0);
		/*************************************/

		/** @var array|Traversable $value */


		$result = $this->createResultCollection ();
		/** @var Df_Varien_Data_Collection $result  */


		$result
			->addValidator (
				$this->createValidator()
			)
		;


		$result->addItems ($value);


		df_result_collection ($result, $this->getItemClass ());
		df_assert ($result instanceof Df_Varien_Data_Collection);

		return $result;
	}



	/**
	 * Должна возвращать класс элементов коллекции
	 *
	 * @abstract
	 * @return string
	 */
	abstract protected function getItemClass ();



	/**
	 * Создает коллецию - результат фильтрации.
	 * Потомки могут перекрытием этого метода создать коллекцию своего класса.
	 * Метод должен возвращать объект класса Df_Varien_Data_Collection или его потомков
	 *
	 * @return Df_Varien_Data_Collection
	 */
	protected function createResultCollection () {
		$result = new Df_Varien_Data_Collection ();

		df_assert ($result instanceof Df_Varien_Data_Collection);

		return $result;
	}



	/**
	 *
	 * @return Zend_Validate_Interface
	 */
	abstract protected function createValidator ();

}


