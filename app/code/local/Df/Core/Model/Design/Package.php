<?php

class Df_Core_Model_Design_Package extends Mage_Core_Model_Design_Package {



	/**
	 * @override
     * @param string $file
     * @param array $params
     * @return string
	 */
	public function getSkinUrl ($file = null, array $params = array()) {

		/** @var string $result  */
		$result = parent::getSkinUrl ($file, $params);

		if (false !== strpos ($result, '/rm/')) {

			/**
			 * Обратите внимание, что для ресурсов из папки js мы добавляем параметр v по-другому:
			 * в методе Df_Page_Block_Html_Head::_prepareStaticAndSkinElements
			 */

			$result = df()->url()->addVersionStamp ($result);
		}

		df_result_string ($result);

		return $result;
	}





	/**
	 * @override
     * @param string $file
     * @param array &$params
     * @param array $fallbackScheme
	 * @return string
	 */
	protected function _fallback ($file, array &$params, array $fallbackScheme = array(array())) {

		/**
		 * Раньше здесь стояло:
		 *
			array_splice (
				$fallbackScheme
				,
				0
				,
				0
				,
				array (
					array (
						'_package' => 'rm'
						,
						'_theme' => 'priority'
					)
					,
					array (
						'_package' => df_a ($params, '_package')
						,
						'_theme' => df_a ($params, '_theme')
					)
				)
			)
			;
		 *
		 *
		 * array_unshift, видимо, работает быстрее
		 */
		array_unshift (
			$fallbackScheme
			,
			array (
				'_package' => 'rm'
				,
				'_theme' => 'priority'
			)
			,
			array (
				'_package' => df_a ($params, '_package')
				,
				'_theme' => df_a ($params, '_theme')
			)
		);



		$fallbackScheme[] =
			array (
				'_package' => 'rm'
				,
				'_theme' => self::DEFAULT_THEME
			)
		;



		/** @var string $result  */
		$result =
			parent::_fallback ($file, $params, $fallbackScheme)
		;

		df_result_string ($result);

		return $result;
	}



}


