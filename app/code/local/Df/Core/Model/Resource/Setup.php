<?php

class Df_Core_Model_Resource_Setup extends Mage_Core_Model_Resource_Setup {


	/**
	 * @return Df_Warehousing_Model_Resource_Setup
	 */
	public function install_2_16_0 () {
		Df_Core_Model_Resource_Location::i()->tableCreate ($this);
		return $this;
	}


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Resource_Setup';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		return 'df_core/setup';
	}
}


