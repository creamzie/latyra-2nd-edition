<?php

class Df_Core_Model_Resource_Location_Collection
	extends Mage_Core_Model_Mysql4_Collection_Abstract {



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct();
		$this->_init (Df_Core_Model_Location::getNameInMagentoFormat());
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Resource_Location_Collection';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		return 'df_core/location_collection';
	}

}


