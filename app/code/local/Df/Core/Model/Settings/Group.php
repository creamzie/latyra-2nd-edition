<?php

class Df_Core_Model_Settings_Group extends Df_Core_Model_Abstract {



	/**
	 * @param string $configKeySuffix
	 * @param array|string $prefixes [optional]
	 * @param mixed $defaultValue [optional]
	 * @param int|string|Mage_Core_Model_Store $store [optional]
	 * @return mixed
	 */
	public function getValue (
		$configKeySuffix
		,
		$prefixes = array ()
		,
		$defaultValue = null
		,
		$store = null
	) {

		df_param_string ($configKeySuffix, 0);

		/**
		 * country_id => country
		 * region_id => region
		 */

		/**
		 * Оптимизация выражения
		 * $configKeySuffix = preg_replace ('#_id$#', '', $configKeySuffix);
		 */
		if ('_id' === substr ($configKeySuffix, -3)) {
			$configKeySuffix = substr ($configKeySuffix, 0, -3);
		}

		if (!is_string ($prefixes)) {
			df_param_array ($prefixes, 1);
		}

		/** @var mixed $result  */
		$result =
			Mage::getStoreConfig (
				$this->expandConfigKey (
					$this->implodePrefixes (
						$configKeySuffix
						,
						$prefixes
					)
				)
				,
				$store
			)
		;

		if (is_null ($result)) {
			$result = $defaultValue;
		}

		return $result;
	}



	/**
	 * @param array|string $prefixes
	 * @return Df_Core_Model_Settings_Group
	 */
	protected function appendPrefixes ($prefixes) {

		if (!is_string ($prefixes)) {
			df_param_array ($prefixes, 1);
		}

		$this
			->setData (
				self::PARAM__PREFIXES
				,
				array_merge (
					$this->getPrefixesAsArray()
					,
					is_array ($prefixes) ? $prefixes : array ($prefixes)
				)
			)
		;

		return $this;
	}



	/**
	 * @return string
	 */
	protected function getGroup () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM__GROUP);

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	protected function getSection () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM__SECTION);

		df_result_string ($result);

		return $result;
	}



	/**
	 * @param string $configKeySuffix
	 * @param array|string $prefixes [optional]
	 * @param bool $defaultValue [optional]
	 * @param int|string|Mage_Core_Model_Store $store [optional]
	 * @return bool
	 */
	protected function getYesNo (
		$configKeySuffix
		,
		$prefixes = array ()
		,
		$defaultValue = null
		,
		$store = null
	) {

		df_param_string ($configKeySuffix, 0);

		if (!is_string ($prefixes)) {
			df_param_array ($prefixes, 1);
		}

		/** @var bool $result  */
		$result =
			df()->settings()->parseYesNo (
				$this->getValue (
					$configKeySuffix
					,
					$prefixes
					,
					$defaultValue
					,
					$store
				)
			)
		;

		if (is_null ($result)) {
			$result = $defaultValue;
		}

		df_result_boolean ($result);

		return $result;
	}



	/**
	 * @param array|string $prefixes
	 * @return Df_Core_Model_Settings_Group
	 */
	protected function prependPrefixes ($prefixes) {

		if (!is_string ($prefixes)) {
			df_param_array ($prefixes, 1);
		}

		$this
			->setData (
				self::PARAM__PREFIXES
				,
				array_merge (
					is_array ($prefixes) ? $prefixes : array ($prefixes)
					,
					$this->getPrefixesAsArray()
				)
			)
		;

		return $this;
	}




	/**
	 * @param string $configKeySuffix
	 * @param array|string $prefixes
	 * @return string
	 */
	private function implodePrefixes ($configKeySuffix, $prefixes) {

		df_param_string ($configKeySuffix, 0);

		if (!is_string ($prefixes)) {
			df_param_array ($prefixes, 1);
		}


		if (!is_array ($prefixes)) {
			$prefixes = array ($prefixes);
		}

		/** @var string $result  */
		$result =
			implode (
				self::PREFIX_SEPARATOR
				,
				array_merge (
					$prefixes
					,
					array ($configKeySuffix)
				)
			)
		;

		df_result_string ($result);

		return $result;
	}
	
	
	
	
	
	/**
	 * @param string $configKeySuffix
	 * @return string
	 */
	private function expandConfigKey ($configKeySuffix) {

		df_param_string ($configKeySuffix, 0);

		/** @var string $result  */
		$result =
			df()->config()->implodeKey (
				array (
					$this->getSection()
					,
					$this->getGroup()
					,
					$this->implodePrefixes (
						$configKeySuffix
						,
						$this->getPrefixesAsArray ()
					)
				)
			)
		;


		df_result_string ($result);

		return $result;
	}
	



	/**
	 * @return array
	 */
	private function getPrefixesAsArray () {

		/** @var array $result  */
		$result = $this->cfg (self::PARAM__PREFIXES, array ());

		if (is_string ($result)) {
			$result = array ($result);
		}

		df_result_array ($result);

		return $result;
	}








	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__GROUP, new Df_Zf_Validate_String()
			)
			->addValidator (
				self::PARAM__SECTION, new Df_Zf_Validate_String()
			)
			->addValidator (
				self::PARAM__PREFIXES, new Df_Zf_Validate_Array(), false
			)
		;
	}



	const PARAM__GROUP = 'group';
	const PARAM__PREFIXES = 'prefixes';
	const PARAM__SECTION = 'section';

	const PREFIX_SEPARATOR = '__';


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Settings_Group';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}