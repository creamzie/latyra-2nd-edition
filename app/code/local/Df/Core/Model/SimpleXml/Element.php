<?php

abstract class Df_Core_Model_SimpleXml_Element extends Df_Core_Model_Abstract {


	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getTagName();



	/**
	 * @return Df_Varien_Simplexml_Element
	 */
	public function getElement () {
	
		if (!isset ($this->_element)) {
	
			/** @var Df_Varien_Simplexml_Element $result  */
			$result = $this->createElement();
	
			df_assert ($result instanceof Df_Varien_Simplexml_Element);
	
			$this->_element = $result;
		}
	
		df_assert ($this->_element instanceof Df_Varien_Simplexml_Element);
	
		return $this->_element;
	}
	
	
	/**
	* @var Df_Varien_Simplexml_Element
	*/
	private $_element;	




	/**
	 * @return Df_Varien_Simplexml_Element
	 */
	protected function createElement () {

		/** @var Df_Varien_Simplexml_Element $result  */
		$result =
			Df_Varien_Simplexml_Element::createNode (
				$this->getTagName()
				,
				$this->getAttributes()
			)
		;

		df_assert ($result instanceof Df_Varien_Simplexml_Element);

		return $result;
	}




	/**
	 * @return array
	 */
	protected function getAttributes () {
		return array();
	}





	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_SimpleXml_Element';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


