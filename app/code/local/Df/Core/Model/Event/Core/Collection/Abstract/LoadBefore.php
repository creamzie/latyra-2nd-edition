<?php



/**
 * Cообщение:		«core_collection_abstract_load_before»
 * Источник:		Mage_Core_Model_Resource_Db_Collection_Abstract::_beforeLoad()
 * [code]
        parent::_beforeLoad();
        Mage::dispatchEvent('core_collection_abstract_load_before', array('collection' => $this));
        if ($this->_eventPrefix && $this->_eventObject) {
            Mage::dispatchEvent($this->_eventPrefix.'_load_before', array(
                $this->_eventObject => $this
            ));
        }
 * [/code]
 */
class Df_Core_Model_Event_Core_Collection_Abstract_LoadBefore extends Df_Core_Model_Event {


	/**
	 * @return Mage_Core_Model_Resource_Db_Collection_Abstract|Mage_Core_Model_Mysql4_Collection_Abstract
	 */
	public function getCollection () {

		/** @var Mage_Core_Model_Resource_Db_Collection_Abstract|Mage_Core_Model_Mysql4_Collection_Abstract $result  */
		$result =
			$this->getEventParam (self::EVENT_PARAM__COLLECTION)
		;


		df()->assert()->resourceDbCollectionAbstract ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	protected function getExpectedEventSuffix() {
		return self::EXPECTED_EVENT_SUFFIX;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Event_Core_Collection_Abstract_LoadBefore';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



	const EXPECTED_EVENT_SUFFIX = '_load_before';

	const EVENT_PARAM__COLLECTION = 'collection';

}


