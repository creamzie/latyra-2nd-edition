<?php



/**
 * Cообщение:		«controller_action_postdispatch»
 * 					«controller_action_postdispatch_controller_action»
 * Источник:		Mage_Core_Controller_Varien_Action::postDispatch()
 * [code]
        Mage::dispatchEvent(
            'controller_action_postdispatch_'.$this->getFullActionName(),
            array('controller_action'=>$this)
        );
        Mage::dispatchEvent(
            'controller_action_postdispatch_'.$this->getRequest()->getRouteName(),
            array('controller_action'=>$this)
        );
        Mage::dispatchEvent('controller_action_postdispatch', array('controller_action'=>$this));
 * [/code]
 *
 * Назначение:		Позволяет выполнить дополнительную обработку запроса
 * 					после обработы запроса контроллером
 */
class Df_Core_Model_Event_Controller_Action_Postdispatch extends Df_Core_Model_Event {

	
	/**
	 * @return bool
	 */
	public function isClientExpectedHtml () {
	
		if (!isset ($this->_clientExpectedHtml)) {
	
			/** @var bool $result  */
			$result =
				df_strings_are_equal_ci (
					Zend_Mime::TYPE_HTML
					,
					df_a ($this->getContentTypesExpectedByClient(), 0)
				)
			;
	
			df_assert_boolean ($result);
	
			$this->_clientExpectedHtml = $result;
		}

		df_result_boolean ($this->_clientExpectedHtml);
	
		return $this->_clientExpectedHtml;
	}
	
	
	/**
	* @var bool
	*/
	private $_clientExpectedHtml;		
	
	

	
	
	/**
	 * @return Mage_Core_Controller_Request_Http
	 */
	public function getRequest () {
	
		/** @var Mage_Core_Controller_Request_Http $result  */
		$result =
			$this->getController()->getRequest()
		;
	
		df_assert ($result instanceof Mage_Core_Controller_Request_Http);
	
		return $result;
	}
	
	
	



	/**
	 * @return Mage_Core_Controller_Varien_Action
	 */
	public function getController () {

		/** @var Mage_Core_Controller_Varien_Action $result  */
		$result =
			$this->getEventParam (self::EVENT_PARAM__CONTROLLER_ACTION)
		;


		df_assert ($result instanceof Mage_Core_Controller_Varien_Action);

		return $result;
	}
	
	
	
	
	
	/**
	 * @return array
	 */
	private function getContentTypesExpectedByClient () {
	
		if (!isset ($this->_contentTypesExpectedByClient)) {
	
			/** @var array $result  */
			$result =
				df_parse_csv (
					$this->getAcceptHeader()
				)
			;
	
			df_assert_array ($result);
	
			$this->_contentTypesExpectedByClient = $result;
		}
	
	
		df_result_array ($this->_contentTypesExpectedByClient);
	
		return $this->_contentTypesExpectedByClient;
	}
	
	
	/**
	* @var array
	*/
	private $_contentTypesExpectedByClient;	
	
	
	
	
	
	
	
	/**
	 * @return string
	 */
	private function getAcceptHeader () {
	
		if (!isset ($this->_acceptHeader)) {
	
			/** @var string|bool $result  */
			$result = 
				$this->getRequest()->getHeader (self::HEADER__ACCEPT);
			;


			if (false === $result) {
				$result = Df_Core_Const::T_EMPTY;
			}
	
	
			df_assert_string ($result);
	
			$this->_acceptHeader = $result;
		}
	
	
		df_result_string ($this->_acceptHeader);
	
		return $this->_acceptHeader;
	}
	
	
	/**
	* @var string
	*/
	private $_acceptHeader;	
	
	





	/**
	 * @return string
	 */
	protected function getExpectedEventPrefix () {
		return self::EXPECTED_EVENT_PREFIX;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Event_Controller_Action_Postdispatch';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



	const EXPECTED_EVENT_PREFIX = 'controller_action_postdispatch';
	const EVENT_PARAM__CONTROLLER_ACTION = 'controller_action';


	/**
	 * Обратите внимание, что названия заголовков могут быть записаны в запросе
	 * как прописными буквами, так и строчными,
	 * однако Zend_Controller_Request_Http::getHeader() самостоятельно приводит их к нужному регистру
	 * @link http://stackoverflow.com/questions/5258977/are-http-headers-case-sensitive
	 */
	const HEADER__ACCEPT = 'Accept';

}


