<?php



/**
 * Cообщение:		«controller_action_predispatch_adminhtml»
 * Источник:		Mage_Core_Controller_Varien_Action::preDispatch()
 * [code]
        Mage::dispatchEvent(
            'controller_action_predispatch_'.$this->getRequest()->getRouteName(),
            array('controller_action'=>$this)
        );
 * [/code]
 */
class Df_Core_Model_Event_Controller_Action_Predispatch_Adminhtml
	extends Df_Core_Model_Event_Controller_Action_Predispatch {



	/**
	 * @return Mage_Adminhtml_Controller_Action
	 */
	public function getController () {

		/** @var Mage_Adminhtml_Controller_Action $result  */
		$result =
			parent::getController()
		;


		df_assert ($result instanceof Mage_Adminhtml_Controller_Action);

		return $result;
	}





	/**
	 * @return string
	 */
	protected function getExpectedEventPrefix () {
		return self::EXPECTED_EVENT_PREFIX;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Event_Controller_Action_Predispatch_Adminhtml';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



	const EXPECTED_EVENT_PREFIX = 'controller_action_predispatch_adminhtml';

}


