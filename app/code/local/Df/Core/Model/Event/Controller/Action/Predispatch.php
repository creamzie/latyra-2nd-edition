<?php



/**
 * Cообщение:		«controller_action_predispatch»
 * Источник:		Mage_Core_Controller_Varien_Action::preDispatch()
 * [code]
		Mage::dispatchEvent('controller_action_predispatch', array('controller_action'=>$this));
 * [/code]
 *
 * Назначение:		Позволяет выполнить дополнительную обработку запроса
 * 					перед обработкой запроса контроллером
 */
class Df_Core_Model_Event_Controller_Action_Predispatch extends Df_Core_Model_Event {



	/**
	 * @return Mage_Core_Controller_Varien_Action
	 */
	public function getController () {

		/** @var Mage_Core_Controller_Varien_Action $result  */
		$result =
			$this->getEventParam (self::EVENT_PARAM__CONTROLLER_ACTION)
		;


		df_assert ($result instanceof Mage_Core_Controller_Varien_Action);

		return $result;
	}





	/**
	 * @return string
	 */
	protected function getExpectedEventPrefix () {
		return self::EXPECTED_EVENT_PREFIX;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Event_Controller_Action_Predispatch';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



	const EXPECTED_EVENT_PREFIX = 'controller_action_predispatch';
	const EVENT_PARAM__CONTROLLER_ACTION = 'controller_action';

}


