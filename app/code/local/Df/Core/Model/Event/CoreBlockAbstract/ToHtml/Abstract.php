<?php



abstract class Df_Core_Model_Event_CoreBlockAbstract_ToHtml_Abstract extends Df_Core_Model_Event {



	/**
	 * @return string
	 */
	public function getBlockName () {

		/** @var string $result  */
		$result =
			$this->getBlock()->getNameInLayout()
		;

		if (is_null ($result)) {
			/**
			 * Оказывается, в макете могут присутствовать блоки с неуказанным типом.
			 * Например, Mage_GiftMessage_Block_Message_Inline.
			 */

			$result = Df_Core_Const::T_EMPTY;
		}

		df_result_string ($result);

		return $result;
	}





	/**
	 * @return string
	 */
	public function getBlockType () {

		/** @var string $result  */
		$result =
			$this->getBlock()->getData (Df_Core_Block_Template::PARAM__TYPE)
		;

		if (is_null ($result)) {
			/**
			 * Оказывается, в макете могут присутствовать блоки с неуказанным типом.
			 * Например, Mage_GiftMessage_Block_Message_Inline.
			 */

			try {
				$result =
					df()->reflection()->getModelNameInMagentoFormat(
						get_class (
							$this->getBlock()
						)
					)
				;
			}
			catch (Exception $e) {

			}

			if (is_null($result)) {
				$result = Df_Core_Const::T_EMPTY;
			}
		}

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return Mage_Core_Block_Abstract
	 */
	public function getBlock () {

		/** @var Mage_Core_Block_Abstract $result  */
		$result = $this->getEventParam (self::EVENT_PARAM__BLOCK);

		df_assert ($result instanceof Mage_Core_Block_Abstract);

		return $result;
	}




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Event_CoreBlockAbstract_ToHtml_Abstract';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}


	const EVENT_PARAM__BLOCK = 'block';

}


