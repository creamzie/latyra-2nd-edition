<?php


/**
 * Cообщение:		«df_adminhtml_block_sales_order_grid__prepare_collection»
 * Источник:		Df_Adminhtml_Block_Sales_Order_Grid::setCollection()
 * [code]
		Mage
			::dispatchEvent(
				'df_adminhtml_block_sales_order_grid__prepare_collection'
				,
				array (
					'collection' => $collection
				)
			)
		;	
 * [/code]
 */
class Df_Core_Model_Event_Adminhtml_Block_Sales_Order_Grid_PrepareCollection
	extends Df_Core_Model_Event {



	/**
	 * @return Mage_Sales_Model_Resource_Order_Grid_Collection|Mage_Sales_Model_Mysql4_Order_Collection
	 */
	public function getCollection () {

		/** @var Mage_Sales_Model_Resource_Order_Grid_Collection|Mage_Sales_Model_Mysql4_Order_Collection $result  */
		$result =
			$this->getEventParam (self::EVENT_PARAM__COLLECTION)
		;

		df_helper()->sales()->assert()->orderGridCollection ($result);

		return $result;
	}





	/**
	 * @return string
	 */
	protected function getExpectedEventPrefix () {
		return self::EXPECTED_EVENT_PREFIX;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Event_Adminhtml_Block_Sales_Order_Grid_PrepareCollection';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



	const EXPECTED_EVENT_PREFIX = 'df_adminhtml_block_sales_order_grid__prepare_collection';

	const EVENT_PARAM__COLLECTION = 'collection';

}



