<?php


/**
 * Cообщение:		«df_adminhtml_block_sales_order_grid__prepare_columns_after»
 * Источник:		Df_Adminhtml_Block_Sales_Order_Grid::_prepareColumns()
 * [code]
		Mage
			::dispatchEvent(
				'df_adminhtml_block_sales_order_grid__prepare_columns_after'
				,
				array (
					'grid' => $this
				)
			)
		;
 * [/code]
 */
class Df_Core_Model_Event_Adminhtml_Block_Sales_Order_Grid_PrepareColumnsAfter
	extends Df_Core_Model_Event {



	/**
	 * @return Df_Adminhtml_Block_Sales_Order_Grid
	 */
	public function getGrid () {

		/** @var Df_Adminhtml_Block_Sales_Order_Grid $result  */
		$result =
			$this->getEventParam (self::EVENT_PARAM__GRID)
		;


		df_assert ($result instanceof Df_Adminhtml_Block_Sales_Order_Grid);

		return $result;
	}





	/**
	 * @return string
	 */
	protected function getExpectedEventPrefix () {
		return self::EXPECTED_EVENT_PREFIX;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Event_Adminhtml_Block_Sales_Order_Grid_PrepareColumnsAfter';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



	const EXPECTED_EVENT_PREFIX = 'df_adminhtml_block_sales_order_grid__prepare_columns_after';

	const EVENT_PARAM__GRID = 'grid';

}



