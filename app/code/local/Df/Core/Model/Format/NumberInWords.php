<?php

class Df_Core_Model_Format_NumberInWords extends Df_Core_Model_Abstract {


	/**
	 * @return string
	 */
	public function getValueInWords () {

		if (!isset ($this->_valueInWords)) {

			/** @var string $result  */
			$result =
				implode (
					Df_Core_Const::T_SPACE
					,
					df_clean (
						array (
							$this->getIntegerValueInWords()
							,
							$this->getFractionalValueInWords()
						)
					)
				)
			;


			df_assert_string ($result);

			$this->_valueInWords = $result;
		}


		df_result_string ($this->_valueInWords);

		return $this->_valueInWords;

	}


	/**
	* @var string
	*/
	private $_valueInWords;
	

	
	
	
	/**
	 * @return string
	 */
	public function getIntegerValueInWords () {
	
		if (!isset ($this->_integerValueInWords)) {
	
			/** @var string $result  */
			$result = 
					(0 === $this->getNumberIntegerPart())
				?
					Df_Core_Const::T_EMPTY
				:
					implode (
						Df_Core_Const::T_SPACE
						,
						array (
							$this->getNumberIntegerPartInWords ()
							,
							df_a (
								$this->getIntegerPartUnits()
								,
								$this->getNumberIntegerPartForm ()
							)
						)
					)
			;
	
	
			df_assert_string ($result);
	
			$this->_integerValueInWords = $result;
		}
	
	
		df_result_string ($this->_integerValueInWords);
	
		return $this->_integerValueInWords;
	}
	
	
	/**
	* @var string
	*/
	private $_integerValueInWords;	
	
	
	
	
	
	
	
	/**
	 * @return string
	 */
	public function getFractionalValueInWords () {
	
		if (!isset ($this->_fractionalValueInWords)) {

	
			/** @var string $result  */
			$result =
					/**
					 * Надо использовать именно ==, а не ===
					 */
					(0 === $this->getNumberFractionalPart())
				?
					Df_Core_Const::T_EMPTY
				:
					implode (
						Df_Core_Const::T_SPACE
						,
						array (
							$this->getNumberFractionalPartInWords ()
							,
							df_a (
								$this->getFractionalPartUnits()
								,
								$this->getNumberFractionalPartForm ()
							)
						)
					)
			;
	
	
			df_assert_string ($result);
	
			$this->_fractionalValueInWords = $result;
		}
	
	
		df_result_string ($this->_fractionalValueInWords);
	
		return $this->_fractionalValueInWords;
	}
	
	
	/**
	* @var string
	*/
	private $_fractionalValueInWords;			
	



	
	
	
	
	/**
	 * @return string
	 */
	public function getNumberIntegerPartInWords () {
	
		if (!isset ($this->_numberIntegerPartInWords)) {
	
			/** @var string $result  */
			$result =
				$this->getNaturalNumberInWords (
					$this->getNumberIntegerPart()
					,
					$this->getIntegerPartGender()
				)
			;
	
			df_assert_string ($result);
	
			$this->_numberIntegerPartInWords = $result;
		}
	
	
		df_result_string ($this->_numberIntegerPartInWords);
	
		return $this->_numberIntegerPartInWords;
	}
	
	
	/**
	* @var string
	*/
	private $_numberIntegerPartInWords;	
	
	
	
	
	
	
	
	
	/**
	 * @return string
	 */
	public function getNumberFractionalPartInWords () {
	
		if (!isset ($this->_numberFractionalPartInWords)) {
	
			/** @var string $result  */
			$result =
				$this->getNaturalNumberInWords (
					$this->getNumberFractionalPart()
					,
					$this->getFractionalPartGender()
				)
			;
	
			df_assert_string ($result);
	
			$this->_numberFractionalPartInWords = $result;
		}
	
	
		df_result_string ($this->_numberFractionalPartInWords);
	
		return $this->_numberFractionalPartInWords;
	}
	
	
	/**
	* @var string
	*/
	private $_numberFractionalPartInWords;






	/**
	 * @return int
	 */
	public function getNumberFractionalPart () {

		if (!isset ($this->_numberFractionalPart)) {

			/** @var int $result  */
			$result =
				round (
						pow (10, $this->getFractionalPartPrecision())
					*
						(
								$this->getNumber()
							-
								$this->getNumberIntegerPart()
						)

				)
			;


			df_assert_integer ($result);

			$this->_numberFractionalPart = $result;
		}


		df_result_integer ($this->_numberFractionalPart);

		return $this->_numberFractionalPart;

	}


	/**
	* @var int
	*/
	private $_numberFractionalPart;
	
	
	
	

	
	
	
	/**
	 * @return int
	 */
	private function getNumberIntegerPartForm () {
	
		if (!isset ($this->_numberIntegerPartForm)) {

			/** @var int $result  */
			$result = self::getNum125 ($this->getNumberIntegerPart());

			df_assert_integer ($result);
	
			$this->_numberIntegerPartForm = $result;
		}
	
	
		df_result_integer ($this->_numberIntegerPartForm);
	
		return $this->_numberIntegerPartForm;
	}
	
	
	/**
	* @var int
	*/
	private $_numberIntegerPartForm;
	
	
	
	
	
	
	/**
	 * @return int
	 */
	private function getNumberFractionalPartForm () {
	
		if (!isset ($this->_numberFractionalPartForm)) {

			/** @var int $result  */
			$result = self::getNum125 ($this->getNumberFractionalPart());

			df_assert_integer ($result);
	
			$this->_numberFractionalPartForm = $result;
		}
	
	
		df_result_integer ($this->_numberFractionalPartForm);
	
		return $this->_numberFractionalPartForm;
	}
	
	
	/**
	* @var int
	*/
	private $_numberFractionalPartForm;	






	/**
	 * @param int $number
	 * @param string $gender
	 * @return string
	 */
	private function getNaturalNumberInWords ($number, $gender) {

		df_param_integer ($number, 0);
		df_param_between ($number, 0, 0, self::MAX_NUMBER);

		df_param_string ($gender, 1);
		df_assert (
			in_array (
				$gender
				,
				array (
					Df_Core_Model_Format_NumberInWords::GENDER__MALE
					,
					Df_Core_Model_Format_NumberInWords::GENDER__FEMALE
				)
			)
		)
		;


		/** @var string $result  */
		$result = 'ноль';


		if (0 !== $number) {
			$result  =
				preg_replace(
					array('/s+/','/\s$/')
					,
					array(' ','')
					,
					$this->getNum1E9 ($number, $gender)
				)
			;
		}


		df_result_string ($result);

		return $result;
	}

	

	
	
	
	
	/**
	 * @return int
	 */
	private function getNumberIntegerPart () {
	
		if (!isset ($this->_numberIntegerPart)) {
	
			/** @var int $result  */
			$result = 
				intval ($this->getNumber())
			;
	
	
			df_assert_integer ($result);
	
			$this->_numberIntegerPart = $result;
		}
	
	
		df_result_integer ($this->_numberIntegerPart);
	
		return $this->_numberIntegerPart;
	}
	
	
	/**
	* @var int
	*/
	private $_numberIntegerPart;
	
	
	
	
	



	/**
	 * @return float
	 */
	private function getNumber () {

		/** @var float $result  */
		$result =
			$this->cfg (self::PARAM__NUMBER)
		;

		df_result_float ($result);

		df_result_between ($result, 0, self::MAX_NUMBER);

		return $result;
	}





	/**
	 * @return string
	 */
	private function getIntegerPartGender () {

		/** @var string $result  */
		$result =
			$this->cfg (self::PARAM__INTEGER_PART_GENDER)
		;

		df_result_string ($result);

		df_assert (
			in_array (
				$result
				,
				array (
					Df_Core_Model_Format_NumberInWords::GENDER__MALE
					,
					Df_Core_Model_Format_NumberInWords::GENDER__FEMALE
				)
			)
		)
		;

		return $result;
	}





	/**
	 * @return string
	 */
	private function getFractionalPartGender () {

		/** @var string $result  */
		$result =
			$this->cfg (self::PARAM__FRACTIONAL_PART_GENDER)
		;

		df_result_string ($result);

		df_assert (
			in_array (
				$result
				,
				array (
					Df_Core_Model_Format_NumberInWords::GENDER__MALE
					,
					Df_Core_Model_Format_NumberInWords::GENDER__FEMALE
				)
			)
		)
		;

		return $result;
	}





	/**
	 * @return int
	 */
	private function getFractionalPartPrecision () {

		/** @var int $result  */
		$result = $this->cfg (self::PARAM__FRACTIONAL_PART_PRECISION);

		df_result_integer ($result);

		df_result_between ($result, 0);

		return $result;
	}




	/**
	 * @return array
	 */
	private function getIntegerPartUnits () {

		/** @var array $result  */
		$result = $this->cfg (self::PARAM__INTEGER_PART_UNITS);

		df_result_array ($result);

		return $result;
	}





	/**
	 * @return array
	 */
	private function getFractionalPartUnits () {

		/** @var array $result  */
		$result = $this->cfg (self::PARAM__FRACTIONAL_PART_UNITS);

		df_result_array ($result);

		return $result;
	}





	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				/**
				 * Обратите внимание, что целые числа не проходят валидацию is_float,
				 * но проходят валидацию Zend_Validate_Float
				 */
				self::PARAM__NUMBER, new Zend_Validate_Float ()
			)
			->addValidator (
				self::PARAM__INTEGER_PART_GENDER, new Df_Zf_Validate_String()
			)
			->addValidator (
				self::PARAM__FRACTIONAL_PART_GENDER, new Df_Zf_Validate_String()
			)
			->addValidator (
				self::PARAM__FRACTIONAL_PART_PRECISION, new Zend_Validate_Int()
			)
			->addValidator (
				self::PARAM__INTEGER_PART_UNITS, new Df_Zf_Validate_Array()
			)
			->addValidator (
				self::PARAM__FRACTIONAL_PART_UNITS, new Df_Zf_Validate_Array()
			)
		;
	}





	/**
	 * @static
	 * @param $number
	 * @param string $gender
	 * @return string
	 */
	private static function getNum1E9 ($number, $gender) {

		df_param_integer ($number, 0);
		df_param_string ($gender, 1);


		/** @var string $result  */
		$result =
				(1e6 > $number)
			?
				self::getNum1E6 ($number, $gender)
			:
				implode (
					Df_Core_Const::T_SPACE
					,
					array (
						self::getNum1000 (
							intval ($number / 1e6)
							,
							self::GENDER__FEMALE
						)

						,
						df_a (
							self::getNe6()
							,
							self::getNum125 (
								intval (
									$number / 1e6
								)
							)
						)

						,
						self::getNum1E6 ($number % 1e6, $gender)
					)
				)
		;

		df_result_string ($result);

		return $result;
	}





	/**
	 * @static
	 * @param $number
	 * @param string $gender
	 * @return string
	 */
	private static function getNum1E6 ($number, $gender) {

		df_param_integer ($number, 0);
		df_param_string ($gender, 1);


		/** @var string $result  */
		$result =
				(1000 > $number)
			?
				self::getNum1000 ($number, $gender)
			:
				implode (
					Df_Core_Const::T_SPACE
					,
					array (
						self::getNum1000 (
							intval ($number / 1000)
							,
							self::GENDER__MALE
						)

						,
						df_a (
							self::getNe3()
							,
							self::getNum125 (
								intval (
									$number / 1000
								)
							)
						)

						,
						self::getNum1000 ($number % 1000, $gender)
					)
				)
		;

		df_result_string ($result);

		return $result;
	}







	/**
	 * @static
	 * @param $number
	 * @param string $gender
	 * @return string
	 */
	private static function getNum1000 ($number, $gender) {

		df_param_integer ($number, 0);
		df_param_string ($gender, 1);


		/** @var string $result  */
		$result =
				(100 > $number)
			?
				self::getNum100 ($number, $gender)
			:
					df_a (
						self::getNe2()
						,
						intval($number/100)
					)
				.
					(
							($number%100)
						?
							(
									Df_Core_Const::T_SPACE
							 	.
								 	self::getNum100 ($number%100, $gender)
							)
						:
							Df_Core_Const::T_EMPTY
					)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @static
	 * @param $number
	 * @param string $gender
	 * @return string
	 */
	private static function getNum100 ($number, $gender) {

		df_param_integer ($number, 0);
		df_param_string ($gender, 1);

		$result =
				(20 > $number)
			?
				df_a (
					df_a (
						self::getNe0()
						,
						$gender
					)
					,
					$number
				)
			:
					df_a (
						self::getNe1()
						,
						intval($number/10)
					)
				.
					(
							($number % 10)
						?
							(
									Df_Core_Const::T_SPACE
							 	.
									df_a (
										df_a (
											self::getNe0()
											,
											$gender
										)
										,
										$number % 10
									)
							)
						:
							Df_Core_Const::T_EMPTY
					)
		;


		df_result_string ($result);

		return $result;


	}






	/**
	 * Форма склонения слова.
	 * Существительное с числительным склоняется одним из трех способов:
	 * 1 миллион, 2 миллиона, 5 миллионов.
	 *
	 * @static
	 * @param int $number
	 * @return int
	 */
	private static function getNum125 ($number) {

		/** @var int $result  */
		$result = null;


		/** @var int $n100 */
		$n100 = $number % 100;

		df_assert_integer ($n100);


		/** @var int $n100 */
		$n10 = $number % 10;

		df_assert_integer ($n10);



		if( ($n100 > 10) && ($n100 < 20) ) {
			$result = self::NUMBER_FORM_5;
		}
		elseif ($n10 === 1) {
			$result = self::NUMBER_FORM_1;
		}
		elseif (($n10 >= 2) && ($n10 <= 4) ) {
			$result = self::NUMBER_FORM_2;
		}
		else {
			$result = self::NUMBER_FORM_5;
		}

		df_result_integer ($result);

		return $result;
	}
	
	
	
	
	

	/**
	 * @static
	 * @return array
	 */
	private static function getNe0 () {
	
		if (!isset (self::$_ne0)) {
	
			/** @var array $result  */
			$result =
				array (
					self::GENDER__MALE =>
						array (
							Df_Core_Const::T_EMPTY
							,
							'один','два','три','четыре','пять','шесть'
							, 'семь','восемь','девять','десять','одиннадцать'
							, 'двенадцать','тринадцать','четырнадцать','пятнадцать'
							, 'шестнадцать','семнадцать','восемнадцать','девятнадцать'
						)
					,
					self::GENDER__FEMALE =>
						array (
							Df_Core_Const::T_EMPTY,'одна','две','три','четыре','пять','шесть'
							, 'семь','восемь','девять','десять','одиннадцать'
							, 'двенадцать','тринадцать','четырнадцать','пятнадцать'
							, 'шестнадцать','семнадцать','восемнадцать','девятнадцать'
						)
				)

			;
	
	
			df_assert_array ($result);
	
			self::$_ne0 = $result;
		}
	
	
		df_result_array (self::$_ne0);
	
		return self::$_ne0;
	}
	
	
	/**
	 * @static
	* @var array
	*/
	private static $_ne0;
	
	
	
	
	
	
	/**
	 * @static
	 * @return array
	 */
	private static function getNe1 () {
	
		if (!isset (self::$_ne1)) {
	
			/** @var array $result  */
			$result = 
				array (
					Df_Core_Const::T_EMPTY
					,
					'десять','двадцать','тридцать','сорок','пятьдесят'
					,
					'шестьдесят','семьдесят','восемьдесят','девяносто'
				)
			;
	
	
			df_assert_array ($result);
	
			self::$_ne1 = $result;
		}
	
	
		df_result_array (self::$_ne1);
	
		return self::$_ne1;
	}
	
	
	/**
	* @var array
	*/
	private static $_ne1;






	/**
	 * @static
	 * @return array
	 */
	private static function getNe2 () {

		if (!isset (self::$_ne2)) {

			/** @var array $result  */
			$result =
				array (
					Df_Core_Const::T_EMPTY
					,
					'сто','двести','триста','четыреста','пятьсот'
					,
					'шестьсот','семьсот','восемьсот','девятьсот'
				)
			;


			df_assert_array ($result);

			self::$_ne2 = $result;
		}


		df_result_array (self::$_ne2);

		return self::$_ne2;

	}


	/**
	 * @static
	* @var array
	*/
	private static $_ne2;
	
	
	
	
	
	
	
	/**
	 * @static
	 * @return array
	 */
	private static function getNe3 () {

		if (!isset (self::$_ne3)) {

			/** @var array $result  */
			$result =
				array (
					self::NUMBER_FORM_1 => 'тысяча'
					,
					self::NUMBER_FORM_2 => 'тысячи'
					,
					self::NUMBER_FORM_5 => 'тысяч'
				)
			;


			df_assert_array ($result);

			self::$_ne3 = $result;
		}


		df_result_array (self::$_ne3);

		return self::$_ne3;

	}


	/**
	 * @static
	* @var array
	*/
	private static $_ne3;
	
	
	
	
	
	
	/**
	 * @static
	 * @return array
	 */
	private static function getNe6 () {

		if (!isset (self::$_ne6)) {

			/** @var array $result  */
			$result =
				array (
					self::NUMBER_FORM_1 => 'миллион'
					,
					self::NUMBER_FORM_2 => 'миллиона'
					,
					self::NUMBER_FORM_5 => 'миллионов'
				)
			;


			df_assert_array ($result);

			self::$_ne6 = $result;
		}


		df_result_array (self::$_ne6);

		return self::$_ne6;

	}


	/**
	 * @static
	* @var array
	*/
	private static $_ne6;
			





	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Format_NumberInWords';
	}



	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}





	const PARAM__NUMBER = 'number';

	const PARAM__INTEGER_PART_GENDER = 'integerPartGender';
	const PARAM__FRACTIONAL_PART_GENDER = 'fractionalPartGender';

	const PARAM__FRACTIONAL_PART_PRECISION = 'fractionalPartPrecision';


	const PARAM__INTEGER_PART_UNITS = 'integerPartUnits';
	const PARAM__FRACTIONAL_PART_UNITS = 'fractionalPartUnits';


	const GENDER__MALE = 'male';
	const GENDER__FEMALE = 'female';



	const NUMBER_FORM_1 = 0;
	const NUMBER_FORM_2 = 1;
	const NUMBER_FORM_5 = 2;


	const MAX_NUMBER = 1e9;




}


