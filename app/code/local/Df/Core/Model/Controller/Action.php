<?php


abstract class Df_Core_Model_Controller_Action extends Df_Core_Model_Abstract {



	/**
	 * @return Df_Core_Model_Controller_Action
	 */
	abstract public function process ();




	/**
	 * @param string $path
	 * @param array $arguments [optional]
	 * @return Df_Core_Model_Controller_Action
	 */
	public function redirect ($path, $arguments = array()) {
		$this->getController()->setRedirectWithCookieCheck ($path, $arguments);
		return $this;
	}




	/**
	 * @return Mage_Core_Controller_Varien_Action
	 */
	protected function getController () {

		/** @var Mage_Core_Controller_Varien_Action $result  */
		$result = $this->cfg (self::PARAM__CONTROLLER);

		df_assert ($result instanceof Mage_Core_Controller_Varien_Action);

		return $result;
	}



	/**
	 * @return Mage_Core_Controller_Response_Http
	 */
	protected function getResponse () {

		/** @var Mage_Core_Controller_Response_Http $result  */
		$result = $this->getController()->getResponse();

		df_assert ($result instanceof Mage_Core_Controller_Response_Http);

		return $result;
	}



	/**
	 * @return Df_Core_Model_InputRequest
	 */
	protected function getRmRequest () {

		if (!isset ($this->_rmRequest)) {

			/** @var Df_Core_Model_InputRequest $result  */
			$result =
				df_model (
					$this->getRmRequestClassMf ()
					,
					array (
						Df_Core_Model_InputRequest
							::PARAM__REQUEST => $this->getController()->getRequest()
					)
				)
			;


			df_assert ($result instanceof Df_Core_Model_InputRequest);

			$this->_rmRequest = $result;
		}


		df_assert ($this->_rmRequest instanceof Df_Core_Model_InputRequest);

		return $this->_rmRequest;

	}


	/**
	* @var Df_Core_Model_InputRequest
	*/
	private $_rmRequest;




	/**
	 * @return string
	 */
	protected function getRmRequestClassMf () {
		return Df_Core_Model_InputRequest::getNameInMagentoFormat();
	}



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM__CONTROLLER, 'Mage_Core_Controller_Varien_Action'
			)
		;
	}


	const PARAM__CONTROLLER = 'controller';




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Controller_Action';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


