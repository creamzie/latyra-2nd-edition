<?php

class Df_Core_Model_Controller_Action_Admin_Entity_Delete
	extends Df_Core_Model_Controller_Action_Admin_Entity {


	/**
	 * @override
	 * @return Df_Core_Model_Controller_Action_Admin_Entity_Delete
	 */
	public function process () {
		df_assert (!is_null ($this->getEntity()->getId()));
		try {
			$this->processDependencies();
			$this->getEntity()->delete();
			df_mage()->adminhtml()->session()
				->addSuccess (
					$this->getMessageSuccess()
				)
			;
		} catch (Exception $e) {
			df_mage()->adminhtml()->session()
				->addError (
					$e->getMessage()
				)
			;
		}
		$this->redirect ('*/*/');
		return $this;
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getEntityClassMf () {
		return $this->cfg (self::PARAM__ENTITY_CLASS_MF);
	}


	/**
	 * @return string
	 */
	private function getMessageSuccess () {
		return $this->cfg (self::PARAM__MESSAGE_SUCCESS, 'Объект удалён');
	}



	/**
	 * @return Df_Core_Model_Controller_Action_Admin_Entity_Delete
	 */
	private function processDependencies () {
		foreach ($this->getEntity()->getDependenciesInfo() as $dependencyInfo) {
			/** @var Df_Core_Model_Entity_Dependency $dependencyInfo */
			df_assert ($dependencyInfo instanceof Df_Core_Model_Entity_Dependency);

			if ($dependencyInfo->needDeleteCascade()) {

				/** @var Df_Core_Model_Entity $dependency */
				$dependency =
					$this->getEntity()->getDependencyByName (
						$dependencyInfo->getName()
					)
				;
				df_assert ($dependency instanceof Df_Core_Model_Entity);

				if (!is_null ($dependency->getId())) {
					$dependency->delete();
					/**
					 * Как ни странно — ядро Magento этого не делает.
					 */
					$dependency->unsetData();
				}

			}
		}
		return $this;
	}




	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__ENTITY_CLASS_MF, new Df_Zf_Validate_String()
			)
			->addValidator (
				self::PARAM__MESSAGE_SUCCESS, new Df_Zf_Validate_String(), $isRequired = false
			)
		;
	}

	const PARAM__ENTITY_CLASS_MF = 'entity_class_mf';
	const PARAM__MESSAGE_SUCCESS = 'message_success';


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Controller_Action_Admin_Entity_Delete';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


