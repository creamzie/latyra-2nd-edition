<?php

abstract class Df_Core_Model_Controller_Action_Admin_Entity_Save
	extends Df_Core_Model_Controller_Action_Admin_Entity {


	/**
	 * @abstract
	 * @return Df_Core_Model_Controller_Action_Admin_Entity_Save
	 */
	abstract protected function entityUpdate();

	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getFormClassMf();



	/**
	 * @override
	 * @throws Exception
	 * @return Df_Core_Model_Controller_Action_Admin_Entity_Save
	 */
	public function process () {
		df_assert (!df_empty ($this->getRequestParams()));
		try {
			$this->processDependencies();
			$this->entityUpdate();
			$this->getEntity()->save();
			$this->postProcessSuccess();
		}
		catch (Exception $e) {
			if ($this->isDependent()) {
				throw $e;
			}
			else {
				$this->handleException ($e);
			}
		}
		if (!$this->isDependent()) {
			$this->redirectToProperPage();
		}
		return $this;
	}



	/**
	 * @return Df_Core_Model_Form
	 */
	protected function getForm () {
		if (!isset ($this->_form)) {
			/** @var Df_Core_Model_Form $result  */
			$result =
				df_model (
					$this->getFormClassMf()
					,
					array (
						Df_Core_Model_Form::PARAM__ZEND_FORM_VALUES =>
							$this->getRequestParams()
					)
				)
			;
			df_assert ($result instanceof Df_Core_Model_Form);
			$this->_form = $result;
		}
		return $this->_form;
	}

	/**
	* @var Df_Core_Model_Form
	*/
	private $_form;


	/**
	 * @return string
	 */
	protected function getMessageSuccessForExistedEntity () {
		return 'Объект изменён';
	}


	/**
	 * @return string
	 */
	protected function getMessageSuccessForNewEntity() {
		return 'Объект добавлен';
	}


	/**
	 * @return string
	 */
	private function getMessageSuccess () {
		/** @var string $result */
		$result =
				$this->isEntityNew()
			?
				$this->getMessageSuccessForNewEntity()
			:
				$this->getMessageSuccessForExistedEntity()
		;
		df_result_string ($result);
		return $result;
	}



	/**
	 * @param Exception $e
	 * @return Df_Core_Model_Controller_Action_Admin_Entity_Save
	 */
	private function handleException (Exception $e) {
		df_exception_add_to_session ($e);
		df_mage()->adminhtml()->session()
			->setData (
				$this->getEntity()->getSessionKey()
				,
				$this->getRequestParams()
			)
		;
		$this->setNeedRedirectBack (true);
	    df_handle_entry_point_exception ($e, $rethrow = false);
		return $this;
	}



	/**
	 * @return bool
	 */
	private function isDependent () {
		return $this->cfg (self::PARAM__DEPENDENT, false);
	}

	
	
	/**
	 * @return bool
	 */
	private function isNeedRedirectBack () {
		if (!isset ($this->_needRedirectBack)) {
	
			/** @var bool $result */
			$result =
				df_parse_boolean (
					$this->getRequestParam ('back', false)
				)
			;
	
			df_result_boolean ($result);
			$this->_needRedirectBack = $result;
		}
		return $this->_needRedirectBack;
	}
	/** @var bool */
	private $_needRedirectBack;




	/**
	 * @return Df_Core_Model_Controller_Action_Admin_Entity_Save
	 */
	private function postProcessSuccess () {
		if (!$this->isDependent()) {
			df_mage()->adminhtml()->session()
				->addSuccess (
					$this->getMessageSuccess()
				)
				/**
				 * Так как мы необязательно возвращаемся на страницу редактирования сущности,
				 * где сессия сущности извлекается (на случай сбоев редактирования сущности)
				 * и очищается,
				 * а можем вместо этого вернуться на страницу списка товаров,
				 * где сессию сущности никто не использует и не очищает,
				 * то очищаем сессию сущности здесь.
				 */
				->unsetData (
					$this->getEntity()->getSessionKey()
				)
			;
		}
		return $this;
	}



	/**
	 * @return Df_Core_Model_Controller_Action_Admin_Entity_Save
	 */
	private function processDependencies () {
		foreach ($this->getEntity()->getDependenciesInfo() as $dependency) {
			/** @var Df_Core_Model_Entity_Dependency $dependency */
			df_assert ($dependency instanceof Df_Core_Model_Entity_Dependency);
			$this->processDependency ($dependency);
		}
		return $this;
	}



	/**
	 * @param Df_Core_Model_Entity_Dependency $dependency
	 * @return Df_Core_Model_Controller_Action_Admin_Entity_Save
	 */
	private function processDependency (Df_Core_Model_Entity_Dependency $dependency) {
		/** @var Df_Core_Model_Controller_Action_Admin_Entity_Save $dependencySaveAction */
		$dependencySaveAction =
			df_model (
				$dependency->getActionSaveClassNameMf()
				,
				array (
					Df_Core_Model_Controller_Action_Admin_Entity_Save::PARAM__CONTROLLER =>
						$this->getController()
					,
					Df_Core_Model_Controller_Action_Admin_Entity_Save::PARAM__DEPENDENT => true
					,
					Df_Core_Model_Controller_Action_Admin_Entity_Save::PARAM__REQUEST_PARAMS =>
						df_a (
							$this->getRequestParams()
							,
							$dependency->getName()
						)
				)
			)
		;
		df_assert (
				$dependencySaveAction
			instanceof
				Df_Core_Model_Controller_Action_Admin_Entity_Save
		);

		$dependencySaveAction->process();

		$this
			->setData (
				self::PARAM__REQUEST_PARAMS
				,
				array_merge (
					$this->getRequestParams()
					,
					array (
						$dependency->getEntityIdFieldName()	=> $dependencySaveAction->getEntity()->getId()
					)
				)
			)
		;

		return $this;
	}




	/**
	 * @return Df_Core_Model_Controller_Action_Admin_Entity_Save
	 */
	private function redirectToProperPage () {
		if (!$this->isNeedRedirectBack()) {
			$this
				->redirect (
					'*/*/'
				)
			;
		}
		else {
			$this
				->redirect (
					'*/*/edit'
					,
					array (
						/**
						 * Обратите внимание, что при запросе на редактирование сущности
						 * идентификатор сущности передаётся в адресе запроса
						 * параметром «id» (для красоты адреса),
						 * однако при запросе на сохранение сущности идентификатор сущности
						 * передаётсся в массиве POST параметром, имя которого соответствует
						 * имени идентификатора сущности (getIdFieldName())
						 */
						Df_Core_Controller_Admin_Entity
							::REQUEST_PARAM__ENTITY_ID => $this->getEntity()->getId()
					)
				)
			;
		}
		return $this;
	}




	/**
	 * @param bool $value
	 * @return Df_Core_Model_Controller_Action_Admin_Entity_Save
	 */
	private function setNeedRedirectBack ($value) {
		df_param_boolean ($value, 0);
		$this->_needRedirectBack = $value;
		return $this;
	}



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__DEPENDENT, new Df_Zf_Validate_Boolean(), $isRequired = false
			)
		;
	}

	const PARAM__DEPENDENT = 'dependent';




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Controller_Action_Admin_Entity_Save';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


