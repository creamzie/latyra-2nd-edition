<?php

class Df_Core_Model_Output_Css_Rule extends Df_Core_Model_Abstract {



	/**
	 * @return string
	 */
	public function __toString () {

		/** @var string $result */
		$result = $this->getText (true);

		return $result;
	}






	/**
	 * @param bool $inline [optional]
	 * @return string
	 */
	public function getText ($inline = true) {

		df_param_boolean ($inline, 0);

		if (!isset ($this->_text [$inline])) {

			/** @var string $result  */
			$result =
				implode (
					Df_Core_Const::T_EMPTY
					,
					array (
						implode (
							': '
							,
							array (
								$this->getName ()

								,
								implode (
									Df_Core_Const::T_EMPTY
									,
									array (
										$this->getValue()
										,
										$this->getUnits()
									)
								)
							)
						)
						,
						';'
					)
				)
			;


			if (!$inline) {
				$result .= "\r\n";
			}



			df_assert_string ($result);

			$this->_text [$inline] = $result;
		}


		df_result_string ($this->_text [$inline]);

		return $this->_text [$inline];

	}


	/**
	* @var array
	*/
	private $_text = array ();



	/**
	 * @return string
	 */
	public function getName () {

		if (!isset ($this->_name)) {

			/** @var string $result  */
			$result = $this->cfg (self::PARAM__NAME);

			if (is_array ($result)) {
				$result = implode ('-', $result);
			}

			df_result_string ($result);

			$this->_name = $result;
		}

		return $this->_name;

	}

	/**
	 * @var string
	 */
	private $_name;



	/**
	 * @return string
	 */
	public function getValue () {

		/** @var string $result  */
		$result = df_string ($this->cfg (self::PARAM__VALUE));

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	public function getUnits () {

		/** @var string $result  */
		$result =
			df_convert_null_to_empty_string (
				$this->cfg (self::PARAM__UNITS)
			)
		;

		return $result;
	}




	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
	    $this
			->addValidator (
				self::PARAM__UNITS,	new Df_Zf_Validate_String(), false
			)
		;
	}



	const PARAM__NAME = 'name';
	const PARAM__VALUE = 'value';
	const PARAM__UNITS = 'units';



	/**
	 * @static
	 * @param string|array $propertyName
	 * @param string|int|float $propertyValue
	 * @param string|null $propertyUnits [optional]
	 * @param bool $appendLineEnding [optional]
	 * @return string
	 */
	public static function compose (
		$propertyName
		,
		$propertyValue
		,
		$propertyUnits = null
		,
		$appendLineEnding = true
	) {

		/** @var Df_Core_Model_Output_Css_Rule $instance  */
		$instance =
			df_model (
				self::getNameInMagentoFormat()
				,
				array (
					self::PARAM__NAME => $propertyName
					,
					self::PARAM__VALUE => $propertyValue
					,
					self::PARAM__UNITS => $propertyUnits
				)
			)
		;

		/** @var string $result  */
		$result = $instance->getText (!$appendLineEnding);

		df_result_string ($result);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Output_Css_Rule';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}


