<?php

abstract class Df_Core_Model_Form_Builder extends Df_Core_Model_Abstract {


	/**
	 * @abstract
	 * @return Df_Core_Model_Form_Builder
	 */
	abstract protected function addFormFields ();


	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getEntityClass ();


	/**
	 * @return Varien_Data_Form_Element_Fieldset
	 */
	public function getFieldset () {
		return $this->cfg (self::PARAM__FIELDSET);
	}


	/**
	 * @return Varien_Data_Form
	 */
	public function getForm () {
		/** @var Varien_Data_Form $result  */
		$result = $this->getFieldset()->getForm();
		df_assert ($result instanceof Varien_Data_Form);
		return $result;
	}


	/**
	 * @return Df_Core_Model_Form_Builder
	 */
	public function run () {
		$this
			->addFormKey()
			->addFormFieldForEntityId()
			->addFormFields()
			->addFormData()
		;
		return $this;
	}


	/**
	 * @param string $name
	 * @param string $label
	 * @param string $type
	 * @param bool $required [optional]
	 * @param array $config [optional]
	 * @param bool $after [optional]
	 * @return Df_Core_Model_Form_Builder
	 */
	protected function addField (
		$name
		,
		$label
		,
		$type
		,
		$required = false
		,
		array $config = array ()
		,
		$after = false
	) {
		$this->getFieldset()
			->addField (
				$this->preprocessFieldId ($name)
				,
				$type
				,
				array_merge (
					array (
						Df_Varien_Data_Form_Element_Abstract::PARAM__NAME =>
							$this->preprocessFieldName ($name)
						,
						Df_Varien_Data_Form_Element_Abstract::PARAM__LABEL => $label
						,
						Df_Varien_Data_Form_Element_Abstract::PARAM__TITLE => $label
						,
						Df_Varien_Data_Form_Element_Abstract::PARAM__REQUIRED => $required
					)
					,
					$config
				)
				,
				$after
			)
		;

		return $this;
	}



	/**
	 * @return array
	 */
	protected function getDataCalculated () {
		return array ();
	}



	/**
	 * @return array
	 */
	protected function getDataDefault () {
		return array ();
	}


	
	
	/**
	 * @return Df_Core_Model_Entity
	 */
	protected function getEntity () {
	
		if (!isset ($this->_entity)) {
	
			/** @var Df_Core_Model_Entity $result  */
			$result = null;

			if (!$this->isDependent()) {
				$result = Mage::registry ($this->getEntityClass());
			}
			else {
				/** @var string $entityClass */
				$entityClass = $this->getEntityClass();

				$result = new $entityClass();

				if (!is_null ($this->getEntityParent()->getId())) {

					/** @var string $entityId */
					$entityIdAsString =
						/**
						 * Обратите внимание, что мы основываемся на уникальности
						 * имёт полей идентификаторов в рамках предметной области
						 * (другими словами, что поле идентификатора называются не просто «id»,
						 * а, например, «location_id», «warehouse_id»).
						 *
						 * Эта уникальность контролируется методом getIdFieldName()
						 */
						$this->getEntityParent()->getData (
							$result->getIdFieldName()
						)
					;
					df_assert (!df_empty ($entityIdAsString));

					/** @var int $entityId */
					$entityId = intval ($entityIdAsString);

					$result->load ($entityId);

					df_assert (0 < intval ($result->getId()));
				}
			}
	
			df_assert ($result instanceof Df_Core_Model_Entity);
	
			$this->_entity = $result;
		}
	
		df_assert ($this->_entity instanceof Df_Core_Model_Entity);
	
		return $this->_entity;
	}
	
	
	/**
	* @var Object
	*/
	private $_entity;



	
	/**
	 * @return array
	 */
	protected function getSessionData () {
	
		if (!isset ($this->_sessionData)) {
	
			/** @var array $result  */
			$result =
				df_mage()->adminhtml()->session()->getData (
					$this->getEntityClass()
					,
					$clear = true
				)
			;

			if (is_null ($result)) {
				$result = array ();
			}
	
			df_assert_array ($result);
	
			$this->_sessionData = $result;
		}
	
	
		df_result_array ($this->_sessionData);
	
		return $this->_sessionData;
	}
	
	
	/**
	* @var array
	*/
	private $_sessionData;



	/**
	 * @return bool
	 */
	protected function isDependent () {
		return $this->cfg (self::PARAM__DEPENDENT, false);
	}



	/**
	 * @param string $fieldId
	 * @return string
	 */
	protected function preprocessFieldId ($fieldId) {

		/** @var string $result  */
		$result =
			implode (
				'_'
				,
				df_clean (
					array (
						$this->getNamespace()
						,
						$fieldId
					)
				)
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @param string $fieldName
	 * @return string
	 */
	protected function preprocessFieldName ($fieldName) {

		/** @var string $result  */
		$result =
				df_empty ($this->getNamespace())
			?
				$fieldName
			:
				sprintf (
					'%s[%s]'
					,
					$this->getNamespace()
					,
					$fieldName
				)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @param string $classNameMf
	 * @param string $namespace
	 * @return Df_Core_Model_Form_Builder
	 */
	protected function runDependentBuilder ($classNameMf, $namespace) {

		/** @var Df_Core_Model_Form_Builder $result  */
		$result =
			df_model (
				$classNameMf
				,
				array (
					Df_Core_Model_Form_Location_Builder::PARAM__DEPENDENT => true
					,
					Df_Core_Model_Form_Location_Builder::PARAM__ENTITY_PARENT => $this->getEntity()
					,
					Df_Core_Model_Form_Location_Builder::PARAM__FIELDSET => $this->getFieldset()
					,
					Df_Core_Model_Form_Location_Builder::PARAM__NAMESPACE => $namespace
				)
			)
		;

		df_assert ($result instanceof Df_Core_Model_Form_Builder);

		$result->run();

		return $result;
	}




	/**
	 * @return Df_Core_Model_Form_Builder
	 */
	private function addFormData () {
		$this->getForm()
			->addValues (
				$this->getFormData()
			)
		;
		return $this;
	}



	/**
	 * @return Df_Core_Model_Form_Builder
	 */
	private function addFormFieldForEntityId () {

		if ($this->getEntity()->getId()) {
			$this->getFieldset()
				->addField (
					$this->preprocessFieldId (
						$this->getEntity()->getIdFieldName()
					)
					,
					Df_Varien_Data_Form_Element_Abstract::TYPE__HIDDEN
					,
					array (
						'name' =>
							$this->preprocessFieldName (
								$this->getEntity()->getIdFieldName()
							)
					)
				)
			;
		}

		return $this;
	}



	/**
	 * @return Df_Core_Model_Form_Builder
	 */
	private function addFormKey () {

		if (!$this->isDependent()) {
			$this->getFieldset()
				->addField (
					'form_key'
					,
					Df_Varien_Data_Form_Element_Abstract::TYPE__HIDDEN
					,
					array (
						'name' => 'form_key',
						'value' => df_mage()->core()->session()->getFormKey()
					)
				)
			;
		}

		return $this;
	}



	/**
	 * @return Df_Core_Model_Entity|null
	 */
	private function getEntityParent () {

		/** @var Df_Core_Model_Entity|null $result */
		$result = $this->cfg (self::PARAM__ENTITY_PARENT);

		if ($this->isDependent() || !is_null ($result)) {
			df_assert ($result instanceof Df_Core_Model_Entity);
		}

		return $result;
	}



	/**
	 * @return array
	 */
	private function getFormData () {
		/** @var array $result */
		$result = array ();
		foreach ($this->getFormDataRaw() as $fieldIdRaw => $fieldValue) {
			/** @var string $fieldIdRaw */
			/** @var string $fieldValue */

			/** @var string $fieldId */
			$fieldId = $this->preprocessFieldId ($fieldIdRaw);
			$result [$fieldId] = $fieldValue;
		}
		df_result_array ($result);
		return $result;
	}



	/**
	 * @return array
	 */
	private function getFormDataRaw () {

		/** @var array $result */
		$result = array ();

		if (!df_empty ($this->getSessionData())) {
			/**
			 * Наличие данных в сессии является следствием их невалидности.
			 * Администратор должен их исправить.
			 */
			$result = $this->getSessionData();
		}
		else if (is_null ($this->getEntity()->getId())) {
			$result = $this->getDataDefault();
		}
		else {
			$result =
				array_merge (
					$this->getEntity()->getData()
					,
					$this->getDataCalculated()
				)
			;
		}

		df_result_array ($result);
		return $result;
	}




	/**
	 * @return string
	 */
	private function getNamespace () {
		return $this->cfg (self::PARAM__NAMESPACE, Df_Core_Const::T_EMPTY);
	}



	/**
	 * @override
	 * @return void
	 */
    protected function _construct() {
		parent::_construct();
        $this
			->addValidator (
				self::PARAM__DEPENDENT, new Df_Zf_Validate_Boolean(), $isRequired = false
			)
			->validateClass (
				self::PARAM__ENTITY_PARENT
				,
				Df_Core_Model_Entity::getClass()
				,
				$isRequired = false
			)
			->validateClass (
				self::PARAM__FIELDSET, 'Varien_Data_Form_Element_Fieldset'
			)
			->addValidator (
				self::PARAM__NAMESPACE, new Df_Zf_Validate_String(), $isRequired = false
			)
		;
    }


	const PARAM__DEPENDENT = 'dependent';
	const PARAM__ENTITY_PARENT = 'entity_parent';
	const PARAM__FIELDSET = 'fieldset';
	const PARAM__NAMESPACE = 'namespace';




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Form_Builder';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


