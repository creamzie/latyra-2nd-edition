<?php

class Df_Core_Model_Form_Location_Builder extends Df_Core_Model_Form_Builder {


	/**
	 * @override
	 * @return Df_Core_Model_Form_Builder
	 */
	protected function addFormFields () {

		$this->getFieldset()
			->addType (
				Df_Varien_Data_Form_Element_Map::getClass()
				,
				Df_Varien_Data_Form_Element_Map::getClass()
			)
		;

		$this
			->addField (
				$name = Df_Core_Model_Location::PARAM__CITY
				,
				$label = Df_Core_Form_Location::LABEL__CITY
				,
				$type = Df_Varien_Data_Form_Element_Abstract::TYPE__TEXT
				,
				$required = true
			)
			->addField (
				$name = Df_Core_Model_Location::PARAM__STREET_ADDRESS
				,
				$label = Df_Core_Form_Location::LABEL__STREET_ADDRESS
				,
				$type = Df_Varien_Data_Form_Element_Abstract::TYPE__TEXTAREA
				,
				$required = true
				,
				$config = array (
					'style' => 'height: 3em;'
				)
			)
			->addField (
				$name = Df_Core_Form_Location::FIELD__MAP
				,
				$label = Df_Core_Form_Location::LABEL__MAP
				,
				$type = Df_Varien_Data_Form_Element_Map::getClass()
				,
				$required = false
				,
				$config = array (
				)
			)
		;
		return $this;
	}



	/**
	 * @override
	 * @return array
	 */
	protected function getDataDefault () {

		/** @var array $result  */
		$result =
			array_merge (
				parent::getDataDefault()
				,
				array (
					Df_Core_Model_Location::PARAM__CITY => Df_Core_Const::T_EMPTY
				)
			)
		;

		df_result_array ($result);

		return $result;
	}



	/**
	 * @override
	 * @return Df_Core_Model_Location
	 */
	protected function getEntity () {

		/** @var Df_Core_Model_Location $result  */
		$result = parent::getEntity();

		df_assert ($result instanceof Df_Core_Model_Location);

		return $result;
	}



	/**
	 * @override
	 * @return string
	 */
	protected function getEntityClass () {
		return Df_Core_Model_Location::getClass();
	}




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Form_Location_Builder';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


