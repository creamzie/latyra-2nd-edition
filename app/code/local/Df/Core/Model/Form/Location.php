<?php

class Df_Core_Model_Form_Location extends Df_Core_Model_Form {

	/**
	 * @return string
	 */
	public function getCity () {
		/** @var string $result  */
		$result =
			df_convert_null_to_empty_string (
				$this->getField (
					Df_Core_Model_Location::PARAM__CITY
				)
			)
		;
		df_result_string ($result);
		return $result;
	}



	/**
	 * @return string
	 */
	public function getStreetAddress () {
		/** @var string $result  */
		$result =
			df_convert_null_to_empty_string (
				$this->getField (
					Df_Core_Model_Location::PARAM__STREET_ADDRESS
				)
			)
		;
		df_result_string ($result);
		return $result;
	}



	/**
	 * @override
	 * @return string
	 */
	protected function getZendFormClass() {
		return 'Df_Core_Form_Location';
	}


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Form_Location';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


