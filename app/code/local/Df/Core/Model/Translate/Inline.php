<?php

class Df_Core_Model_Translate_Inline extends Mage_Core_Model_Translate_Inline {


    /**
     * Prepare other text inline translates
     * @return string
     */
    protected function _otherText()
    {
		/** @var string */
		return
				(df_enabled (Df_Core_Feature::LOCALIZATION) && df_cfg()->localization()->translation()->getFixInlineTranslation())
			?
				$this->_otherTextDf ()
			:
				parent::_otherText ()
		;
    }


    /**
     * Prepare other text inline translates
     * @return string
     */
    protected function _otherTextDf ()
    {
	    if ($this->getIsJson()) {
            $quoteHtml = '\"';
        } else {
            $quoteHtml = '"';
        }

        $next = 0;
        $m    = array();
        while (preg_match('#(>|title=\")*('.$this->_tokenRegex.')#', $this->_content, $m, PREG_OFFSET_CAPTURE, $next)) {

	        // BEGIN PATCH
	        $tr = '{shown:\''.$this->_escape($m[3][0]).'\','
	            .'translated:\''.$this->_escape($m[4][0]).'\','
	            .'original:\''.$this->_escape($m[5][0]).'\','
	            .'location:\'Text\','
	            .'scope:\''.$this->_escape($m[6][0]).'\'}';
	        $spanHtml = '<span translate='.$quoteHtml.'['.$tr.']'.$quoteHtml.'>'.$m[3][0].'</span>';
	        // END PATCH


            $this->_content = substr_replace($this->_content, $spanHtml, $m[2][1], strlen($m[2][0]) );
            $next = $m[0][1];
        }

    }

}