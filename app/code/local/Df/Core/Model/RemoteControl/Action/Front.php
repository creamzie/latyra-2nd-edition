<?php

class Df_Core_Model_RemoteControl_Action_Front extends Df_Core_Model_RemoteControl_Action {


	/**
	 * @override
	 * @return Df_Core_Model_RemoteControl_Action
	 */
	public function process () {

		try {
			$this->getActionConcrete()->process();

			Df_Core_Model_RemoteControl_MessageSerializer_Http
				::serializeMessageResponse (
					$this->getResponse()
					,
					$this->getActionConcrete()->getMessageResponse()
				)
			;
		}
		catch (Exception $e) {
			df_handle_entry_point_exception ($e);
		}

		return $this;
	}



	
	/**
	 * @return Df_Core_Model_RemoteControl_Action_Concrete
	 */
	private function getActionConcrete () {
	
		if (!isset ($this->_actionConcrete)) {
	
			/** @var Df_Core_Model_RemoteControl_Action_Concrete $result  */
			$result = 	
				df_model (
					$this->getMessageRequest()->getActionClassMf()
					,
					array (
						Df_Core_Model_RemoteControl_Action_Concrete
							::PARAM__CONTROLLER => $this->getController()
						,
						Df_Core_Model_RemoteControl_Action_Concrete
							::PARAM__MESSAGE_REQUEST => $this->getMessageRequest()
					)
				)
			;
	
			df_assert ($result instanceof Df_Core_Model_RemoteControl_Action_Concrete);
	
			$this->_actionConcrete = $result;
		}
	
		df_assert ($this->_actionConcrete instanceof Df_Core_Model_RemoteControl_Action_Concrete);
	
		return $this->_actionConcrete;
	}
	
	
	/**
	* @var Df_Core_Model_RemoteControl_Action_Concrete
	*/
	private $_actionConcrete;	




	/**
	 * @return Df_Core_Model_RemoteControl_Message_Request
	 */
	private function getMessageRequest () {

		if (!isset ($this->_messageRequest)) {

			/** @var Df_Core_Model_RemoteControl_Message_Request $result  */
			$result =
				Df_Core_Model_RemoteControl_MessageSerializer_Http::restoreMessageRequest (
					$this->getController()->getRequest()
				)
			;

			df_assert ($result instanceof Df_Core_Model_RemoteControl_Message_Request);

			$this->_messageRequest = $result;
		}

		df_assert ($this->_messageRequest instanceof Df_Core_Model_RemoteControl_Message_Request);

		return $this->_messageRequest;
	}


	/**
	* @var Df_Core_Model_RemoteControl_Message_Request
	*/
	private $_messageRequest;



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_RemoteControl_Action_Front';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


