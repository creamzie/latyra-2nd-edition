<?php

abstract class Df_Core_Model_RemoteControl_Message_Request extends Df_Core_Model_RemoteControl_Message {

	/**
	 * @abstract
	 * @return string
	 */
	abstract public function getActionClassMf();




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_RemoteControl_Message_Request';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


