<?php

class Df_Core_Model_RemoteControl_Message_Response extends Df_Core_Model_RemoteControl_Message {


	/**
	 * @return string
	 */
	public function getText () {

		/** @var string $result */
		$result = $this->cfg (self::PARAM__TEXT);

		if (df_empty($result)) {
			$result = Df_Core_Const::T_EMPTY;
		}

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	public function isOk () {
		return $this->cfg (self::PARAM__IS_OK);
	}



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__IS_OK, new Df_Zf_Validate_Boolean()
			)
			->addValidator (
				self::PARAM__TEXT, new Df_Zf_Validate_String(), $isRequired = false
			)
		;
	}



	const PARAM__IS_OK = 'is_ok';
	const PARAM__TEXT = 'text';


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_RemoteControl_Message_Response';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


