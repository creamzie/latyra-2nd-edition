<?php

abstract class Df_Core_Model_RemoteControl_Request extends Df_Core_Model_Abstract {

	/**
	 * @abstract
	 * @return Zend_Uri_Http
	 */
	abstract protected function createUri();



	/**
	 * @return Df_Core_Model_RemoteControl_Message_Response
	 */
	public function getMessageResponse () {
		return $this->_messageResponse;
	}

	/**
	 * @var Df_Core_Model_RemoteControl_Message_Response
	 */
	private $_messageResponse;



	/**
	 * @return Df_Core_Model_RemoteControl_Message_Response
	 */
	public function send () {

		/** @var Df_Core_Model_RemoteControl_Message_Response $result  */
		$result = null;


		/** @var Zend_Http_Response $response  */
		$response =
			$this->getHttpClient()->request (
				Zend_Http_Client::POST
			)
		;

		/** @var Df_Core_Model_RemoteControl_Message_Response $result  */
		$result =
			Df_Core_Model_RemoteControl_MessageSerializer_Http::restoreMessageResponse (
				$response
			)
		;

		df_assert ($result instanceof Df_Core_Model_RemoteControl_Message_Response);


		if (!$result->isOk()) {
			df_error ($result->getText());
		}

		/**
		 * Надо бы ещё проверять, что данные пришли именно от нашего сервера
		 * (то есть, контролировать подпись данных)
		 */

		$this->_messageResponse = $result;

		return $result;
	}



	/**
	 * @return Zend_Http_Client
	 */
	protected function createHttpClient () {

		/** @var Zend_Http_Client $result  */
		$result = new Zend_Http_Client ();


		Df_Core_Model_RemoteControl_MessageSerializer_Http
			::serializeMessageRequest (
				$result
				,
				$this->getMessageRequest()
			)
		;


		$result
			->setHeaders (
				array (
					'Accept' => 'application/octet-stream'
					,
					'Accept-Encoding' => 'gzip, deflate'
					,
					'Accept-Language' => 'en-us,en;q=0.5'
					,
					'Connection' => 'keep-alive'
					,
					'Host' =>
						/**
						 * Маскируемся доменом того магазина, к которому обращаемся
						 */
						$this->getUri()->getHost()
					,
					'Referer' =>
						/**
						 * Маскируемся адресом того магазина, к которому обращаемся
						 */
						Mage::app()->getRequest()->getRequestUri()
					,
					'User-Agent' => Df_Core_Const::FAKE_USER_AGENT
				)
			)
			->setUri ($this->getUri())
			->setConfig (
				array (

					/**
					 * в секундах
					 */
					'timeout' => 10
				)
			)
		;

		df_assert ($result instanceof Zend_Http_Client);

		return $result;
	}



	/**
	 * @return Df_Core_Model_RemoteControl_Message_Request
	 */
	protected function getMessageRequest () {
		return $this->cfg (self::PARAM__MESSAGE_REQUEST);
	}



	/**
	 * @return Zend_Uri_Http
	 */
	protected function getUri () {

		if (!isset ($this->_uri)) {

			/** @var Zend_Uri_Http $result  */
			$result = $this->createUri ();

			df_assert ($result instanceof Zend_Uri_Http);

			$this->_uri = $result;
		}

		df_assert ($this->_uri instanceof Zend_Uri_Http);

		return $this->_uri;
	}


	/**
	* @var Zend_Uri_Http
	*/
	private $_uri;




	/**
	 * @return Zend_Http_Client
	 */
	private function getHttpClient () {
	
		if (!isset ($this->_httpClient)) {
	
			/** @var Zend_Http_Client $result  */
			$result = $this->createHttpClient ();
	
			df_assert ($result instanceof Zend_Http_Client);
	
			$this->_httpClient = $result;
		}
	
		df_assert ($this->_httpClient instanceof Zend_Http_Client);
	
		return $this->_httpClient;
	}
	
	
	/**
	* @var Zend_Http_Client
	*/
	private $_httpClient;	
	


	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM__MESSAGE_REQUEST
				,
				Df_Core_Model_RemoteControl_Message_Request::getClass()
			)
		;
	}


	const PARAM__MESSAGE_REQUEST = 'message_request';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_RemoteControl_Request';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


