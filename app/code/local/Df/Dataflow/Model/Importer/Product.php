<?php

class Df_Dataflow_Model_Importer_Product extends Df_Dataflow_Model_Importer_Row {

	/**
	 * @return Df_Catalog_Model_Product
	 */
	public function getProduct () {

		if (!isset ($this->_product)) {

			/** @var Df_Catalog_Model_Product $result  */
			$result =
				df_model (
					Df_Catalog_Const::DF_PRODUCT_CLASS_MF
				)
			;


			if (!($result instanceof Df_Catalog_Model_Product)) {
				df_error (
					sprintf (
								"Объект-товар должен относиться к классу «%s», однако относится к классу «%s»."
							.
								"\nВозможно, один из сторонних модулей переопределил класс объекта-товара."
							.
								"\nОбратитесь к специалисту."
						,
							is_object ($result) ? get_class($result) : gettype ($result)
						,
							Df_Catalog_Const::DF_PRODUCT_CLASS

					)
				);
			}


			df_helper()->eav()->cleanCache();


			/**
			 * Перед загрузкой товара из базы данных устанавливаем для него магазин.
			 */
			$result
				->setDataUsingMethod (
					Df_Catalog_Const::PRODUCT_PARAM_STORE_ID
					,
					$this->getRow()->getStore()->getId ()
				)
			;



			if (!$this->getRow()->isProductNew()) {

				$result->load ($this->getRow()->getId ());

				if ($result->getId () !== $this->getRow()->getId ()) {
					df_error (
						sprintf (
							'Не могу загрузить из базы данных товар с артикулом «%s» и идентификатором «%s».'
							,
							$this->getRow()->getSku()
							,
							$this->getRow()->getId()
						)
					);
				}
			}


			else {

				/**
				 * Новый товар
				 */


				/*******************************************************
				 * тип товара
				 */

				df_assert (
					!is_null ($this->getRow()->getProductType())
					,
					sprintf (
						"Для нового товара вы должны обязательно указать его тип в поле «%s»"
						,
						Df_Dataflow_Model_Import_Product_Row::FIELD__PRODUCT_TYPE
					)

				)
				;


				$result
					->setDataUsingMethod (
						Df_Catalog_Const::PRODUCT_PARAM_TYPE_ID
						,
						/**
						 * Обратите внимание, что идентификатором типа товаров является строка, а не число.
						 * Пример идентификатарора: «simple», «bundle».
						 */
						$this->getRow()->getProductType()
					)
				;

				/**
				 * тип товара
				 *******************************************************/



				/*******************************************************
				 * идентификатор набора характеристик
				 */

				df_assert (
					!is_null ($this->getRow()->getAttributeSetId ())
					,
					sprintf (
						"Для нового товара вы должны обязательно указать название его набора характеристик в поле «%s»"
						,
						Df_Dataflow_Model_Import_Product_Row::FIELD__ATTRIBUTE_SET
					)

				)
				;


				$result
					->setDataUsingMethod (
						Df_Catalog_Const::PRODUCT_PARAM_ATTRIBUTE_SET_ID
						,
						/**
						 * Обратите внимание, что идентификатором типа товаров является строка, а не число.
						 * Пример идентификатарора: «simple», «bundle».
						 */
						$this->getRow()->getAttributeSetId ()
					)
				;

				/**
				 * идентификатор набора характеристик
				 *******************************************************/


				$result->setData ('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
			}


			$this->_product = $result;
		}


		df_assert ($this->_product instanceof Df_Catalog_Model_Product);

		return $this->_product;

	}


	/**
	* @var Df_Catalog_Model_Product
	*/
	private $_product;



	/**
	 * @override
	 * @return Df_Dataflow_Model_Importer_Row
	 */
	public function import () {

		try {

			if ($this->getRow()->isProductNew()) {

				/**
				 * Убеждаемся в присутствии значений обязательных для нового товара полей
				 */
				$this->assertRequiredAttributesExistence();
			}

			$this->importCategoriesUsingStandardTechnology ();

			$this->importCategoriesUsingAdvancedTechnology ();

			$this->initWebsiteAttributeForProduct ();

			$this->importAttributeValues ();

			if (!$this->getProduct()->getDataUsingMethod ('visibility')) {
				$this->getProduct()
					->getDataUsingMethod (
						Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE
					)
				;
			}

			$this->importInventoryData();


			df_helper()->catalog()->product()
				->save (
					$this->getProduct()
					,
					$isMassUpdate = true
				)
			;



			$this->importImages ();


			$this->importCustomOptions();


			$this->importBundleData ();



			/**
			 * Применяем ценовые правила
			 */
			$this->getProduct()->unsetData ('is_massupdate');

			/** @var Mage_CatalogRule_Model_Rule $catalogRule  */
			$catalogRule = df_model ('catalogrule/rule');

			df_assert ($catalogRule instanceof Mage_CatalogRule_Model_Rule);

			$catalogRule->applyAllRulesToProduct ($this->getProduct()->getId());
		}

		catch (Exception $e) {
			df_handle_entry_point_exception ($e, true);
		}

		return $this;
	}



	/**
	 * @return Df_Dataflow_Model_Importer_Product
	 */
	private function importBundleData () {
		
		
		if ('bundle' === $this->getProduct()->getTypeId()) {

			
			$this->reloadProduct();
	
	
			$this->getBundleImporter ()
				->process ()
			;
	
			df_helper()->catalog()->product()
				->save (
					$this->getProduct()
					,
					$isMassUpdate = true
				)
			;
			
		}

		return $this;
	}
	

	
	
	/**
	 * @return Df_Dataflow_Model_Importer_Product_Bundle
	 */
	private function getBundleImporter () {
	
		if (!isset ($this->_bundleImporter)) {
	
			/** @var Df_Dataflow_Model_Importer_Product_Bundle $result  */
			$result = 
				df_model (
					Df_Dataflow_Model_Importer_Product_Bundle::getNameInMagentoFormat()
					,

					array (
						Df_Dataflow_Model_Importer_Product_Bundle
							::PARAM_PRODUCT => $this->getProduct()

						,
						Df_Dataflow_Model_Importer_Product_Bundle
							::PARAM_IMPORTED_ROW => $this->getRow()->getAsArray()
					)
				)
			;

			df_assert ($result instanceof Df_Dataflow_Model_Importer_Product_Bundle);
	
			$this->_bundleImporter = $result;
		}
	
		df_assert ($this->_bundleImporter instanceof Df_Dataflow_Model_Importer_Product_Bundle);
	
		return $this->_bundleImporter;
	}
	
	
	/**
	* @var Df_Dataflow_Model_Importer_Product_Bundle
	*/
	private $_bundleImporter;	
	
	



	/**
	 * @return Df_Dataflow_Model_Importer_Product
	 */
	private function importCustomOptions () {

        if (
				df_enabled (
					Df_Core_Feature::DATAFLOW_CO
					, 
					$this->getRow()->getStore()->getId()
				) 
			&& 
				df_cfg()->dataflow()->products()->getCustomOptionsSupport ()
		) {
			
			
			$this->reloadProduct();
			
			
			$this->getCustomOptionsImporter ()
				->process ()
			;

			df_helper()->catalog()->product()
				->save (
					$this->getProduct()
					,
					$isMassUpdate = true
				)
			;
        }

		return $this;
	}
	



	
	
	
	/**
	 * @return Df_Dataflow_Model_Importer_Product_Options
	 */
	private function getCustomOptionsImporter () {
	
		if (!isset ($this->_customOptionsImporter)) {
	
			/** @var Df_Dataflow_Model_Importer_Product_Options $result  */
			$result = 
				df_model (
					Df_Dataflow_Model_Importer_Product_Options::getNameInMagentoFormat()
					,

					array (
						Df_Dataflow_Model_Importer_Product_Options
							::PARAM_PRODUCT => $this->getProduct()

						,
						Df_Dataflow_Model_Importer_Product_Options
							::PARAM_IMPORTED_ROW => $this->getRow()->getAsArray()
					)
				)
			;

			df_assert ($result instanceof Df_Dataflow_Model_Importer_Product_Options);
	
			$this->_customOptionsImporter = $result;
		}
	
		df_assert ($this->_customOptionsImporter instanceof Df_Dataflow_Model_Importer_Product_Options);
	
		return $this->_customOptionsImporter;
	}
	
	
	/**
	* @var Df_Dataflow_Model_Importer_Product_Options
	*/
	private $_customOptionsImporter;	







	/**
	 * @throws Exception
	 * @return Df_Dataflow_Model_Importer_Product
	 */
	private function importImages () {

        $this->reloadProduct();


		/** @var array $primaryImages  */
		$primaryImages =
			$this->getGalleryImporter()->getPrimaryImages ()
		;


	    if (0 < count ($primaryImages)) {

			if (
					df_enabled (
						Df_Core_Feature::DATAFLOW_IMAGES
						,
						$this->getRow()->getStore()->getId()
					)
				&&
					df_cfg()->dataflow()->products()->getDeletePreviousImages()
			) {
				//remove previous images
				$this->getProduct()
					->deleteImages ()
				;
			}


			foreach ($primaryImages as $file => $fields) {

				/** @var string $file  */
				/** @var array $fields  */

				df_assert_string ($file);
				df_assert_array ($fields);

				$imagePath =
					Mage::getBaseDir('media') . DS . 'import' . trim ($file)
				;

				if (!is_file ($imagePath)) {
					throw new Exception (
						sprintf (
							"Image file %s does not exist"
							,
							$imagePath
						)
					)
					;
				}

				try {
					$this->getProduct()
						->addImageToMediaGallery(
							$imagePath
							,
							array ('thumbnail','small_image','image')
							,
							false
							,
							false
						)
					;
				}
				catch (Exception $e) {
					df_handle_entry_point_exception ($e, false);
				}
			}


			if (
					df_enabled (
						Df_Core_Feature::DATAFLOW_IMAGES
						,
						$this->getRow()->getStore()->getId()
					)
				&&
					df_cfg()->dataflow()->products()->getGallerySupport()
			) {

				$this->getGalleryImporter()->addAdditionalImagesToProduct ();

			}

			df_helper()->catalog()->product()
				->save (
					$this->getProduct()
					,
					$isMassUpdate = true
				)
			;
	    }

		return $this;
	}






	/**
	 * @return Df_Dataflow_Model_Importer_Product_Gallery
	 */
	private function getGalleryImporter () {
	
		if (!isset ($this->_galleryImporter)) {
	
			/** @var Df_Dataflow_Model_Importer_Product_Gallery $result  */
			$result = 
				df_model (
					Df_Dataflow_Model_Importer_Product_Gallery::getNameInMagentoFormat ()
					,
					array (
						Df_Dataflow_Model_Importer_Product_Gallery::PARAM_PRODUCT => $this->getProduct()
						,
						Df_Dataflow_Model_Importer_Product_Gallery::PARAM_IMPORTED_ROW => $this->getRow()->getAsArray()
					)
				)
			;
	
	
			df_assert ($result instanceof Df_Dataflow_Model_Importer_Product_Gallery);
	
			$this->_galleryImporter = $result;
		}
	
		df_assert ($this->_galleryImporter instanceof Df_Dataflow_Model_Importer_Product_Gallery);
	
		return $this->_galleryImporter;
	}
	
	
	/**
	* @var Df_Dataflow_Model_Importer_Product_Gallery
	*/
	private $_galleryImporter;
	







	/**
	 * @return Df_Dataflow_Model_Importer_Product
	 */
	private function importInventoryData () {

		/** @var array $stockData  */
		$stockData = array();


		foreach ($this->getInventoryFields() as $inventoryField) {

			/** @var string $inventoryField */

			df_assert_string ($inventoryField);


			/** @var string|null $inventoryFieldValue  */
			$inventoryFieldValue = $this->getRow()->getFieldValue ($inventoryField);

			if (!is_null ($inventoryFieldValue)) {

				if (
					in_array (
						$inventoryField
						,
						df_helper()->catalog()->product_dataflow()->getNumericFields())
				) {
					$inventoryFieldValue = $this->getRow()->parseAsNumber ($inventoryFieldValue);
				}

				$stockData[$inventoryField] = $inventoryFieldValue;
			}
		}

		$this->getProduct()
			->setDataUsingMethod ('stock_data', $stockData)
		;

		return $this;
	}







	/**
	 * @return array
	 */
	private function getInventoryFields () {

		/** @var array $result  */
		$result =
			df_helper()->catalog()->product_dataflow()->getInventoryFieldsByProductType(
				$this->getProduct()->getTypeId()
			)
		;

		df_result_array ($result);

		return $result;
	}





	/**
	 * @return Df_Dataflow_Model_Importer_Product
	 */
	private function reloadProduct () {

		$this->_product =
			df_helper()->catalog()->product()->reload (
				$this->getProduct()
			)
		;

		return $this;
	}







	/**
	 * @override
	 * @return Df_Dataflow_Model_Import_Product_Row
	 */
	protected function getRow () {

		/** @var Df_Dataflow_Model_Import_Product_Row $result  */
		$result = parent::getRow();

		df_assert ($result instanceof Df_Dataflow_Model_Import_Product_Row);

		return $result;
	}







	/**
	 * @return Df_Dataflow_Model_Importer_Product
	 */
	private function initWebsiteAttributeForProduct () {

		$this
			->initWebsiteAttributeFromStore()
			->initWebsiteAttributeFromWebsiteIdsField()
		;

		return $this;
	}







	/**
	 * @return Df_Dataflow_Model_Importer_Product
	 */
	private function importCategoriesUsingStandardTechnology () {


		/** @var string|null $categoryIdsAsString  */
		$categoryIdsAsString = $this->getRow()->getCategoryIdsAsString ();

		if (!is_null ($categoryIdsAsString)) {
			$this->getProduct()->setCategoryIds($categoryIdsAsString);
		}

		return $this;
	}






	/**
	 * @return Df_Dataflow_Model_Importer_Product
	 */
	private function importCategoriesUsingAdvancedTechnology () {

		if (
				df_enabled (
					Df_Core_Feature::DATAFLOW_CATEGORIES
					,
					$this->getRow()->getStore ()->getId ()
				)
			&&
				df_cfg()->dataflow()->products()->getEnhancedCategorySupport()
		) {

			/** @var Df_Dataflow_Model_Importer_Product_Categories $categoriesImporter */
			$categoriesImporter =
				df_model (
					Df_Dataflow_Model_Importer_Product_Categories::getNameInMagentoFormat()
					,
					array (
						Df_Dataflow_Model_Importer_Product_Specialized
							::PARAM_PRODUCT => $this->getProduct()
						,
						Df_Dataflow_Model_Importer_Product_Specialized
							::PARAM_IMPORTED_ROW  => $this->getRow()->getAsArray ()
						,
						Df_Dataflow_Model_Importer_Product_Categories
							::PARAM_STORE => $this->getRow()->getStore ()
					)
				)

			;

			$categoriesImporter->process ();
		}

		return $this;
	}








	/**
	 * @return Df_Dataflow_Model_Importer_Product
	 */
	private function initWebsiteAttributeFromStore () {

		/** @var array $websiteIds  */
		$websiteIds = $this->getProduct()->getWebsiteIds();

		df_assert_array ($websiteIds);


		if (!in_array ($this->getRow()->getStore()->getWebsiteId(), $websiteIds)) {

			/**
			 * Это условие заимствовано из стандартного кода.
			 * Ценность не вполне осознана мной.
			 */
			if (0 !== $this->getRow()->getStore()->getId ()) {

				$websiteIds []= $this->getRow()->getStore()->getWebsiteId();

				$this->getProduct ()->setDataUsingMethod ('website_ids', $websiteIds);

			}
		}

		return $this;
	}





	/**
	 * @return Df_Dataflow_Model_Importer_Product
	 */
	private function initWebsiteAttributeFromWebsiteIdsField () {

		if (0 < count ($this->getRow()->getWebsites())) {

			/** @var array $websiteIds  */
			$websiteIds = $this->getProduct()->getWebsiteIds();

			if (
					is_null ($websiteIds)
				||
					/**
					 * Странное условие.
					 * Присутствует в оригинальном коде ядра.
					 */
					(0 === $this->getRow()->getStore()->getId ())
			) {

				$websiteIds = array ();

			}

			df_assert_array ($websiteIds);



			foreach ($this->getRow()->getWebsites() as $website) {

				/** @var Mage_Core_Model_Website $website  */


				if (!in_array($website->getId(), $websiteIds)) {
					$websiteIds[] = $website->getId();
				}

			}

			$this->getProduct()->setDataUsingMethod ('website_ids', $websiteIds);
		}

		return $this;
	}








	/**
	 * @throws Exception
	 * @return Df_Dataflow_Model_Importer_Product
	 */
	private function importAttributeValues () {

		/********************************************************************
		 * Заплатка для локали.
		 * Стандартный программный код приводит к проблеме:
		 * если в импортируемом файле значения опций записаны в фомате одной локали,
		 * а при импорте установлена другая локаль, то значения не будут распознаны.
		 *
		 * Данная заплатка позволяет администратору устанавливать локаль для опций
		 * в профиле Magento Dataflow.
		 *
		 * Пример:
		 *
			<action type="vichy_dataflow/import_products_parser" method="parse">
				<var name="adapter">catalog/convert_adapter_product</var>
				<var name="method">parse</var>
				<var name="locale">en_US</var>
			</action>
		 */


		/** @var string $originalLocaleCode */
		$originalLocaleCode = df_mage()->core()->translate()->getLocale ();

		/** @var Exception $exception  */
		$exception = null;

		df_assert_string ($originalLocaleCode);


		/** @var string|null $localeCodeFromBatchParams  */
		$localeCodeFromBatchParams = $this->getConfigParam('locale');

		if (!is_null ($localeCodeFromBatchParams)) {
			df_assert_string ($localeCodeFromBatchParams);

			df_mage()->core()->translate()
				->setLocale ($localeCodeFromBatchParams)
				->init('adminhtml', true)
			;
		}


		try {

			foreach ($this->getRow()->getAsArray() as $fieldName => $fieldValue) {

				/** @var string $fieldName */
				/** @var string|float|int|null $fieldValue */

				df_assert_string ($fieldName);


				if (
					in_array (
						$fieldName
						,
						df_helper()->catalog()->product_dataflow()->getInventoryFields()
					)
				) {
					continue;
				}


				if (
					in_array (
						$fieldName
						,
						df_helper()->catalog()->product_dataflow()->getIgnoredFields()
					)
				) {
					continue;
				}


				if (is_null ($fieldValue)) {
					continue;
				}


				/** @var Mage_Eav_Model_Entity_Attribute|null $fieldAttribute  */
				$fieldAttribute = $this->getAttributeForField ($fieldName);

				if (is_null ($fieldAttribute)) {
					continue;
				}

				df_assert ($fieldAttribute instanceof Mage_Eav_Model_Entity_Attribute);


				/** @var bool $isArray  */
				$isArray = false;

				/** @var string|array $valueToSet  */
				$valueToSet = $fieldValue;


				if ('multiselect' === $fieldAttribute->getDataUsingMethod ('frontend_input')) {

					$fieldValue =
						explode (
							Mage_Catalog_Model_Convert_Adapter_Product::MULTI_DELIMITER
							,
							$fieldValue
						)
					;

					$isArray = true;

					$valueToSet = array();

				}



				if ('decimal' === $fieldAttribute->getBackendType()) {

					$valueToSet =
						$this->getRow()->parseAsNumber($fieldValue)
					;

				}


				if ($fieldAttribute->usesSource()) {

					/**
					 * Данный программный код приводит к проблеме:
					 * если в импортируемом файле значения опции записаны в фомате одной локали,
					 * а при импорте установлена другая локаль, то значения не будут распознаны.
					 */

					/** @var Mage_Eav_Model_Entity_Attribute_Source_Abstract $source  */
					$source = $fieldAttribute->getSource();


					/** @var array $options  */
					$options =

						/**
						 * Как ни странно, хотя базовый интерфейс Mage_Eav_Model_Entity_Attribute_Source_Interface
						 * определяет getAllOptions как метод без параметров,
						 * класс Mage_Core_Model_Design_Source_Design переопределяет этот метод как метод с параметром,
						 * причём значением по умолчанию является true.
						 *
						 * Не уверен, что вызов getAllOptions(false) — это лучшее решение, но так делает стандартный код.
						 */
						$source->getAllOptions (false)
					;

					df_assert_array ($options);



					if ($isArray) {


						foreach ($options as $option) {

							/** @var array $option */

							df_assert_array ($option);

							if (
								in_array (
									df_a ($option, 'label')
									,
									$fieldValue
								)
							) {
								$valueToSet[] = df_a ($option, 'value');
							}
						}
					}

					else {

						$valueToSet = false;


						foreach ($options as $option) {

							/** @var array $option */

							df_assert_array ($option);

							if (df_a ($option, 'label') === $fieldValue) {
								$valueToSet = df_a ($option, 'value');
								break;
							}
						}

					}

				}

				$this->getProduct()->setData($fieldName, $valueToSet);
			}
		}

		/***************************
		 * Заключительная часть заплатки для локали
		 */

		catch (Exception $e) {
			$exception = $e;
		}


		if ($originalLocaleCode != df_mage()->core()->translate()->getLocale()) {

			df_mage()->core()->translate()
				->setLocale ($originalLocaleCode)
				->init('adminhtml', true)
			;
		}


		if (!is_null ($exception)) {
			throw $exception;
		}


		/***************************
		 * Конец заплатки для локали
		 */


		return $this;
	}




	/**
	 * @return Df_Dataflow_Model_Import_Product_Row
	 */
	private function assertRequiredAttributesExistence () {

		foreach (df_helper()->catalog()->product_dataflow()->getRequiredFields() as $requiredField) {

			/** @var string $requiredField */

			df_assert_string ($requiredField);


			/** @var Mage_Eav_Model_Entity_Attribute $attribute */
			$attribute = $this->getAttributeForField ($requiredField);

			if (!is_null ($attribute)) {

				df_assert ($attribute instanceof Mage_Eav_Model_Entity_Attribute);

				if ($attribute->getDataUsingMethod('is_required')) {

					/**
					 * Убеждаемся в присутствии значений
					 */
					$this->getRow ()->getFieldValue ($requiredField, true);
				}
			}
		}

		return $this;
	}









	/**
	 * Обратите внимание, что мы не кешируем свойства,
	 * потому что они могут изменяться динамически (например, при импорте из 1C)
	 *
	 * @param string $fieldName
	 * @return Mage_Eav_Model_Entity_Attribute|null
	 */
	private function getAttributeForField ($fieldName) {

		df_param_string ($fieldName, 0);

		/**
		 * Не используем df_helper()->catalog()->product()->getResource()->getAttribute ($fieldName),
		 * потому что там результат кэшируется, а нам не нужно кэшировать
		 * (свойства могут изменяться динамически)
		 *
		 * @var Mage_Catalog_Model_Resource_Eav_Attribute $result
		 */
		$result =
			df_model (
				'catalog/resource_eav_attribute'
			)
		;

		df_assert ($result instanceof Mage_Catalog_Model_Resource_Eav_Attribute);


		$result
			->loadByCode (
				df_helper()->eav()->getProductEntityTypeId()
				,
				$fieldName
			)
		;

		if (false === $result) {
			$result = null;
		}
		else {

			df_assert ($result instanceof Mage_Eav_Model_Entity_Attribute);


			if ($result instanceof Mage_Catalog_Model_Resource_Eav_Attribute) {

				/** @var Mage_Catalog_Model_Resource_Eav_Attribute $result  */

				/** @var array $applyTo  */
				$applyTo = $result->getApplyTo ();

				/**
				 * apply_to позволяет ограничить область применения свойства
				 */
				if (!is_null ($applyTo)) {

					df_assert_array ($applyTo);

					/**
					 * Mage_Catalog_Model_Resource_Eav_Attribute::getApplyTo() возващает пустой массив,
		 			 * если свойство применим ко всем типам товара.
					 */
					if (0 < count ($applyTo)) {

						if (!in_array($this->getProduct ()->getTypeId(), $applyTo)) {

							/**
							 * Скопировал данную логику из метода
							 * Mage_Catalog_Model_Convert_Adapter_Product::getAttribute()
							 */
							$result = null;
						}

					}

				}

			}
		}


		if (!is_null ($result)) {
			df_assert ($result instanceof Mage_Eav_Model_Entity_Attribute);
		}

		return $result;
	}





	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Dataflow_Model_Importer_Product';
	}





	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


