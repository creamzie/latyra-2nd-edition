<?php

class Df_Admin_Model_Form_Element extends Df_Core_Model_Abstract {


	/**
	 * @return string
	 */
	public function getCheckboxLabel () {
		return
				$this->getWrappedElement()->getData (
					self::FORM_ELEMENT__ATTRIBUTE_CAN_USE_WEBSITE_VALUE
				)
			?
				df_mage()->adminhtml()->__(self::T_USE_WEBSITE)
			:
				(
						$this->getWrappedElement()->getData (
							self::FORM_ELEMENT__ATTRIBUTE_CAN_USE_DEFAULT_VALUE
						)
					?
						df_mage()->adminhtml()->__(self::T_USE_DEFAULT)
					:
						Df_Core_Const::T_EMPTY
				)
		;
	}




	/**
	 * @return string
	 */
    public function getComment () {

        $result =
			df_convert_null_to_empty_string (
				$this->getWrappedElement()->getData ("comment")
			)
		;

		df_result_string ($result);

		return $result;
    }




	/**
	 * @return string
	 */
	public function getDefaultText () {
		if (!isset ($this->_defaultText)) {
            $this->_defaultText =
					!df_empty ($this->getOptions ())
				?
					$this->getWrappedElement()->getData (
						self::FORM_ELEMENT__ATTRIBUTE_DEFAULT_VALUE
					)
				:
					$this->getDefaultValueFromOptions ()
            ;
		}
		return $this->_defaultText;
	}



	/**
	 * @var string
	 */
	private $_defaultText;




	/**
	 * @return Df_Licensor_Model_Feature
	 */
	public function getFeature () {
		return df_feature ($this->getFeatureCode ());
	}




	/**
	 * @return string|null
	 */
	public function getFeatureCode () {
		if (!isset ($this->_featureCode)) {
			$this->_featureCode =
				df_a (
					$this->getFieldConfig ()->asArray ()
					,
					self::FIELD_ATTRIBUTE_FEATURE
				)
			;

			if (df_empty (df_trim ($this->_featureCode))) {
				$this->_featureCode = null;
			}


			if (!is_null ($this->_featureCode)) {
				df_result_string ($this->_featureCode);
			}
		}
		return $this->_featureCode;
	}


	/**
	 * @var string|null
	 */
	private $_featureCode;




	/**
	 * @return Df_Licensor_Model_Feature_Info
	 */
	public function getFeatureInfo () {
		return df_helper()->licensor()->getFeatureInfo($this->getFeature());
	}




	/**
	 * @return string
	 */
	public function getHint () {
		return $this->getWrappedElement()->getData (self::FORM_ELEMENT__ATTRIBUTE_HIHT);
	}




	/**
	 * @return bool
	 */
	public function getInherit () {
		$result =
			(
					1
				===
					intval (
						$this->getWrappedElement()
							->getDataUsingMethod (
								Df_Varien_Data_Form_Element_Abstract::PARAM__INHERIT
							)
					)
			)
		;

		df_result_boolean ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	public function getNamePrefix () {
		if (!isset ($this->_namePrefix)) {
			$this->_namePrefix =
				preg_replace(
					'#\[value\](\[\])?$#'
					,
					''
					,
					$this->getWrappedElement()->getName()
				)
			;

			df_result_string ($this->_namePrefix);
		}
		return $this->_namePrefix;
	}


	/**
	 * @var string
	 */
	private $_namePrefix;




	/**
	 * @return string
	 */
	public function getScopeLabel () {
		return
				$this->getWrappedElement()->getData (self::FORM_ELEMENT__ATTRIBUTE_SCOPE)
			?
				$this->getWrappedElement()->getData (self::FORM_ELEMENT__ATTRIBUTE_SCOPE_LABEL)
			:
				Df_Core_Const::T_EMPTY
		;
	}




	/**
	 * @return Mage_Core_Model_Config_Element
	 */
	protected function getFieldConfig () {
		return $this->getWrappedElement()->getData (self::FIELD_CONFIG);
	}




	/**
	 * @return string
	 */
	private function getDefaultValueFromOptions () {
		$defTextArr = array();
		foreach ($this->getOptions () as $k=>$v) {

			/** @var string $k */
			/** @var array $v */

			if ($this->isMultiple ()) {
				if (is_array($v['value']) && in_array($k, $v['value'])) {
					$defTextArr[] = $v['label'];
				}
			}
			elseif (
					$v['value']
				===
					$this->getWrappedElement()
						->getDataUsingMethod (
							Df_Varien_Data_Form_Element_Abstract::PARAM__DEFAULT_VALUE
						)
			) {
				$defTextArr[] = $v['label'];
				break;
			}
		}
		$result = implode (', ', $defTextArr);

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return array
	 */
	private function getOptions () {

		/** @var array $result  */
		$result = $this->getWrappedElement()
			->getDataUsingMethod (
				Df_Varien_Data_Form_Element_Abstract::PARAM__VALUES
			)
		;

		/**
		 * Varien_Data_Form_Element_Abstract::getValues () вполне может вернуть null
		 */
		if (is_null ($result)) {
			$result = array ();
		}

		df_result_array ($result);

		return $result;
	}




	/**
	 * @return bool
	 */
	private function isMultiple () {

		$result =
			(
					self::MULTIPLE
				===
					$this->getWrappedElement()
						->getDataUsingMethod (
							Df_Varien_Data_Form_Element_Abstract::PARAM__EXT_TYPE
						)
			)
		;

		df_result_boolean ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	public function getId () {
		/** @var string $result */
		$result = $this->getWrappedElement()->getHtmlId();
		df_result_string ($result);
		return $result;
	}





	/**
	 * @return string
	 */
	public function getLabel () {

		/** @var string $result */
		$result =
			df_convert_null_to_empty_string (
				$this->getWrappedElement()->getData ("label")
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	public function getHtml () {
		/** @var string $result */
		$result = $this->getWrappedElement()->getElementHtml();
		df_result_string ($result);
		return $result;
	}



	/**
	 * @return Varien_Data_Form_Element_Abstract
	 */
	public function getWrappedElement() {

		if (!isset ($this->_wrappedElement)) {
			$this->_wrappedElement = $this->cfg (self::PARAM_WRAPPED_ELEMENT);
			if (
					$this->_wrappedElement->getDataUsingMethod (
						Df_Varien_Data_Form_Element_Abstract::PARAM__CAN_USE_WEBSITE_VALUE
					)
				&&
					$this->_wrappedElement->getDataUsingMethod (
						Df_Varien_Data_Form_Element_Abstract::PARAM__CAN_USE_DEFAULT_VALUE
					)
				&&
					$this->_wrappedElement->getDataUsingMethod (
						Df_Varien_Data_Form_Element_Abstract::PARAM__INHERIT
					)
			) {
				$this->_wrappedElement
					->setDataUsingMethod (
						Df_Varien_Data_Form_Element_Abstract::PARAM__DISABLED, true
					)
				;
			}

			df_assert ($this->_wrappedElement instanceof Varien_Data_Form_Element_Abstract);
		}
		return $this->_wrappedElement;
	}


	/**
	 * @var Varien_Data_Form_Element_Abstract
	 */
	private $_wrappedElement;



	/**
	 * @return bool
	 */
	public function needToAddInheritBox () {
		$result =
				$this->getWrappedElement()
					->getDataUsingMethod (
						Df_Varien_Data_Form_Element_Abstract::PARAM__CAN_USE_WEBSITE_VALUE
					)
			&&
				$this->getWrappedElement()
					->getDataUsingMethod (
						Df_Varien_Data_Form_Element_Abstract::PARAM__CAN_USE_DEFAULT_VALUE
					)
		;

		df_result_boolean ($result);

		return $result;
	}




	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM_WRAPPED_ELEMENT, self::PARAM_WRAPPED_ELEMENT_TYPE
			)
		;
	}



	const FIELD_ATTRIBUTE_FEATURE = 'df_feature';
	const FIELD_CONFIG = 'field_config';
	const FORM_ELEMENT__ATTRIBUTE_CAN_USE_DEFAULT_VALUE = 'сan_use_default_value';
	const FORM_ELEMENT__ATTRIBUTE_CAN_USE_WEBSITE_VALUE = 'can_use_website_value';
	const FORM_ELEMENT__ATTRIBUTE_DEFAULT_VALUE = 'default_value';
	const FORM_ELEMENT__ATTRIBUTE_HIHT = 'hint';
	const FORM_ELEMENT__ATTRIBUTE_SCOPE = 'scope';
	const FORM_ELEMENT__ATTRIBUTE_SCOPE_LABEL = 'scope_label';

	const MULTIPLE = 'multiple';

	const PARAM_WRAPPED_ELEMENT = 'wrappedElement';
	const PARAM_WRAPPED_ELEMENT_TYPE = 'Varien_Data_Form_Element_Abstract';

	const T_USE_DEFAULT= 'Use Default';
	const T_USE_WEBSITE = 'Use Website';


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Model_Form_Element';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}