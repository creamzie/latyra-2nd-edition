<?php

class Df_Admin_Model_Config_Extractor_Font extends Df_Admin_Model_Config_Extractor {



	/**
	 * @param string $text
	 * @param string|null $domElementId [optional]
	 * @return string
	 */
	public function format ($text, $domElementId = null) {

		df_param_string ($text, 0);

		$domElementId = df_convert_null_to_empty_string ($domElementId);
		df_param_string ($domElementId, 1);

		/** @var string $result  */
		$result =
			df_trim (
				df_block (
					Df_Core_Block_FormattedText::getNameInMagentoFormat()
					,
					null
					,
					array (
						Df_Core_Block_FormattedText::PARAM__FONT_CONFIG => $this
						,
						Df_Core_Block_FormattedText::PARAM__RAW_TEXT => $text
						,
						Df_Core_Block_FormattedText::PARAM__DOM_ELEMENT_ID => $domElementId
						,
						Df_Core_Block_FormattedText::PARAM__USE_INLINE_CSS_RULES => true
					)
				)
					->toHtml()
			)
		;


		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	public function getLetterCase () {

		/** @var string $result  */
		$result = $this->getValue (self::CONFIG_KEY__LETTER_CASE);

		if (is_null ($result)) {
			$result =
				Df_Admin_Model_Config_Source_Format_Text_LetterCase::_DEFAULT
			;
		}

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return Df_Core_Model_Output_Css_Rule
	 */
	public function getLetterCaseAsCssRule () {

		if (!isset ($this->_letterCaseAsCssRule)) {

			/** @var Df_Core_Model_Output_Css_Rule $result  */
			$result =
				df_model (
					Df_Core_Model_Output_Css_Rule::getNameInMagentoFormat()
					,
					array (
						Df_Core_Model_Output_Css_Rule::PARAM__NAME => 'text-transform'
						,
						Df_Core_Model_Output_Css_Rule::PARAM__VALUE =>
							Df_Admin_Model_Config_Source_Format_Text_LetterCase::convertToCss (
								$this->getLetterCase()
							)
					)
				)
			;


			df_assert ($result instanceof Df_Core_Model_Output_Css_Rule);

			$this->_letterCaseAsCssRule = $result;
		}


		df_assert ($this->_letterCaseAsCssRule instanceof Df_Core_Model_Output_Css_Rule);

		return $this->_letterCaseAsCssRule;

	}


	/**
	* @var Df_Core_Model_Output_Css_Rule
	*/
	private $_letterCaseAsCssRule;





	/**
	 * @return boolean
	 */
	public function needSetup () {

		/** @var bool $result  */
		$result = $this->getYesNo (self::CONFIG_KEY__SETUP);

		df_result_boolean ($result);

		return $result;
	}



	/**
	 * @return boolean
	 */
	public function useBold () {

		/** @var bool $result  */
		$result = $this->getYesNo (self::CONFIG_KEY__BOLD);

		df_result_boolean ($result);

		return $result;
	}



	/**
	 * @return boolean
	 */
	public function useItalic () {

		/** @var bool $result  */
		$result = $this->getYesNo (self::CONFIG_KEY__ITALIC);

		df_result_boolean ($result);

		return $result;
	}



	/**
	 * @return boolean
	 */
	public function useUnderline () {

		/** @var bool $result  */
		$result = $this->getYesNo (self::CONFIG_KEY__UNDERLINE);

		df_result_boolean ($result);

		return $result;
	}





	/**
	 * @override
	 * @return string
	 */
	protected function getEntityName () {

		/** @var string $result  */
		$result = self::CONFIG_ENTITY_NAME;

		df_result_string ($result);

		return $result;
	}



	const CONFIG_ENTITY_NAME = 'font';

	const CONFIG_KEY__SETUP = 'setup';
	const CONFIG_KEY__LETTER_CASE = 'letter_case';
	const CONFIG_KEY__BOLD = 'emphase__bold';
	const CONFIG_KEY__ITALIC = 'emphase__italic';
	const CONFIG_KEY__UNDERLINE = 'emphase__underline';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Model_Config_Extractor_Font';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


