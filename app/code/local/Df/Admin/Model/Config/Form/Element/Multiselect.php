<?php

class Df_Admin_Model_Config_Form_Element_Multiselect extends Varien_Data_Form_Element_Multiselect {


	/**
	 * @override
	 * @param array $option
	 * @param array $selected
	 * @return string
	 */
    protected function _optionToHtml($option, $selected)
    {
        $html = '<option value="'.$this->_escape($option['value']).'"';
        $html.= isset($option['title']) ? 'title="'.$this->_escape($option['title']).'"' : '';
        $html.= isset($option['style']) ? 'style="'.$option['style'].'"' : '';

		if (
				in_array((string)$option['value'], $selected)
			||
				/******************************
				 * Начало заплатки
				 */
				in_array (self::RM__ALL, $selected)
				/******************************
				 * Конец заплатки
				 */
		) {
            $html.= ' selected="selected"';
        }

        $html.= '>'.$this->_escape($option['label']). '</option>'."\n";
        return $html;
    }


	const RM__ALL = 'df-all';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Model_Config_Form_Element_Multiselect';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


