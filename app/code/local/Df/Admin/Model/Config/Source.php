<?php

/**
 * @singleton
 * Система создаёт объект-одиночку для потомков этого класса.
 * Не забывайте об этом при реализации кеширования результатов вычислений внутри этого класса!
 */
abstract class Df_Admin_Model_Config_Source extends Df_Core_Model_Abstract {


	/**
	 * @abstract
	 * @param bool $isMultiSelect [optional]
	 * @return array
	 */
	abstract protected function toOptionArrayInternal ($isMultiSelect = false);
	
	
	
	/**
	 * @return Df_Admin_Model_Config_Source
	 */
	protected function reset () {

		unset ($this->_fieldCode);
		unset ($this->_fieldConfigNode);
		unset ($this->_fieldConfigNodeAsCanonicalArray);

		unset ($this->_groupCode);
		unset ($this->_groupConfigNode);	
		
		unset ($this->_pathExploded);
		
		unset ($this->_sectionCode);
		unset ($this->_sectionConfigNode);		

		return $this;
	}
	
	
	
	/**
	 * @param bool $isMultiSelect [optional]
	 * @return array
	 */
	public function toOptionArray ($isMultiSelect = false) {
		
		$this->reset ();
	
		/** @var array $result  */
		$result = $this->toOptionArrayInternal ($isMultiSelect);	
	
		df_result_array ($result);
	
		return $result;
	}




	/**
	 * @return array
	 */
	public function toOptionArrayAssoc () {

		/** @var array $result  */
		$result = array ();

		foreach ($this->toOptionArray() as $option) {
			/** @var array $option */

			$result [df_a ($option, self::OPTION_KEY__VALUE)] =
				df_a ($option, self::OPTION_KEY__LABEL)
			;
		}



		df_result_array ($result);

		return $result;
	}





	/**
	 * @param string $paramName
	 * @param mixed $defaultValue
	 * @return string|mixed
	 */
	protected function getFieldParam ($paramName, $defaultValue = Df_Core_Const::T_EMPTY) {

		/** @var string|mixed $result  */
		$result =
			df_a (
				$this->getFieldConfigNodeAsCanonicalArray()
				,
				$paramName
				,
				$defaultValue
			)
		;

		if (df_empty ($result)) {
			$result = Df_Core_Const::T_EMPTY;
		}

		if ($defaultValue !== $result) {
			df_result_string ($result);
		}

		return $result;
	}
	



	/**
	 * @return string
	 */
	protected function getPath () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM__PATH);

		df_result_string ($result);

		return $result;
	}



	
	/**
	 * @return array
	 */
	protected function getPathExploded () {
	
		if (!isset ($this->_pathExploded)) {
	
			/** @var array $result  */
			$result = 
				df()->config()->explodeKey (
					$this->getPath()
				)			
			;
	
	
			df_assert_array ($result);
	
			$this->_pathExploded = $result;
		}
	
	
		df_result_array ($this->_pathExploded);
	
		return $this->_pathExploded;
	}
	
	
	/**
	* @var array
	*/
	private $_pathExploded;	
	
	
	



	/**
	 * @return Mage_Adminhtml_Model_Config
	 */
	private function getAdminhtmlConfig () {

		/** @var Mage_Adminhtml_Model_Config $result  */
		$result = Mage::getSingleton('adminhtml/config');

		df_assert ($result instanceof Mage_Adminhtml_Model_Config);

		return $result;
	}
	
	
	
	
	/**
	 * @return string
	 */
	private function getFieldCode () {

		if (!isset ($this->_fieldCode)) {

			/** @var string $result  */
			$result =
				df_a (
					$this->getPathExploded(), 2
				)
			;

			df_assert_string ($result);

			$this->_fieldCode = $result;
		}


		df_result_string ($this->_fieldCode);

		return $this->_fieldCode;

	}


	/**
	* @var string
	*/
	private $_fieldCode;


	


	/**
	 * @return Varien_Simplexml_Element
	 */
	private function getFieldConfigNode () {

		if (!isset ($this->_fieldConfigNode)) {

			/** @var Varien_Simplexml_Element $result  */
			$result =
				df_a (
					$this->getGroupConfigNode()->xpath (
						implode (
							Df_Core_Const::T_XPATH_SEPARATOR
							,
							array (
								'fields'
								,
								$this->getFieldCode()
							)
						)
					)
					,
					0
				)
			;


			df_assert ($result instanceof Varien_Simplexml_Element);

			$this->_fieldConfigNode = $result;
		}


		df_assert ($this->_fieldConfigNode instanceof Varien_Simplexml_Element);

		return $this->_fieldConfigNode;

	}


	/**
	* @var Varien_Simplexml_Element
	*/
	private $_fieldConfigNode;
	
	
	
	
	
	/**
	 * @return array
	 */
	private function getFieldConfigNodeAsCanonicalArray () {
	
		if (!isset ($this->_fieldConfigNodeAsCanonicalArray)) {
	
			/** @var array $result  */
			$result =
					$this->isCreatedStandalone ()
				?
					$this->getData ()
				:
					$this->getFieldConfigNode()->asCanonicalArray()
			;
	
	
			df_assert_array ($result);
	
			$this->_fieldConfigNodeAsCanonicalArray = $result;
		}
	
	
		df_result_array ($this->_fieldConfigNodeAsCanonicalArray);
	
		return $this->_fieldConfigNodeAsCanonicalArray;
	}
	
	
	/**
	* @var array
	*/
	private $_fieldConfigNodeAsCanonicalArray;	
	
	




	/**
	 * @return string
	 */
	private function getGroupCode () {

		if (!isset ($this->_groupCode)) {

			/** @var string $result  */
			$result =
				df_a (
					$this->getPathExploded(), 1
				)
			;

			df_assert_string ($result);

			$this->_groupCode = $result;
		}


		df_result_string ($this->_groupCode);

		return $this->_groupCode;

	}


	/**
	* @var string
	*/
	private $_groupCode;




	/**
	 * @return Varien_Simplexml_Element
	 */
	private function getGroupConfigNode () {

		if (!isset ($this->_groupConfigNode)) {


			/** @var Varien_Simplexml_Element $result  */
			$result =
				df_a (
					$this->getSectionConfigNode()->xpath (
						implode (
							Df_Core_Const::T_XPATH_SEPARATOR
							,
							array (
								'groups'
								,
								$this->getGroupCode()
							)
						)
					)
					,
					0
				)
			;


			df_assert ($result instanceof Varien_Simplexml_Element);

			$this->_groupConfigNode = $result;
		}


		df_assert ($this->_groupConfigNode instanceof Varien_Simplexml_Element);

		return $this->_groupConfigNode;

	}


	/**
	* @var Varien_Simplexml_Element
	*/
	private $_groupConfigNode;

	
	

	
	
	/**
	 * @return string
	 */
	private function getSectionCode () {
	
		if (!isset ($this->_sectionCode)) {
	
			/** @var string $result  */
			$result = 
				df_a (
					$this->getPathExploded(), 0
				)
			;	
	
			df_assert_string ($result);
	
			$this->_sectionCode = $result;
		}
	
	
		df_result_string ($this->_sectionCode);
	
		return $this->_sectionCode;
	}
	
	
	/**
	* @var string
	*/
	private $_sectionCode;	
	
	
	
	
	/**
	 * @return Varien_Simplexml_Element
	 */
	private function getSectionConfigNode () {
	
		if (!isset ($this->_sectionConfigNode)) {
	
			/** @var Varien_Simplexml_Element $result  */
			$result = 
				$this->getAdminhtmlConfig()->getSection (
					$this->getSectionCode()
				)
			;
	
	
			df_assert ($result instanceof Varien_Simplexml_Element);
	
			$this->_sectionConfigNode = $result;
		}
	
		df_assert ($this->_sectionConfigNode instanceof Varien_Simplexml_Element);
	
		return $this->_sectionConfigNode;
	}
	
	
	/**
	* @var Varien_Simplexml_Element
	*/
	private $_sectionConfigNode;	
	
	
	
	
	
	/**
	 * true, если объект создан не системой, а вручную.
	 * В таком случае у объекта отсутствует информация о своём пути в настройках и т.п.
	 *
	 * @return bool
	 */
	private function isCreatedStandalone () {
	
		if (!isset ($this->_createdStandalone)) {
	
			/** @var bool $result  */
			$result = 
				is_null (
					$this->cfg (self::PARAM__PATH)
				)
			;
	
	
			df_assert_boolean ($result);
	
			$this->_createdStandalone = $result;
		}
	
	
		df_result_boolean ($this->_createdStandalone);
	
		return $this->_createdStandalone;
	}
	
	
	/**
	* @var bool
	*/
	private $_createdStandalone;		
	
	
	



	const PARAM__PATH = 'path';

	const OPTION_KEY__LABEL = 'label';
	const OPTION_KEY__VALUE = 'value';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Model_Config_Source';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


