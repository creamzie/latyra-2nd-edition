<?php

abstract class Df_Admin_Model_Config_Backend_Cron extends Mage_Core_Model_Config_Data {

	
	
	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getCronJobName ();	
	
	

	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getTimeConfigFieldName ();



	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getTimeConfigGroupName ();



	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getFrequencyConfigFieldName ();



	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getFrequencyConfigGroupName ();




	/**
	 * @override
	 * @throws Exception
	 * @return void
	 */
    protected function _afterSave() {

		try {
			$this
				->saveConfigKey (
					$this->getCronSchedulePath(), $this->getCronExpression()
				)
			;


			$this
				->saveConfigKey (
					$this->getCronModelPath()
					,
					(string) Mage::getConfig()->getNode($this->getCronModelPath())
				)
			;
		}

		catch (Exception $e) {
			df_handle_entry_point_exception ($e);
			//throw new Exception(Mage::helper('cron')->__('Unable to save the cron expression.'));
		}
    }




	/**
	 * @param string $groupName
	 * @param string $fieldName
	 * @return mixed
	 */
	private function getConfigFieldValue ($groupName, $fieldName) {

		df_param_string ($groupName, 0);
		df_param_string ($fieldName, 1);


		/** @var mixed $result  */
		$result =
			$this->getData (
				df()->config()->implodeKey (
					array (
						'groups', $groupName, 'fields', $fieldName, 'value'
					)
				)
			)
		;

		return $result;
	}




	/**
	 * @return string
	 */
	private function getCronExpression () {

		if (!isset ($this->_cronExpression)) {

			/** @var string $result  */
			$result =
				implode (
					Df_Core_Const::T_SPACE
					,
					array (
						// минута
						$this->getMinute()

						,
						// час
						$this->getHour()

						,
						// день месяца
								(
										$this->getFrequency()
									===
										Mage_Adminhtml_Model_System_Config_Source_Cron_Frequency::CRON_MONTHLY
								)
							?
								'1'
							:
								'*'

						,
						// месяц года
						'*'

						,
						// день недели
							(
									$this->getFrequency()
								===
									Mage_Adminhtml_Model_System_Config_Source_Cron_Frequency::CRON_WEEKLY
							)
						?
							'1'
						:
							'*'
					)
				)
			;


			df_assert_string ($result);

			$this->_cronExpression = $result;
		}


		df_result_string ($this->_cronExpression);

		return $this->_cronExpression;

	}


	/**
	* @var string
	*/
	private $_cronExpression;





	/**
	 * @return string
	 */
	private function getCronModelPath () {

		if (!isset ($this->_cronModelPath)) {

			/** @var string $result  */
			$result =
				df()->config()->implodeKey (
					array (
						'crontab', 'jobs', $this->getCronJobName (), 'run', 'model'
					)
				)
			;


			df_assert_string ($result);

			$this->_cronModelPath = $result;
		}


		df_result_string ($this->_cronModelPath);

		return $this->_cronModelPath;

	}


	/**
	* @var string
	*/
	private $_cronModelPath;




	/**
	 * @return string
	 */
	private function getCronSchedulePath () {

		if (!isset ($this->_cronSchedulePath)) {

			/** @var string $result  */
			$result =
				df()->config()->implodeKey (
					array (
						'crontab', 'jobs', $this->getCronJobName (), 'schedule', 'cron_expr'
					)
				)
			;


			df_assert_string ($result);

			$this->_cronSchedulePath = $result;
		}


		df_result_string ($this->_cronSchedulePath);

		return $this->_cronSchedulePath;

	}


	/**
	* @var string
	*/
	private $_cronSchedulePath;



	/**
	 * @return string
	 */
	private function getFrequency () {

		if (!isset ($this->_frequency)) {

			/** @var string $result  */
			$result =
				$this->getConfigFieldValue (
					$this->getFrequencyConfigGroupName()
					,
					$this->getFrequencyConfigFieldName()
				)
			;

			df_assert_string ($result);

			$this->_frequency = $result;
		}


		df_result_string ($this->_frequency);

		return $this->_frequency;

	}


	/**
	* @var string
	*/
	private $_frequency;




	/**
	 * @return int
	 */
	private function getHour () {

		/** @var int $result  */
		$result =
			intval (
				df_a ($this->getTime(), 0)
			)
		;

		df_result_integer ($result);

		return $result;
	}



	/**
	 * @return int
	 */
	private function getMinute () {

		/** @var int $result  */
		$result =
			intval (
				df_a ($this->getTime(), 1)
			)
		;

		df_result_integer ($result);

		return $result;
	}




	/**
	 * @return array
	 */
	private function getTime () {

		if (!isset ($this->_time)) {

			/** @var array $result  */
			$result =
				$this->getConfigFieldValue (
					$this->getTimeConfigGroupName()
					,
					$this->getTimeConfigFieldName()
				)
			;


			df_assert_array ($result);

			$this->_time = $result;
		}


		df_result_array ($this->_time);

		return $this->_time;

	}


	/**
	* @var array
	*/
	private $_time;





	/**
	 * @param string $path
	 * @param string $value
	 * @return Df_Admin_Model_Config_Backend_Cron
	 */
	private function saveConfigKey ($path, $value) {

		df_param_string ($path, 0);
		df_param_string ($value, 1);

		/** @var Mage_Core_Model_Config_Data $configData  */
		$configData = Mage::getModel('core/config_data');
		$configData->load($path, 'path');
		$configData->setValue($value);
		$configData->setPath($path);
		$configData->save();

		return $this;
	}



}


