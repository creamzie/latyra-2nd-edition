<?php

class Df_Admin_Model_Config_Backend_Validator extends Df_Admin_Model_Config_Backend {


	/**
	 * Метод публичен, потому что его использует применяемая валидатором стратегия
	 * (стратегия является отдельным от валидатора объектом,
	 * что позволяет подставлять в валидатор разные стратегии)
	 *
	 * @return Mage_Core_Model_Message_Collection
	 */
	public function getMessages () {
		if (!isset ($this->_messages)) {
			$this->_messages = new Mage_Core_Model_Message_Collection();
		}
		return $this->_messages;
	}


	/**
	* @var Mage_Core_Model_Message_Collection
	*/
	private $_messages;




	/**
	 * @overide
	 * @return Df_Admin_Model_Config_Backend_Validator
	 */
	protected function _beforeSave () {

		try {
			/**
			 * Проводим валидацию только в том случае, если административная опция включена
			 */
			if (true === df_parse_boolean($this->getValue())) {
				$this->validate ();
			}
		}
		catch (Exception $e) {
			/**
			 * Сюда мы попадаем не тогда, когда валидация не пройдена,
			 * а когда программный код валидатора дал сбой.
			 */
			df_log_exception ($e);

			$this->getMessages()
				->addMessage (
					new Mage_Core_Model_Message_Error (
						$e->getMessage()
					)
				)
			;
		}

		/**
		 * Показываем администратору все сообщения,
		 * которые валидатор счёл нужным предоставить.
		 */
		foreach ($this->getMessages()->getItems() as $message) {
			/** @var Mage_Core_Model_Message_Abstract $message */
			df_assert ($message instanceof Mage_Core_Model_Message_Abstract);

			rm_session()->addMessage ($message);
		}

		parent::_beforeSave();

		return $this;
	}




	/**
	 * @return string
	 */
	private function getStrategyClassMf () {
		/** @var string $result  */
		$result = $this->getFieldConfigParam ('df_backend_validator_strategy', $mustBeNonEmpty = true);
		df_result_string ($result);
		return $result;
	}




	/**
	 * @return Df_Admin_Model_Config_Backend_Validator
	 */
	private function validate () {

		/** @var bool $isValid */
		$isValid = true;

		foreach ($this->getStores() as $store) {
			/** @var Mage_Core_Model_Store $store */
			$isValid =
					$isValid
				&&
					$this->validateForStore ($store)
			;
		}
		
		/**
		 * Если валидация не пройдена, то отключаем опцию
		 */
		$this
			->setValue (
					$isValid
				&&
					df_parse_boolean (
						$this->getValue()
					)
			)
		;

		return $this;
	}




	/**
	 * @param Mage_Core_Model_Store $store
	 * @return bool
	 */
	private function validateForStore (Mage_Core_Model_Store $store) {

		/** @var Df_Admin_Model_Config_Backend_Validator_Strategy $strategy */
		$strategy =
			df_model (
				$this->getStrategyClassMf()
				,
				array (
					Df_Admin_Model_Config_Backend_Validator_Strategy::PARAM__BACKEND => $this
					,
					Df_Admin_Model_Config_Backend_Validator_Strategy::PARAM__STORE => $store
				)
			)
		;

		df_assert ($strategy instanceof Df_Admin_Model_Config_Backend_Validator_Strategy);

		/** @var bool $result */
		$result = $strategy->validate();

		df_result_boolean ($result);

		return $result;
	}





	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Model_Config_Backend';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


