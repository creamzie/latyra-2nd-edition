<?php

class Df_Admin_Model_Config_Source_NoYes {

	/**
	 * @return array
	 */
	public function toOptionArray() {
		return array(
			array('value' => 0, 'label'=>Mage::helper('adminhtml')->__('Yes')),
			array('value' => 1, 'label'=>Mage::helper('adminhtml')->__('No'))
		);
	}


	/**
	 * @return array
	 */
	public function toArray() {
		return array(
			1 => Mage::helper('adminhtml')->__('No'),
			0 => Mage::helper('adminhtml')->__('Yes')
		);
	}

}
