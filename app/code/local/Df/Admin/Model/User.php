<?php

class Df_Admin_Model_User extends Mage_Admin_Model_User {


	const PARAM__EMAIL = 'email';
	const PARAM__FIRSTNAME = 'firstname';
	const PARAM__IS_ACTIVE = 'is_active';
	const PARAM__LASTNAME = 'lastname';
	const PARAM__NEW_PASSWORD = 'new_password';
	const PARAM__PASSWORD_CONFIRMATION = 'password_confirmation';
	const PARAM__ROLE_ID = 'role_id';
	const PARAM__USERNAME = 'username';


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Model_User';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


