<?php

class Df_Admin_Helper_Settings_Admin extends Df_Core_Helper_Settings {



	/**
	 * @return Df_AccessControl_Helper_Settings
	 */
	public function access_control () {

		/** @var Df_AccessControl_Helper_Settings $result  */
		$result =
			Mage::helper (
				Df_AccessControl_Helper_Settings::getNameInMagentoFormat()
			)
		;

		df_assert ($result instanceof Df_AccessControl_Helper_Settings);

		return $result;
	}




	/**
	 * @return Df_Admin_Helper_Settings_Admin_Catalog
	 */
	public function catalog () {

		/** @var Df_Admin_Helper_Settings_Admin_Catalog $result  */
		$result = Mage::helper (Df_Admin_Helper_Settings_Admin_Catalog::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Admin_Helper_Settings_Admin_Catalog);

		return $result;
	}
	
	
	
	
	
	/**
	 * @return Df_Admin_Helper_Settings_Admin_Interface
	 */
	public function _interface () {

		/** @var Df_Admin_Helper_Settings_Admin_Interface $result  */
		$result = Mage::helper (Df_Admin_Helper_Settings_Admin_Interface::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Admin_Helper_Settings_Admin_Interface);

		return $result;
	}




	/**
	 * @return Df_Logging_Helper_Settings
	 */
	public function logging () {

		/** @var Df_Logging_Helper_Settings $result  */
		static $result;

		if (!isset ($result)) {
			$result =
				Mage::helper (
					Df_Logging_Helper_Settings::getNameInMagentoFormat()
				)
			;

			df_assert ($result instanceof Df_Logging_Helper_Settings);
		}

		return $result;
	}





	/**
	 * @return Df_Admin_Helper_Settings_Admin_Optimization
	 */
	public function optimization () {

		/** @var Df_Admin_Helper_Settings_Admin_Optimization $result  */
		$result = Mage::helper (Df_Admin_Helper_Settings_Admin_Optimization::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Admin_Helper_Settings_Admin_Optimization);

		return $result;
	}
	
	
	
	
	
	/**
	 * @return Df_Admin_Helper_Settings_Admin_Jquery
	 */
	public function jquery () {

		/** @var Df_Admin_Helper_Settings_Admin_Jquery $result  */
		static $result;

		if (!isset ($result)) {
			$result =
				Mage::helper (
					Df_Admin_Helper_Settings_Admin_Jquery::getNameInMagentoFormat()
				)
			;

			df_assert ($result instanceof Df_Admin_Helper_Settings_Admin_Jquery);
		}

		return $result;
	}
	
	
	



	/**
	 * @return Df_Admin_Helper_Settings_Admin_Promotions
	 */
	public function promotions () {

		/** @var Df_Admin_Helper_Settings_Admin_Promotions $result  */
		$result = Mage::helper (Df_Admin_Helper_Settings_Admin_Promotions::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Admin_Helper_Settings_Admin_Promotions);

		return $result;
	}



	/**
	 * @return Df_Admin_Helper_Settings_Admin_Sales
	 */
	public function sales () {

		/** @var Df_Admin_Helper_Settings_Admin_Sales $result  */
		$result = Mage::helper (Df_Admin_Helper_Settings_Admin_Sales::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Admin_Helper_Settings_Admin_Sales);

		return $result;
	}



	/**
	 * @return Df_Admin_Helper_Settings_Admin_System
	 */
	public function system () {

		/** @var Df_Admin_Helper_Settings_Admin_System $result  */
		$result = Mage::helper (Df_Admin_Helper_Settings_Admin_System::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Admin_Helper_Settings_Admin_System);

		return $result;
	}




	/**
	 * @return Df_Admin_Helper_Settings_Admin_Wysiwyg
	 */
	public function wysiwyg () {

		/** @var Df_Admin_Helper_Settings_Admin_Wysiwyg $result  */
		static $result;

		if (!isset ($result)) {
			$result =
				Mage::helper (
					Df_Admin_Helper_Settings_Admin_Wysiwyg::getNameInMagentoFormat()
				)
			;

			df_assert ($result instanceof Df_Admin_Helper_Settings_Admin_Wysiwyg);
		}

		return $result;
	}





	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Helper_Settings_Admin';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}