<?php

class Df_Admin_Helper_Settings_Admin_Sales extends Df_Core_Helper_Settings {

	/**
	 * @return Df_Admin_Helper_Settings_Admin_Sales_Customers
	 */
	public function customers () {

		/** @var Df_Admin_Helper_Settings_Admin_Sales_Customers $result  */
		$result = Mage::helper (Df_Admin_Helper_Settings_Admin_Sales_Customers::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Admin_Helper_Settings_Admin_Sales_Customers);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Helper_Settings_Admin_Sales';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}