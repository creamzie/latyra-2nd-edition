<?php

class Df_Seo_Model_Template_Property_Product_Default
	extends Df_Seo_Model_Template_Property_Product {

	/**
	 * @return string
	 */
	public function getValue () {
		return
				!df_helper()->catalog()->product()->getResource()->getAttribute (
					$this->getName ()
				)
			?
				null
			:
				(
						df_empty (
							$this->getAttributeText ()
						)
					?
						$this->getProduct ()->getData ($this->getName ())
					:
						$this->getAttributeText ()
				)
		;
	}


	/**
	 * @return string
	 */
	private function getAttributeText () {
		if (!isset ($this->_attributeText)) {
			$this->_attributeText =
				$this->getProduct ()->getAttributeText(
					$this->getName ()
				)
			;
		}
		return $this->_attributeText;
	}


	/**
	 * @var string
	 */
	private $_attributeText;



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Seo_Model_Template_Property_Product_Default';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}