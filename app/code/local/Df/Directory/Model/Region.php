<?php

class Df_Directory_Model_Region extends Mage_Directory_Model_Region {


	const PARAM__COUNTRY_ID = 'country_id';
	const PARAM__CODE = 'code';
	const PARAM__DEFAULT_NAME = 'default_name';
	const PARAM__DF_CAPITAL = 'df_capital';
	const PARAM__DF_TYPE = 'df_type';
	const PARAM__NAME = 'name';
	const PARAM__ORIGINAL_NAME = 'original_name';
	const PARAM__REGION_ID = 'region_id';


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Directory_Model_Region';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


