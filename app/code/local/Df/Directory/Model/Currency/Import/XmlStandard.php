<?php

abstract class Df_Directory_Model_Currency_Import_XmlStandard
	extends Df_Directory_Model_Currency_Import {

	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getBaseCurrencyCode();


	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getTagName_CurrencyCode();


	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getTagName_CurrencyItem();

	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getTagName_Denominator();

	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getTagName_Rate();


	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getUrl();


	/**
	 * @override
	 *
	 * @param string $currencyFrom
	 * @param string $currencyTo
	 * @return float
	 */
	protected function convertInternal ($currencyFrom, $currencyTo) {

		/** @var float $rateFrom */
		$rateFrom = $this->getRate ($currencyFrom);

		/** @var float $rateTo */
		$rateTo = $this->getRate ($currencyTo);

		/** @var float $result */
		$result = $rateFrom / $rateTo;

		df_result_float ($result);
		df_assert ($result > 0.0);

		return $result;
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getName() {
		return 'Банк России';
	}


	/**
	 * @param array $currencyData
	 * @return float
	 */
	private function calculateRate (array $currencyData) {

		/** @var int $rateDenominator */
		$rateDenominator =
			df_a (
				$currencyData
				,
				$this->getTagName_Denominator()
			)
		;
		df_assert_integer ($rateDenominator);
		$rateDenominator = intval ($rateDenominator);
		df_assert ($rateDenominator > 0);


		/** @var string $valueRawAsText */
		$valueRawAsText =
			df_a (
				$currencyData
				,
				$this->getTagName_Rate()
			)
		;
		df_assert_string ($valueRawAsText);


		/** @var float $rateRaw */
		$rateRaw =
			str_replace (
				',', '.', $valueRawAsText
			)
		;
		$rateRaw = floatval ($rateRaw);
		df_assert ($rateRaw > 0.0);


		/** @var float $rate */
		$result =
			$rateRaw / $rateDenominator
		;
		df_result_float ($result);
		df_assert ($result > 0.0);

		return $result;
	}


	/**
	 * @return array
	 */
	private function getMapFromCurrencyCodeToRate () {
		if (!isset ($this->_mapFromCurrencyCodeToRate)) {
	
			/** @var array $result  */
			$result = array ();

			/** @var Varien_Simplexml_Element $currenciesAsSimpleXml */
			$currenciesAsSimpleXml =
				$this->getSimpleXml()->descend (
					$this->getTagName_CurrencyItem()
				)
			;
			df_assert ($currenciesAsSimpleXml instanceof Varien_Simplexml_Element);

			foreach ($currenciesAsSimpleXml as $currencyAsSimpleXml) {
				/** @var Varien_Simplexml_Element $currencyAsSimpleXml */
				df_assert ($currencyAsSimpleXml instanceof Varien_Simplexml_Element);

				/**
					<Valute ID="R01720">
						<NumCode>980</NumCode>
						<CharCode>UAH</CharCode>
						<Nominal>10</Nominal>
						<Name>Украинских гривен</Name>
						<Value>37,1672</Value>
					</Valute>

				<item>
					<date>2013-02-11</date>
					<code>643</code>
					<char3>RUB</char3>
					<size>10</size>
					<name>російських рублів</name>
					<rate>2.6504</rate>
					<change>-0.0095</change>
				</item>
				 *
				 */

				/** @var array $currencyAsArray */
				$currencyAsArray = $currencyAsSimpleXml->asArray();
				df_assert_array ($currencyAsArray);

				/** @var string $currencyCode */
				$currencyCode =
					df_a (
						$currencyAsArray
						,
						$this->getTagName_CurrencyCode()
					)
				;
				df_assert_string ($currencyCode);

				$result [$currencyCode] =
					$this->calculateRate (
						$currencyAsArray
					)
				;
			}
	
			df_result_array ($result);
			$this->_mapFromCurrencyCodeToRate = $result;
		}
		return $this->_mapFromCurrencyCodeToRate;
	}
	/** @var array */
	private $_mapFromCurrencyCodeToRate;


	/**
	 * @param string $currencyCode
	 * @return float
	 */
	private function getRate ($currencyCode) {

		/** @var float $result */
		$result =
				($this->getBaseCurrencyCode() === $currencyCode)
			?
				1.0
			:
				df_a (
					$this->getMapFromCurrencyCodeToRate ()
					,
					$currencyCode
				)
		;

		if (is_null ($result)) {
			$this->throwNoRate ($this->getBaseCurrencyCode(), $currencyCode);
		}

		df_result_float ($result);
		df_assert ($result > 0.0);
		return $result;
	}

	
	/**
	 * @return Varien_Simplexml_Element
	 */
	private function getSimpleXml () {
		if (!isset ($this->_simpleXml)) {
			try {
				/** @var string $xml */
				$xml =
					file_get_contents (
						$this->getUrl()
					)
				;
				df_assert_string ($xml);

				/** @var Varien_Simplexml_Element $result */
				$result =
					simplexml_load_string (
						$xml
						,
						'Varien_Simplexml_Element'
					)
				;
				df_assert ($result instanceof Varien_Simplexml_Element);
				$this->_simpleXml = $result;
			}
			catch (Exception $e) {
				$this
					->throwServiceFailure (
						$this->getUrl()
					)
				;
			}
		}
		return $this->_simpleXml;
	}
	/** @var Varien_Simplexml_Element */
	private $_simpleXml;	


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Directory_Model_Currency_Import_XmlStandard';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


