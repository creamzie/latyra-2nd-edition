<?php

class Df_Wishlist_Block_Customer_Sidebar extends Mage_Wishlist_Block_Customer_Sidebar {


	/**
	 * @override
	 * @return string
	 */
	public function __ () {
		/** @var array $args  */
		$args = func_get_args();

		/** @var string $result  */
		$result = df_helper()->localization()->translation()->translateByParent ($args, $this);

		return $result;
	}


	/**
	 * @override
	 * @return array
	 */
	public function getCacheKeyInfo() {

		/** @var array $result  */
		$result = parent::getCacheKeyInfo();

		if (
				df_module_enabled (Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->wishlistCustomerSidebar()
		) {
			$result =
				array_merge (
					$result
					,
					array (
						get_class ($this)
					)
					,
					$this->getProductIds()
				)
			;
		}

		df_result_array ($result);

		return $result;
	}



	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		if (
				df_module_enabled (Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->wishlistCustomerSidebar()
		) {
			$this->setData ('cache_lifetime', Df_Core_Block_Template::CACHE_LIFETIME_STANDARD);
		}
	}




	/**
	 * @return array
	 */
	private function getProductIds () {

		/** @var array $result  */
		$result = array ();

		foreach ($this->getWishlistItems() as $item) {
			/** @var Mage_Wishlist_Model_Item $item */

			$result []= $item->getProductId();
		}

		df_result_array ($result);

		return $result;
	}
}
