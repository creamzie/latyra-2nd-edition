<?php

class Df_Checkout_Helper_Settings_Interface extends Df_Core_Helper_Settings {


	/**
	 * @return boolean
	 */
	public function needShowAllStepsAtOnce () {

		/** @var bool $result */
		static $result;

		if (!isset ($result)) {
			$result =
					df_enabled (Df_Core_Feature::CHECKOUT)
				&&
					$this->getYesNo (
						'df_checkout/interface/show_all_steps_at_once'
					)
			;
		}

		df_result_boolean ($result);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Checkout_Helper_Settings_Interface';
	}


	/**
	 * Например, для класса Df_checkoutRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_checkout_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		return
			df()->reflection()->getModelNameInMagentoFormat (
				self::getClass()
			)
		;
	}
}