<?php

class Df_Shipping_Helper_Data extends Mage_Core_Helper_Abstract {


	/**
	 * @param mixed $value
	 * @return Df_Shipping_Helper_Data
	 */
	public function assertPostalCodeDestination ($value) {

		if (df_empty ($value)) {
			df_error ('Укажите почтовый индекс адреса доставки.');
		}

		df_param_string ($value, 0);


		/** @var Zend_Validate_PostCode $validator */
		$validator =
			new Zend_Validate_PostCode (
				array (
					'locale' => Df_Core_Const::LOCALE__RUSSIAN
				)
			)
		;

		df_assert ($validator instanceof Zend_Validate_PostCode);

		if (!$validator->isValid ($value)) {
			df_error (
				sprintf (
					'Система не понимает указанный Вами почтовый индекс адреса доставки: «%s».'
					,
					$value
				)
			);
		}

		return $this;
	}



	/**
	 * @param mixed $value
	 * @return Df_Shipping_Helper_Data
	 */
	public function assertPostalCodeSource ($value) {

		if (df_empty ($value)) {
			df_error ('Администратор магазина должен указать почтовый индекс склада магазина.');
		}

		df_param_string ($value, 0);


		/** @var Zend_Validate_PostCode $validator */
		$validator =
			new Zend_Validate_PostCode (
				array (
					'locale' => 'ru_RU'
				)
			)
		;

		df_assert ($validator instanceof Zend_Validate_PostCode);

		if (!$validator->isValid ($value)) {
			df_error (
				sprintf (
					'Система не понимает указанный администратором почтовый индекс склада магазина: «%s».'
					,
					$value
				)
			);
		}

		return $this;
	}





	/**
	 * @return Mage_Shipping_Model_Shipping
	 */
	public function getMagentoMainShippingModel () {

		if (!isset ($this->_magentoMainShippingModel)) {

			/** @var Mage_Shipping_Model_Shipping $result  */
			$result =
				df_model (
					'shipping/shipping'
				)
			;

			df_assert ($result instanceof Mage_Shipping_Model_Shipping);

			$this->_magentoMainShippingModel = $result;
		}


		df_assert ($this->_magentoMainShippingModel instanceof Mage_Shipping_Model_Shipping);

		return $this->_magentoMainShippingModel;

	}


	/**
	* @var Mage_Shipping_Model_Shipping
	*/
	private $_magentoMainShippingModel;




	/**
	 * @param string $message
	 * @return Df_Shipping_Helper_Data
	 */
	public function log ($message) {

		df_log ($message, 'rm.shipping.log');

		return $this;
	}



	/**
	 * @return Df_Torg12_Helper_Data
	 */
	public function torg12 () {

		/** @var Df_Torg12_Helper_Data $result  */
		$result = Mage::helper (Df_Torg12_Helper_Data::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Torg12_Helper_Data);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Shipping_Helper_Data';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}