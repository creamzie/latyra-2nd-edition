<?php

class Df_Shipping_Helper_Settings_Product extends Df_Core_Helper_Settings {



	/**
	 * @return string
	 */
	public function getAttributeNameHeight () {

		/** @var string $result  */
		$result =
			Mage::getStoreConfig (
				'df_shipping/product/attribute_name__height'
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	public function getAttributeNameLength () {

		/** @var string $result  */
		$result =
			Mage::getStoreConfig (
				'df_shipping/product/attribute_name__length'
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	public function getAttributeNameWidth () {

		/** @var string $result  */
		$result =
			Mage::getStoreConfig (
				'df_shipping/product/attribute_name__width'
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return float
	 */
	public function getDefaultHeight () {

		/** @var float $result  */
		$result =
			floatval (
				Mage::getStoreConfig (
					'df_shipping/product/default__height'
				)
			)
		;

		df_result_float ($result);

		return $result;
	}




	/**
	 * @return float
	 */
	public function getDefaultLength () {

		/** @var float $result  */
		$result =
			floatval (
				Mage::getStoreConfig (
					'df_shipping/product/default__length'
				)
			)
		;

		df_result_float ($result);

		return $result;
	}



	/**
	 * @return float
	 */
	public function getDefaultWeight () {

		/** @var float $result  */
		$result =
			floatval (
				Mage::getStoreConfig (
					'df_shipping/product/default__weight'
				)
			)
		;

		df_result_float ($result);

		return $result;
	}




	/**
	 * @return float
	 */
	public function getDefaultWidth () {

		/** @var float $result  */
		$result =
			floatval (
				Mage::getStoreConfig (
					'df_shipping/product/default__width'
				)
			)
		;

		df_result_float ($result);

		return $result;
	}





	/**
	 * @return string
	 */
	public function getUnitsLength () {

		/** @var string $result  */
		$result =
			Mage::getStoreConfig (
				'df_shipping/product/units__length'
			)
		;

		df_result_string ($result);

		return $result;
	}


	

	/**
	 * @return string
	 */
	public function getUnitsWeight () {

		/** @var string $result  */
		$result =
			Mage::getStoreConfig (
				'df_shipping/product/units__weight'
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Shipping_Helper_Settings_Product';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}