<?php

class Df_Shipping_Model_Resource_Setup extends Mage_Core_Model_Resource_Setup {


	/**
	 * @return Df_Shipping_Model_Resource_Setup
	 */
	public function install_2_15_4 () {

		/** @var Df_Core_Model_Lib $libCore */
		$libCore = Mage::getSingleton ('df_core/lib');
		$libCore->init ();

		/** @var Df_Shipping_Model_Setup_2_15_4 $processor */
		$processor =
			df_model (
				Df_Shipping_Model_Setup_2_15_4::getNameInMagentoFormat()
				,
				array (
					Df_Shipping_Model_Setup_2_15_4::PARAM__SETUP => $this
				)
			)
		;

		df_assert ($processor instanceof Df_Shipping_Model_Setup_2_15_4);

		$processor->process();

		return $this;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Shipping_Model_Resource_Setup';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		return 'df_shipping/setup';
	}
}


