<?php

class Df_Shipping_Model_Request extends Df_Core_Model_Abstract {



	
	/**
	 * @return phpQueryObject
	 */
	public function getResponseAsPq () {
	
		if (!isset ($this->_responseAsPq)) {

			df_helper()->phpquery()->lib()->init();
	
			/** @var phpQueryObject $result  */
			$result = 
				phpQuery::newDocument (
					$this->getResponseAsText()
				)
			;
	
	
			df_assert ($result instanceof phpQueryObject);
	
			$this->_responseAsPq = $result;
		}
	
		df_assert ($this->_responseAsPq instanceof phpQueryObject);
	
		return $this->_responseAsPq;
	}
	
	
	/**
	* @var phpQueryObject
	*/
	private $_responseAsPq;	
	




	/**
	 * @return string
	 */
	public function getResponseAsText () {
	
		if (!isset ($this->_responseAsText)) {
	
			/** @var string $result  */
			$result = false;


			if ($this->needCacheResponse()) {

				$result =
					$this->getCache()->load (
						$this->getCacheKey_Shipping()
					)
				;

			}

			if (false === $result) {

				try {
					$result = $this->getResponse()->getBody();
				}
				catch (Exception $e) {
					$this->responseFailureHandle ($e);
				}


				if ($this->needCacheResponse()) {

					$this->getCache()
						->save (
							$result
							,
							$this->getCacheKey_Shipping()
						)
					;

				}

			}
	
	
			df_assert_string ($result);
	
			$this->_responseAsText = $result;
		}
	
	
		df_result_string ($this->_responseAsText);
	
		return $this->_responseAsText;
	}
	
	
	/**
	* @var string
	*/
	private $_responseAsText;




	/**
	 * @return Mage_Core_Model_Cache
	 */
	protected function getCache () {

		/** @var Mage_Core_Model_Cache $result  */
		$result = Mage::app()->getCacheInstance();

		df_assert ($result instanceof Mage_Core_Model_Cache);

		return $result;
	}



	/**
	 * Этот метод protected, потому что он перекрывается, в частности, модулем Деловые Линии.
	 *
	 * @return array
	 */
	protected function getCacheKeyParams () {

		/** @var array $result  */
		$result =
			array (
				get_class ($this)
				,
				md5 (
					$this->getUri()->__toString()
				)
			)
		;


		if ($this->isItPost()) {

			$result =
				array_merge (
					$result
					,
					/**
					 * Обратите внимание, что $this->getPostParameters()
					 * может вернуть многомерный массив (например, так происходит в модуле Df_Pec).
					 * Поэтому мы не используем "в лоб" array_merge, а используем http_build_query.
					 */
					array (
						'post' =>
								!df_empty ($this->getPostRawData())
							?
								$this->getPostRawData()
							:
								http_build_query ($this->getPostParameters())
					)
				)
			;
		}


		df_result_array ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	protected function getErrorMessage () {
		return Df_Core_Const::T_EMPTY;
	}





	/**
	 * @return array
	 */
	protected function getHeaders () {
		return array ();
	}



	/**
	 * @return array
	 */
	protected function getPostParameters () {

		/** @var array $result  */
		$result = $this->cfg (self::PARAM__POST_PARAMS, array ());

		df_result_array ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	protected function getPostRawData () {
		return Df_Core_Const::T_EMPTY;
	}




	/**
	 * @return string
	 */
	protected function getQueryHost () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM__QUERY_HOST, Df_Core_Const::T_EMPTY);

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return array
	 */
	protected function getQueryParams () {

		/** @var array $result  */
		$result = $this->cfg (self::PARAM__QUERY_PARAMS, array ());

		df_result_array ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	protected function getQueryPath () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM__QUERY_PATH, Df_Core_Const::T_EMPTY);

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return array
	 */
	protected function getRequestConfuguration () {
		return array ();
	}




	/**
	 * @return string
	 */
	protected function getRequestMethod () {
		return Zend_Http_Client::GET;
	}




	/**
	 * @return Df_Shipping_Model_Request
	 */
	protected function responseFailureDetect () {
		return $this;
	}




	/**
	 * @param Exception|string $message
	 * @return Df_Shipping_Model_Request
	 */
	protected function responseFailureHandle ($message) {

		$this->responseFailureLog ($message);

		if ($message instanceof Exception) {

			/** @var Exception $e */
			$e = $message;

			$message = $e->getMessage();

			if (!($e instanceof Df_Core_Exception_Client)) {

				df_log_exception ($e);

				$message =
					df_mage()->shippingHelper()->__ (
						Df_Shipping_Model_Carrier::T_INTERNAL_ERROR
					)
				;
			}

			/**
			 * Вообще говоря, причиной сбоя self::MESSAGE__ERROR_PARSING_BODY
			 * был некорректно работающий класс Varien_Http_Client.
			 * После замены его на Varien_Http_Client побобный сбой вроде не возникает.
			 */
			if (self::MESSAGE__ERROR_PARSING_BODY === $message) {
				$message = 'Служба доставки не смогла рассчитать тариф';
			}
		}

		df_error (
				!df_empty (
					$message
				)
			?
				$message
			:
				self::T__ERROR_MESSAGE__DEFAULT
		);

		return $this;
	}




	/**
	 * @return bool
	 */
	protected function needCacheResponse () {
		return true;
	}




	/**
	 * @return bool
	 */
	protected function needLogNonExceptionErrors () {
		return true;
	}




	/**
	 * 	Некоторые калькуляторы допускают несколько одноимённых опций.
	 *  http_build_query кодирует их как a[0]=1&a[1]=2&a[2]=3
	 *  Если калькулятору нужно получить a=1&a=2&a=3,
	 *  то перекройте этот метод и верните true.
	 *
	 * @return bool
	 */
	protected function needPostKeysWithSameName () {
		return false;
	}




	/**
	 * @return string
	 */
	private function getCacheKey_Shipping () {

		if (!isset ($this->_cacheKey)) {

			/** @var string $result  */
			$result =
				implode (
					'--'
					,
					$this->getCacheKeyParams ()
				)
			;

			df_assert_string ($result);

			$this->_cacheKey = $result;
		}


		df_result_string ($this->_cacheKey);

		return $this->_cacheKey;

	}


	/**
	* @var string
	*/
	private $_cacheKey;




	/**
	 * @return Zend_Http_Response
	 */
	private function getResponse () {

		if (!isset ($this->_response)) {

			/**
			 * Обратите внимание,
			 * что мы используем класс Zend_Http_Client, а не Varien_Http_Client,
			 * потому что применение Varien_Http_Client зачастую приводит к сбою:
			 * Error parsing body - doesn't seem to be a chunked message
			 *
			 * @var Zend_Http_Client $httpClient
			 */
			$httpClient = new Zend_Http_Client ();


			$httpClient
				->setHeaders (
					$this->getHeaders()
				)
				->setUri (
					$this->getUri()
				)
				->setConfig (
					array_merge (
						array (

							/**
							 * в секундах
							 */
							'timeout' => 10
						)
						,
						$this->getRequestConfuguration ()
					)
				)
			;


			if ($this->isItPost()) {

				if (!df_empty ($this->getPostRawData())) {
					$httpClient
						->setRawData(
							$this->getPostRawData()
						)
					;
				}
				else {
					if (!$this->needPostKeysWithSameName ()) {
						$httpClient
							->setParameterPost (
								$this->getPostParameters()
							)
						;
					}
					else {
						$httpClient
							->setRawData (
								/**
								 * Некоторые калькуляторы допускают несколько одноимённых опций.
								 * http_build_query кодирует их как a[0]=1&a[1]=2&a[2]=3
								 * Чтобы убрать квадратные скобки, используем регулярное выражение
								 *
								 * @link http://www.php.net/manual/en/function.http-build-query.php#78603
								 */
								preg_replace (
									'#%5B(?:[0-9]|[1-9][0-9]+)%5D=#u'
									,
									'='
									,
									http_build_query (
										$this->getPostParameters()
										,
										''
										,
										'&'
									)
								)
							)
						;
					}
				}
			}


			/** @var Zend_Http_Response $result  */
			$result =
				$httpClient->request (
					$this->getRequestMethod()
				)
			;


			df_assert ($result instanceof Zend_Http_Response);

			$this->_response = $result;
		}


		df_assert ($this->_response instanceof Zend_Http_Response);

		return $this->_response;

	}


	/**
	* @var Zend_Http_Response
	*/
	private $_response;




	/**
	 * @return Zend_Uri_Http
	 */
	private function getUri () {

		if (!isset ($this->_uri)) {

			/** @var Zend_Uri_Http $result  */
			$result = Zend_Uri::factory ('http');

			$result->setHost ($this->getQueryHost());
			$result->setPath ($this->getQueryPath());
			$result
				->setQuery (
					df_clean (
						$this->getQueryParams()
					)
				)
			;


			df_assert ($result instanceof Zend_Uri_Http);

			$this->_uri = $result;
		}


		df_assert ($this->_uri instanceof Zend_Uri_Http);

		return $this->_uri;

	}


	/**
	* @var Zend_Uri_Http
	*/
	private $_uri;

	
	



	/**
	 * @return bool
	 */
	private function isItPost () {

		/** @var bool $result  */
		$result = (Zend_Http_Client::POST === $this->getRequestMethod());

		df_result_boolean ($result);

		return $result;
	}





	/**
	 * @param Exception|string $message
	 * @return Df_Shipping_Model_Request
	 */
	private function responseFailureLog ($message) {

		/** @var bool $isException */
		$isException = ($message instanceof Exception);

		if ($isException) {
			$message = $message->getMessage();
		}

		if ($isException || $this->needLogNonExceptionErrors()) {

			/** @var array $logRecordParts  */
			$logRecordParts = array (self::T__ERROR_MESSAGE__DEFAULT);

			$logRecordParts []=
				sprintf (
					'Модуль: %s'
					,
					df_a (
						explode (
							'_'
							,
							get_class ($this)
						)
						,
						1
					)
				)
			;

			if (!df_empty($message)) {
				$logRecordParts []= $message;
			}

			$logRecordParts []=
				sprintf (
					'Адрес: %s'
					,
					$this->getUri()->__toString()
				)
			;

			$logRecordParts []=
				print_r (
						$this->isItPost()
					?
						(
								!df_empty ($this->getPostRawData())
							?
								$this->getPostRawData()
							:
								$this->getPostParameters()
						)
					:
						$this->getUri()->getQueryAsArray()
					,
					true
				)
			;


			df_helper()->shipping()
				->log (
					implode (
						"\n"
						,
						$logRecordParts
					)
				)
			;
		}

		return $this;
	}




	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__POST_PARAMS, new Df_Zf_Validate_Array(), false
			)
			->addValidator (
				self::PARAM__QUERY_HOST, new Df_Zf_Validate_String(), false
			)
			->addValidator (
				self::PARAM__QUERY_PARAMS, new Df_Zf_Validate_Array(), false
			)
			->addValidator (
				self::PARAM__QUERY_PATH, new Df_Zf_Validate_String(), false
			)
		;
	}




	const MESSAGE__ERROR_PARSING_BODY = "Error parsing body - doesn't seem to be a chunked message";



	const NO_INTERNET = false;

	const PARAM__POST_PARAMS = 'post_params';

	const PARAM__QUERY_HOST = 'query_host';
	const PARAM__QUERY_PARAMS = 'query_params';
	const PARAM__QUERY_PATH = 'query_path';


	const T__ERROR_MESSAGE__DEFAULT = 'Обращение к программному интерфейсу службы доставки привело к сбою.';




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Shipping_Model_Request';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


