<?php

abstract class Df_Shipping_Model_ConfigManager extends Df_Core_Model_Abstract {


	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getKeyBase ();



	/**
	 * @return Df_Shipping_Model_Carrier
	 */
	protected function getShippingMethod () {

		/** @var Df_Shipping_Model_Carrier $result  */
		$result = $this->cfg (self::PARAM__SHIPPING_METHOD);

		df_assert ($result instanceof Df_Shipping_Model_Carrier);

		return $result;
	}



	/**
	 * @param string $key
	 * @return string
	 */
	protected function preprocessKey ($key) {

		df_param_string ($key, 0);

		/** @var string $result  */
		$result =
			df()->config()->implodeKey (
				array (
					$this->getKeyBase()
					,
					$this->getShippingMethod()->getRmId()
					,
					$key
				)
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM__SHIPPING_METHOD, Df_Shipping_Model_Carrier::getClass()
			)
		;
	}




	const PARAM__SHIPPING_METHOD = 'shipping_method';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Shipping_Model_ConfigManager';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}

