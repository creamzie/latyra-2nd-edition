<?php

class Df_Shipping_Model_Config_Backend_Validator_Strategy_Origin_SpecificCountry
	extends Df_Shipping_Model_Config_Backend_Validator_Strategy_Origin {


	/**
	 * @override
	 * @return bool
	 */
	public function validate () {

		/** @var bool $result */
		$result = true;

		if (
				!is_null ($this->getLimitationCountry())
			&&
				(
						$this->getLimitationCountry()->getIso2Code()
					!==
						$this->getOrigin()->getCountryId()
				)
		) {
			$result = false;
			$this->getBackend()->getMessages()
				->addMessage (
					new Mage_Core_Model_Message_Error (
						strtr (
									"Модуль «%module-title%» в настоящее время работает только с грузами, отправляемыми из страны «%limitation-country%»."
								.
									"<br/>У Вас же в качестве страны расположения склада магазина указано «%store-country%»."
								.
									"<br/>Вам нужно в качестве страны расположения склада магазина указать «%limitation-country%»."
								.
									"<br/>Эта настройка расположена в разделе: «Система» → «Настройки» → «Продажи» → «Доставка: общие настройки» → «Расположение магазина» → «Страна»."
							,
							array (
								'%limitation-country%' => $this->getLimitationCountry()->getName()
								,
								'%module-title%' => $this->getModuleTitle()
								,
								'%store-country%' =>
										is_null ($this->getOrigin()->getCountry())
									?
										'пусто'
									:
										$this->getOrigin()->getCountry()->getName()
							)
						)
					)
				)
			;
		}

		df_result_boolean ($result);

		return $result;
	}




	/**
	 * @return Mage_Directory_Model_Country|null
	 */
	private function getLimitationCountry () {

		if (!isset ($this->_limitationCountry) && !$this->_limitationCountryIsNull) {

			/** @var string|null $countryId */
			$countryId =
				$this->getBackend()->getFieldConfigParam ('df_origin_country');
			;

			/** @var Mage_Directory_Model_Country|null $result  */
			$result =
					df_empty ($countryId)
				?
					null
				:
					df_helper()->directory()->country()->getByIso2Code ($countryId)
			;

			if (!is_null ($result)) {
				df_assert ($result instanceof Mage_Directory_Model_Country);
			}
			else {
				$this->_limitationCountryIsNull = true;
			}

			$this->_limitationCountry = $result;
		}


		if (!is_null ($this->_limitationCountry)) {
			df_assert ($this->_limitationCountry instanceof Mage_Directory_Model_Country);
		}

		return $this->_limitationCountry;
	}


	/**
	* @var Mage_Directory_Model_Country|null
	*/
	private $_limitationCountry;

	/**
	 * @var bool
	 */
	private $_limitationCountryIsNull = false;






	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Shipping_Model_Config_Backend_Validator_Strategy_Origin_SpecificCountry';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


