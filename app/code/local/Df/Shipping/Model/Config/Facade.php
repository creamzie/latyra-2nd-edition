<?php

class Df_Shipping_Model_Config_Facade extends Df_Shipping_Model_Config_Abstract {


	/**
	 * @return Df_Shipping_Model_Config_Area_Admin
	 */
	public function admin () {

		/** @var Df_Shipping_Model_Config_Area_Admin $result  */
		$result =
			$this->getConfig (
				$this->getAdminConfigClassMf()
			)
		;

		df_assert ($result instanceof Df_Shipping_Model_Config_Area_Admin);

		return $result;
	}



	/**
	 * @return Df_Shipping_Model_Config_Area_Frontend
	 */
	public function frontend () {

		/** @var Df_Shipping_Model_Config_Area_Frontend $result  */
		$result =
			$this->getConfig (
				$this->getFrontendConfigClassMf()
			)
		;

		df_assert ($result instanceof Df_Shipping_Model_Config_Area_Frontend);

		return $result;
	}



	/**
	 * @return Df_Shipping_Model_Config_Area_No
	 */
	public function noArea () {

		/** @var Df_Shipping_Model_Config_Area_No $result  */
		$result =
			$this->getConfig (
				Df_Shipping_Model_Config_Area_No::getNameInMagentoFormat()
			)
		;

		df_assert ($result instanceof Df_Shipping_Model_Config_Area_No);

		return $result;
	}




	/**
	 * @return Df_Shipping_Model_Config_Area_Service
	 */
	public function service () {

		/** @var Df_Shipping_Model_Config_Area_Service $result  */
		$result =
			$this->getConfig (
				$this->getServiceConfigClassMf()
			)
		;

		df_assert ($result instanceof Df_Shipping_Model_Config_Area_Service);

		return $result;
	}




	/**
	 * Получаем заданное ранее администратором
	 * значение конкретной настройки платёжного способа
	 *
	 * @override
	 * @param string $key
	 * @param mixed $defaultValue [optional]
	 * @return mixed
	 */
	public function getVar ($key, $defaultValue = null) {

		df_param_string ($key, 0);

		/** @var mixed $result  */
		$result = $defaultValue;


		/** @var Df_Shipping_Model_Config_Abstract $processor */
		$processor = $this->getConcreteConfigForKey ($key);

		df_assert ($processor instanceof Df_Shipping_Model_Config_Abstract);


		if ($processor) {
			$result =
				$processor->getVar ($key, $defaultValue)
			;
		}

		return $result;
	}




	/**
	 * @return string
	 */
	protected function getAdminConfigClassMf () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM__CONFIG_CLASS__ADMIN);

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	protected function getFrontendConfigClassMf () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM__CONFIG_CLASS__FRONTEND);

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	protected function getServiceConfigClassMf () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM__CONFIG_CLASS__SERVICE);

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return array
	 */
	protected function getUncertainKeyProcessors () {

		/** @var array $result  */
		$result =
			array (
				$this->frontend()
				,
				$this->admin()
				,
				$this->service()
			)
		;


		df_result_array ($result);

		return $result;
	}




	/**
	 * @param string $uncertainKey
	 * @return Df_Shipping_Model_Config_Abstract
	 */
	private function getConcreteConfigForKey ($uncertainKey) {

		df_param_string ($uncertainKey, 0);


		/** @var Df_Shipping_Model_Config_Abstract $result  */
		$result = null;


		foreach ($this->getUncertainKeyProcessors() as $processor) {

			/** @var Df_Shipping_Model_Config_Abstract $processor */
			df_assert ($processor instanceof Df_Shipping_Model_Config_Abstract);

			if ($processor->canProcessUncertainKey ($uncertainKey)) {
				$result = $processor;
			}
		}


		if (is_null ($result)) {
			$result = $this->noArea();
		}


		df_assert ($result instanceof Df_Shipping_Model_Config_Abstract);

		return $result;
	}




	/**
	 * @param string $configClass
	 * @return Df_Shipping_Model_Config_Abstract
	 */
	private function getConfig ($configClass) {

		df_param_string ($configClass, 0);

		if (!isset ($this->_config[$configClass])) {

			/** @var Df_Shipping_Model_Config_Abstract $result  */
			$result =
				df_model (
					$configClass
					,
					array (
						Df_Shipping_Model_Config_Abstract::PARAM__CONST_MANAGER =>
							$this->getConstManager()
						,
						Df_Shipping_Model_Config_Abstract::PARAM__VAR_MANAGER =>
							$this->getVarManager()
					)
				)
			;


			df_assert ($result instanceof Df_Shipping_Model_Config_Abstract);

			$this->_config[$configClass] = $result;
		}


		df_assert ($this->_config[$configClass] instanceof Df_Shipping_Model_Config_Abstract);

		return $this->_config[$configClass];

	}


	/**
	* @var Df_Shipping_Model_Config_Abstract[]
	*/
	private $_config = array ();




	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__CONFIG_CLASS__ADMIN, new Df_Zf_Validate_String()
			)
			->addValidator (
				self::PARAM__CONFIG_CLASS__FRONTEND, new Df_Zf_Validate_String()
			)
			->addValidator (
				self::PARAM__CONFIG_CLASS__SERVICE, new Df_Zf_Validate_String()
			)
		;
	}



	const PARAM__CONFIG_CLASS__ADMIN = 'config_class__admin';
	const PARAM__CONFIG_CLASS__FRONTEND = 'config_class__frontend';
	const PARAM__CONFIG_CLASS__SERVICE = 'config_class__shipping_service';





	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Shipping_Model_Config_Facade';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


