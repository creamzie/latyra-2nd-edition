<?php

class Df_Shipping_Model_Config_Area_Admin extends Df_Shipping_Model_Config_Area_Abstract {



	/**
	 * @return float
	 */
	public function getDeclaredValuePercent () {

		/** @var float $result  */
		$result =
			floatval (
				$this->getVar (
					self::KEY__VAR__DECLARED_VALUE_PERCENT
					,
					0.0
				)
			)
		;

		df_result_float ($result);

		return $result;
	}



	/**
	 * @return float
	 */
	public function getFeeFixed () {

		/** @var float $result  */
		$result =
			floatval (
				$this->getVar (
					self::KEY__VAR__FEE_FIXED
					,
					0.0
				)
			)
		;

		df_result_float ($result);

		return $result;
	}




	/**
	 * @return float
	 */
	public function getFeePercent () {

		/** @var float $result  */
		$result =
			floatval (
				$this->getVar (
					self::KEY__VAR__FEE_PERCENT
					,
					0.0
				)
			)
		;

		df_result_float ($result);

		return $result;
	}






	/**
	 * @override
	 * @return string
	 */
	protected function getAreaPrefix () {

		/** @var string $result  */
		$result = self::AREA_PREFIX;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @override
	 * @return array
	 */
	protected function getUncertainKeys () {

		/** @var array $result  */
		$result =
			array_merge (
				parent::getUncertainKeys()
				,
				array (
				)
			)
		;

		df_result_array ($result);

		return $result;
	}




	const AREA_PREFIX = 'admin';

	const KEY__VAR__DECLARED_VALUE_PERCENT = 'declared_value_percent';
	const KEY__VAR__FEE_FIXED = 'fee_fixed';
	const KEY__VAR__FEE_PERCENT = 'fee_percent';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Shipping_Model_Config_Area_Admin';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


