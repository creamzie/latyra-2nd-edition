<?php

class Df_Shipping_Model_Origin extends Df_Core_Model_Abstract {
	
		
	/**
	 * @return string
	 */
	public function getCity () {
		return 
			df_trim (
				df_convert_null_to_empty_string (				
					$this->cfg (self::PARAM__CITY)					
				)				
			)
		;
	}	
	
	
	
	
	/**
	 * @return Mage_Directory_Model_Country|null
	 */
	public function getCountry () {
	
		if (!isset ($this->_country) && !$this->_countryIsNull) {
	
			/** @var Mage_Directory_Model_Country|null $result  */
			$result =
					df_empty ($this->getCountryId())
				?
					null
				:
					df_helper()->directory()->country()->getByIso2Code (
						$this->getCountryId()
					)
			;
	
			if (!is_null ($result)) {
				df_assert ($result instanceof Mage_Directory_Model_Country);
			}
			else {
				$this->_countryIsNull = true;
			}
	
			$this->_country = $result;
		}
	
	
		if (!is_null ($this->_country)) {
			df_assert ($this->_country instanceof Mage_Directory_Model_Country);
		}		
		
		return $this->_country;
	}
	
	
	/**
	* @var Mage_Directory_Model_Country|null
	*/
	private $_country;	
	
	/**
	 * @var bool
	 */
	private $_countryIsNull = false;	
	
	
	
	
	/**
	 * @return string
	 */
	public function getCountryId () {
		return $this->cfg (self::PARAM__COUNTRY_ID);
	}		
	
	
	
	/**
	 * @return string
	 */
	public function getPostalCode () {
		return 
			df_trim (
				df_convert_null_to_empty_string (				
					$this->cfg (self::PARAM__POSTAL_CODE)					
				)				
			)
		;
	}




	/**
	 * @return Mage_Directory_Model_Region|null
	 */
	private function getRegion () {

		if (!isset ($this->_region) && !$this->_regionIsNull) {

			/** @var Mage_Directory_Model_Region|null $result  */
			$result = null;

			if (0 !== $this->getRegionId()) {
				$result =
					df_model (
						Df_Directory_Const::REGION_CLASS_MF
					)
				;
				$result->load ($this->getRegionId());

				if (0 === intval ($result->getId())) {
					$result = null;
				}
			}

			if (!is_null ($result)) {
				df_assert ($result instanceof Mage_Directory_Model_Region);
			}
			else {
				$this->_regionIsNull = true;
			}

			$this->_region = $result;
		}


		if (!is_null ($this->_region)) {
			df_assert ($this->_region instanceof Mage_Directory_Model_Region);
		}

		return $this->_region;
	}


	/**
	* @var Mage_Directory_Model_Region|null
	*/
	private $_region;

	/**
	 * @var bool
	 */
	private $_regionIsNull = false;

	
	
	
	
	/**
	 * @return int
	 */
	public function getRegionId () {
		return 
			intval (
				$this->cfg (self::PARAM__COUNTRY_ID)				
			)
		;
	}




	/**
	 * @return string
	 */
	public function getRegionName () {
		return
				!is_null ($this->getRegion())
			?
				$this->getRegion()->getName()
			:
				df_trim (
					df_convert_null_to_empty_string (
						$this->cfg (self::PARAM__REGION_NAME)
					)
				)
		;
	}
	
	



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__CITY, new Df_Zf_Validate_String(), $isRequired = false
			)
			->addValidator (
				self::PARAM__COUNTRY_ID, new Df_Zf_Validate_String(), $isRequired = false
			)
			->addValidator (
				self::PARAM__POSTAL_CODE, new Df_Zf_Validate_String(), $isRequired = false
			)
			->addValidator (
				self::PARAM__REGION_NAME, new Df_Zf_Validate_String(), $isRequired = false
			)
			->addValidator (
				self::PARAM__REGION_ID, new Df_Zf_Validate_Int(), $isRequired = false
			)
		;
	}


	const PARAM__CITY = 'city';
	const PARAM__COUNTRY_ID = 'country_id';
	const PARAM__POSTAL_CODE = 'postal_code';
	const PARAM__REGION_NAME = 'region_name';
	const PARAM__REGION_ID = 'region_id';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Shipping_Model_Origin';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}
