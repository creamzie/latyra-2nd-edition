<?php

class Df_Shipping_Model_Setup_2_15_4 extends Df_Core_Model_Setup {


	/**
	 * @return Df_Shipping_Model_Setup_2_15_4
	 */
	public function process () {

		foreach (df_helper()->dataflow()->registry()->attributeSets() as $attributeSet) {

			/** @var Mage_Eav_Model_Entity_Attribute_Set $attributeSet */
			df_assert ($attributeSet instanceof Mage_Eav_Model_Entity_Attribute_Set);

			foreach ($this->getAttributeMap() as $ordering => $attribute) {
				/** @var int $ordering */
				/** @var Mage_Eav_Model_Entity_Attribute_Set $attribute */

				Df_Catalog_Model_Installer_AddAttributeToSet
					::processStatic (
						$attribute->getAttributeCode()
						,
						$attributeSet->getId()
						,
						Df_Catalog_Model_Installer_AddAttributeToSet::GROUP_NAME__GENERAL
						,
						$ordering
					)
				;
			}
		}

		/**
		 * Вот в таких ситуациях, когда у нас меняется структура прикладного типа товаров,
		 * нам нужно сбросить глобальный кэш EAV.
		 */

		df_helper()->eav()->cleanCache();

		return $this;
	}

	
	
	
	/**
	 * @param string $code
	 * @param string $label
	 * @param int $ordering
	 * @return Mage_Catalog_Model_Resource_Eav_Attribute
	 */
	private function getAttribute ($code, $label, $ordering) {
	
		if (!isset ($this->_attribute [$code])) {
	
			/** @var Mage_Catalog_Model_Resource_Eav_Attribute $result  */
			$result = 	
				df_helper()->dataflow()->registry()->attributes()
					->findByCodeOrCreate (
						$code
						,
						array (
							/**
							 * Класс объектов для свойства (товары, покупатели...)
							 */
							'entity_type_id' => df_helper()->eav()->getProductEntityTypeId()

							,
							/**
							 * Код свойства
							 */
							'attribute_code' => $code

							,
							/**
							 * Доступность свойства («всеобщая», «витрина», «сайт»)
							 */
							'is_global' => 1

							,
							/**
							 * Элемент управления для администратора
							 */
							'frontend_input' => 'text'

							,
							/**
							 * Значение по умолчанию
							 */
							'default_value' => null

							,
							/**
							 * Требовать ли от администратора
							 * уникальности значения данного свойства
							 * для каждого товара?
							 */
							'is_unique' => 0

							,
							/**
							 * Обязательно ли для заполнения?
							 */
							'is_required' => 0

							,
							/**
							 * Административная проверка
							 */
							'frontend_class' => Df_Eav_Const::FRONTEND_CLASS__NATURAL_NUMBER

							,
							'attribute_model' => null

							,
							'backend_model' => Df_Core_Const::T_EMPTY

							,
							'backend_type' => 'int'

							,
							'backend_table' => null

							,
							'frontend_model' => null

							,
							'frontend_label' => $label

							,
							'source_model' => Df_Core_Const::T_EMPTY

							,
							'is_user_defined' => 1

							,
							'note' => null

							,
							'frontend_input_renderer' => null

							,
							'is_visible' => 1

							,
							/**
							 * Должна ли система учитывать
							 * вхождения искомой посетителем магазина фразы
							 * в значение данного свойства при полнотекстовом поиске?
							 */
							'is_searchable' => 0

							,
							/**
							 * Использовать ли в качестве критерия расширенного поиска?
							 */
							'is_visible_in_advanced_search' => 0

							,
							/**
							 * Показывать ли в таблице сравнения товаров?
							 */
							'is_comparable' => 1

							,
							/**
							 * Использовать ли для пошаговой фильтрации?
							 */
							'is_filterable' => 0

							,
							/**
							 * Использовать ли для пошаговой фильтрации результатов поиска?
							 */
							'is_filterable_in_search' => 0

							,
							/**
							 * Позволять ли администратору использовать данное свойство
							 * в качестве условия ценовых правил?
							 */
							'is_used_for_promo_rules' => 0

							,
							/**
							 * @deprecated
							 * Позволять ли администратору использовать данное свойство
							 * в качестве условия ценовых правил?
							 * (используется в Magento 1.4.0.1,
							 * не используется в современных версиях Magento)
							 */
							'is_used_for_price_rules' => 0

							,
							/**
							 * Порядковый номер свойства в блоке пошаговой фильтрации
							 */
							'position' => 0

							,
							/**
							 * Может ли значение содержать теги HTML?
							 */
							'is_html_allowed_on_front' => 0

							,
							/**
							 * Показывать ли на витринной товарной карточке?
							 */
							'is_visible_on_front' => 1

							,
							/**
							 * Показывавать ли в списке товаров на витрине?
							 */
							'used_in_product_listing' => 0

							,
							/**
							 * Давать ли покупателю возможность упорядочивать товары
							 * по значениям данного свойства?
							 */
							'used_for_sort_by' => 0

							,
							/**
							 * Позволять ли администратору использовать данное свойство
							 * в качестве опции настраиваемого товара?
							 */
							'is_configurable' => 0

							,
							/**
							 * Разрешать ли режим полного соответствия текстового редактора
							 * при редактировании администратором значения данного свойства?
							 */
							'is_wysiwyg_enabled' => 0
						)
						,
						$ordering
					)
			;
	
			df_assert ($result instanceof Mage_Catalog_Model_Resource_Eav_Attribute);
	
			$this->_attribute [$code] = $result;
		}
	
		df_assert ($this->_attribute [$code] instanceof Mage_Catalog_Model_Resource_Eav_Attribute);
	
		return $this->_attribute [$code];
	}
	
	
	/**
	* @var Mage_Catalog_Model_Resource_Eav_Attribute[]
	*/
	private $_attribute = array ();




	/**
	 * @return Mage_Catalog_Model_Resource_Eav_Attribute
	 */
	private function getAttributeHeight () {

		/** @var Mage_Catalog_Model_Resource_Eav_Attribute $result  */
		$result =
			$this->getAttribute (
				Df_Catalog_Model_Product::PARAM__HEIGHT
				,
				'Высота'
				,
				self::ORDERING_OFFSET + 2
			)
		;

		df_assert ($result instanceof Mage_Catalog_Model_Resource_Eav_Attribute);

		return $result;
	}





	/**
	 * @return Mage_Catalog_Model_Resource_Eav_Attribute
	 */
	private function getAttributeLength () {

		/** @var Mage_Catalog_Model_Resource_Eav_Attribute $result  */
		$result =
			$this->getAttribute (
				Df_Catalog_Model_Product::PARAM__LENGTH
				,
				'Длина'
				,
				self::ORDERING_OFFSET
			)
		;

		df_assert ($result instanceof Mage_Catalog_Model_Resource_Eav_Attribute);

		return $result;
	}




	/**
	 * @return Mage_Catalog_Model_Resource_Eav_Attribute[]
	 */
	private function getAttributeMap () {

		/** @var Mage_Catalog_Model_Resource_Eav_Attribute[] $result  */
		$result =
			array (
				self::ORDERING_OFFSET => $this->getAttributeLength()
				,
				self::ORDERING_OFFSET + 1 => $this->getAttributeWidth()
				,
				self::ORDERING_OFFSET + 2 => $this->getAttributeHeight()
			)
		;

		df_result_array ($result);

		return $result;
	}





	/**
	 * @return Mage_Catalog_Model_Resource_Eav_Attribute
	 */
	private function getAttributeWidth () {

		/** @var Mage_Catalog_Model_Resource_Eav_Attribute $result  */
		$result =
			$this->getAttribute (
				Df_Catalog_Model_Product::PARAM__WIDTH
				,
				'Ширина'
				,
				self::ORDERING_OFFSET + 1
			)
		;

		df_assert ($result instanceof Mage_Catalog_Model_Resource_Eav_Attribute);

		return $result;
	}




	const ORDERING_OFFSET = 100;



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Shipping_Model_Setup_2_15_4';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


