<?php

class Df_Shipping_Model_ConfigManager_Const extends Df_Shipping_Model_ConfigManager {



	/**
	 * @return array
	 */
	public function getAvailableShippingMethodsAsCanonicalConfigArray () {

		if (!isset ($this->_availableShippingMethodsAsCanonicalConfigArray)) {


			/** @var string $configKey  */
			$configKey =
				df()->config()->implodeKey (
					array (
						self::KEY__SHIPPING_METHODS
					)
				)
			;

			df_assert_string ($configKey);


			/** @var Mage_Core_Model_Config_Element|null $node */
			$node =
				$this->getNode (
					$configKey
					,
					false
				)
			;


			/** @var array $result  */
			$result =
					is_null ($node)
				?
					array ()
				:
					$node->asCanonicalArray()
			;

			df_assert_array ($result);

			$this->_availableShippingMethodsAsCanonicalConfigArray = $result;
		}


		df_result_array ($this->_availableShippingMethodsAsCanonicalConfigArray);

		return $this->_availableShippingMethodsAsCanonicalConfigArray;

	}


	/**
	* @var array
	*/
	private $_availableShippingMethodsAsCanonicalConfigArray;




	/**
	 * Способы оплаты, предоставляемые данной платёжной системой
	 * @return array
	 */
	public function getAvailableShippingMethodsAsOptionArray () {

		if (!isset ($this->_availableShippingMethodsAsOptionArray)) {

			/** @var array $result  */
			$result = array ();


			foreach ($this->getAvailableShippingMethodsAsCanonicalConfigArray ()
				as $methodCode => $methodOptions) {

				/** @var string $methodCode */
				/** @var array $methodOptions */

				df_assert_string ($methodCode);
				df_assert_array ($methodOptions);


				/** @var string $methodTitle */
				$methodTitle =
					df_a ($methodOptions, self::KEY__TITLE)
				;

				df_assert_string ($methodTitle);


				$result []=
					array (
						Df_Admin_Model_Config_Source::OPTION_KEY__LABEL => $methodTitle
						,
						Df_Admin_Model_Config_Source::OPTION_KEY__VALUE => $methodCode
					)
				;

			}


			df_assert_array ($result);

			$this->_availableShippingMethodsAsOptionArray = $result;
		}


		df_result_array ($this->_availableShippingMethodsAsOptionArray);

		return $this->_availableShippingMethodsAsOptionArray;

	}


	/**
	* @var array
	*/
	private $_availableShippingMethodsAsOptionArray;



	
	
	
	/**
	 * @param string $key
	 * @param bool $canBeTest [optional]
	 * @return Mage_Core_Model_Config_Element|null
	 */
	public function getNode (
		$key
		,
		$canBeTest = true
	) {

		df_param_string ($key, 0);
		df_param_boolean ($canBeTest, 1);


		if ($canBeTest) {
			$key =
				df()->config()->implodeKey (
					array (
							$this->getShippingMethod()->isTestMode()
						?
							self::KEY__TEST
						:
							self::KEY__PRODUCTION
						,
						$key
					)
				)
			;
		}

	
		if (!isset ($this->_node[$key][$canBeTest])) {
	
			/** @var Mage_Core_Model_Config_Element|null $result  */
			$result =
				df()->config()->getNodeByKey (
					$this->preprocessKey (
						$key
					)
				)
			;


			if (is_null ($result)) {

				/**
				 * Пробуем получить стандартное значение параметра:
				 * из настроек модуля Df_Shipping
				 */

				$result =
					df()->config()->getNodeByKey (
						$this->preprocessKeyDefault (
							$key
						)
					)
				;

			}


			if (!is_null ($result)) {
				df_assert ($result instanceof Mage_Core_Model_Config_Element);
			}

	
			$this->_node[$key][$canBeTest] = $result;
		}
	
		if (!is_null ($this->_node[$key][$canBeTest])) {
			df_assert ($this->_node[$key][$canBeTest] instanceof Mage_Core_Model_Config_Element);
		}
	
		return $this->_node[$key][$canBeTest];
	}
	
	
	/**
	* @var mixed[]
	*/
	private $_node = array ();




	/**
	 * @param string $key
	 * @param bool $canBeTest [optional]
	 * @param string $defaultValue [optional]
	 * @return string
	 */
	public function getUrl (
		$key
		,
		$canBeTest = true
		,
		$defaultValue = Df_Core_Const::T_EMPTY
	) {

		df_param_string ($key, 0);
		df_param_boolean ($canBeTest, 1);
		df_param_string ($defaultValue, 2);


		/** @var string $result  */
		$result =
			$this->getValue (
				df()->config()->implodeKey (
					array (
						self::KEY__URL
						,
						$key
					)
				)
				,
				$canBeTest
			)
		;

		df_result_string ($result);

		return $result;
	}





	/**
	 * @param string $key    
	 * @param bool $canBeTest
	 * @param string $defaultValue [optional]
	 * @return string
	 */
	public function getValue (
		$key
		,
		$canBeTest
		,
		$defaultValue = Df_Core_Const::T_EMPTY
	) {

		df_param_string ($key, 0);
		df_param_boolean ($canBeTest, 1);
		df_param_string ($defaultValue, 2);


		/** @var string $result  */
		$result =
			df()->config()->getNodeValueAsString (
				$this->getNode ($key, $canBeTest)
			)
		;

		if (df_empty ($result)) {

			if ($canBeTest) {

				/**
				 * Пробуем получить значение без приставок test/production
				 */
				$result =
					df()->config()->getNodeValueAsString (
						$this->getNode ($key, !$canBeTest)
					)
				;

			}
		}

		if (df_empty ($result)) {
			$result = $defaultValue;
		}

		df_result_string ($result);

		return $result;
	}



	/**
	 * @override
	 * @return string
	 */
	protected function getKeyBase () {
		return self::KEY__BASE;
	}





	/**
	 * @param string $key
	 * @return string
	 */
	protected function preprocessKeyDefault ($key) {

		df_param_string ($key, 0);

		/** @var string $result  */
		$result =
			df()->config()->implodeKey (
				array (
					self::KEY__BASE
					,
					self::KEY__DEFAULT
					,
					$key
				)
			)
		;

		df_result_string ($result);

		return $result;
	}
	
	



	const KEY__ALLOWED = 'allowed';
	const KEY__BASE = 'df/shipping';
	const KEY__DEFAULT = 'default';
	const KEY__SHIPPING_METHODS = 'shipping-methods';
	const KEY__PRODUCTION = 'production';
	const KEY__TEST = 'test';
	const KEY__TITLE = 'title';
	const KEY__URL = 'url';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Shipping_Model_ConfigManager_Const';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


