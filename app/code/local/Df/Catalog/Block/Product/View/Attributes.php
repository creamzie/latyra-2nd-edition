<?php

class Df_Catalog_Block_Product_View_Attributes extends Mage_Catalog_Block_Product_View_Attributes {


	/**
	 * @override
	 * @return string
	 */
	public function __ () {
		/** @var array $args  */
		$args = func_get_args();

		/** @var string $result  */
		$result = df_helper()->localization()->translation()->translateByParent ($args, $this);

		return $result;
	}



    /**
     * $excludeAttr is optional array of attribute codes to
     * exclude them from additional data array
     *
	 * @override
     * @param array $excludeAttr
     * @return array
     */
    public function getAdditionalData(array $excludeAttr = array())
    {
        return
					df_module_enabled (Df_Core_Module::TWEAKS)
				&&
					df_enabled (Df_Core_Feature::TWEAKS)
				&&
					df_cfg ()->tweaks()->catalog()->product()->view()->getHideEmptyAttributes()
			?
				$this->getAdditionalDataDf ($excludeAttr)
			:
		        parent::getAdditionalData ($excludeAttr)
		;
    }



    /**
     * $excludeAttr is optional array of attribute codes to
     * exclude them from additional data array
     *
     * @param array $excludeAttr
     * @return array
     */
    private function getAdditionalDataDf (array $excludeAttr = array())
    {
		/** @var array $data */
        $data = array();

		/** @var Mage_Catalog_Model_Product $product */
        $product = $this->getProduct();

		/** @var array $attributes */
        $attributes = $product->getAttributes();

        foreach ($attributes as $attribute) {
			/** @var Mage_Eav_Model_Attribute $attribute */

            if (
					$attribute->getIsVisibleOnFront()
				&&
					!in_array (
						$attribute->getAttributeCode()
						,
						$excludeAttr
					)
			) {

				/** @var Mage_Eav_Model_Entity_Attribute_Frontend_Abstract $frontend */
				$frontend = $attribute->getFrontend();

				/** @var mixed $value */
				$value = $frontend->getValue ($product);

                if (!$product->hasData($attribute->getAttributeCode())) {
                    continue;
                } elseif (Df_Core_Const::T_EMPTY === (string)$value) {
                    continue;
                } elseif (
						('price' === $attribute->getDataUsingMethod('frontend_input'))
					&&
						is_string($value)
				) {
                    $value = Mage::app()->getStore()->convertPrice($value, true);
                }

                if (is_string($value) && strlen($value)) {
                    $data[$attribute->getAttributeCode()] = array(
                        'label' => $attribute->getStoreLabel(),
                        'value' => $value,
                        'code'  => $attribute->getAttributeCode()
                    );
                }
            }
        }
        return $data;
    }


}