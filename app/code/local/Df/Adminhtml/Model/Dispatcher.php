<?php

class Df_Adminhtml_Model_Dispatcher extends Df_Core_Model_Abstract {



	/**
	 * @param Varien_Event_Observer $observer
	 * @return void
	 */
	public function adminhtml_block_html_before (
		Varien_Event_Observer $observer
	) {

		try {

			/**
			 * Для ускорения работы системы проверяем класс блока прямо здесь,
			 * а не в обработчике события.
			 *
			 * Это позволяет нам не создавать обработчики событий для каждого блока.
			 */

			/** @var Mage_Core_Block_Abstract $block  */
			$block = $observer->getData ('block');

			if ($block instanceof Mage_Adminhtml_Block_Widget_Form) {

				df_handle_event (
					Df_Adminhtml_Model_Handler_AdjustLabels_Form
						::getNameInMagentoFormat ()
					,
					Df_Core_Model_Event_CoreBlockAbstract_ToHtml_Before
						::getNameInMagentoFormat ()
					,
					$observer
				);

			}



			if ($block instanceof Mage_Adminhtml_Block_Widget_Grid) {

				df_handle_event (
					Df_Adminhtml_Model_Handler_AdjustLabels_Grid
						::getNameInMagentoFormat ()
					,
					Df_Core_Model_Event_CoreBlockAbstract_ToHtml_Before
						::getNameInMagentoFormat ()
					,
					$observer
				);

			}

		}

		catch (Exception $e) {
			df_handle_entry_point_exception ($e);
		}

	}




	/**
	 * @param Varien_Event_Observer $observer
	 * @return void
	 */
	public function adminhtml_sales_order_create_process_data_before (
		Varien_Event_Observer $observer
	) {

		try {
			/**
			 * @link http://magento-forum.ru/topic/2612/
			 */
			df_handle_event (
				Df_Adminhtml_Model_Handler_Sales_Order_Address_SetRegionName
					::getNameInMagentoFormat ()
				,
				Df_Adminhtml_Model_Event_Sales_Order_Create_Process_Data_Before
					::getNameInMagentoFormat ()
				,
				$observer
			);

		}

		catch (Exception $e) {
			df_handle_entry_point_exception ($e);
		}

	}





	/**
	 * @param Varien_Event_Observer $observer
	 * @return void
	 */
	public function controller_action_predispatch_adminhtml (
		Varien_Event_Observer $observer
	) {

		try {

			df_handle_event (
				Df_Adminhtml_Model_Handler_CorrectUsedModuleName
					::getNameInMagentoFormat ()
				,
				Df_Core_Model_Event_Controller_Action_Predispatch_Adminhtml
					::getNameInMagentoFormat ()
				,
				$observer
			);
		}

		catch (Exception $e) {
			df_handle_entry_point_exception ($e);
		}

	}



	/**
	 * @param Varien_Event_Observer $observer
	 * @return void
	 */
	public function core_block_abstract_to_html_before (
		Varien_Event_Observer $observer
	) {

		try {

			/**
			 * Для ускорения работы системы проверяем класс блока прямо здесь,
			 * а не в обработчике события.
			 *
			 * Это позволяет нам не создавать обработчики событий для каждого блока.
			 */

			/** @var Mage_Core_Block_Abstract $block  */
			$block = $observer->getData ('block');

			if ($block instanceof Mage_Adminhtml_Block_Widget_Button) {

				df_handle_event (
					Df_Adminhtml_Model_Handler_AdjustLabels_Button
						::getNameInMagentoFormat ()
					,
					Df_Core_Model_Event_CoreBlockAbstract_ToHtml_Before
						::getNameInMagentoFormat ()
					,
					$observer
				);

			}
		}

		catch (Exception $e) {
			df_handle_entry_point_exception ($e);
		}

	}




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Adminhtml_Model_Dispatcher';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}


