<?php


/**
 * @link http://magento-forum.ru/topic/2612/
 */
class Df_Adminhtml_Model_Handler_Sales_Order_Address_SetRegionName extends Df_Core_Model_Handler {


	/**
	 * Метод-обработчик события
	 *
	 * @override
	 * @return void
	 */
	public function handle () {

		/** @var array|null $order */
		$order = $this->getEvent()->getRequest()->getParam ('order');

		/**
		 * Как показывают отчёты от клиентов,
		 * иногда переменная $order в этом месте равна null
		 */
		if (!is_null ($order)) {

			$order [self::REQUEST_PARAM__BILLING_ADDRESS] =
				$this->processAddress (
					df_a ($order, self::REQUEST_PARAM__BILLING_ADDRESS)
				)
			;

			$order [self::REQUEST_PARAM__SHIPPING_ADDRESS] =
				$this->processAddress (
					df_a ($order, self::REQUEST_PARAM__SHIPPING_ADDRESS)
				)
			;

			$this->getEvent()->getRequest()->setParam ('order', $order);

			/**
			 * Приходится делать так,
			 * потому что $this->getRequest()->getPost('order')
			 * в методе Mage_Adminhtml_Sales_Order_CreateController::saveAction()
			 * обращается напрямую к $_POST
			 */
			$_POST ['order'] = $order;

		}
	}



	/**
	 * Объявляем метод заново, чтобы IDE знала настоящий тип объекта-события
	 *
	 * @return Df_Adminhtml_Model_Event_Sales_Order_Create_Process_Data_Before
	 */
	protected function getEvent () {

		/** @var Df_Adminhtml_Model_Event_Sales_Order_Create_Process_Data_Before $result  */
		$result = parent::getEvent();

		df_assert ($result instanceof Df_Adminhtml_Model_Event_Sales_Order_Create_Process_Data_Before);

		return $result;
	}





	/**
	 * Класс события (для валидации события)
	 *
	 * @override
	 * @return string
	 */
	protected function getEventClass () {

		/** @var string $result  */
		$result = Df_Adminhtml_Model_Event_Sales_Order_Create_Process_Data_Before::getClass();

		df_result_string ($result);

		return $result;
	}




	/**
	 * @param array|null $address
	 * @return array|null
	 */
	private function processAddress ($address) {

		/** @var array|null $result */
		$result = $address;


		/**
		 * Как ни странно, судя по отчетам $address — не всегда массив
		 * @link http://magento-forum.ru/topic/3283/
		 */
		if (is_array ($address)) {

			/** @var string|null $regionName */
			$regionName = df_a ($address, self::KEY__REGION_NAME);

			if (df_empty ($regionName)) {
				/** @var int $regionId */
				$regionId = intval (df_a ($address, self::KEY__REGION_ID));

				if (0 < $regionId) {

					/** @var Mage_Directory_Model_Region $region */
					$region = df_model (Df_Directory_Const::REGION_CLASS_MF);
					df_assert ($region instanceof Mage_Directory_Model_Region);

					$region->load ($regionId);
					df_assert ($regionId === intval ($region->getId()));

					$result [self::KEY__REGION_NAME] = $region->getName();
				}
			}
		}

		return $result;
	}




	const KEY__REGION_NAME = 'region';
	const KEY__REGION_ID = 'region_id';

	const REQUEST_PARAM__BILLING_ADDRESS = 'billing_address';
	const REQUEST_PARAM__SHIPPING_ADDRESS = 'shipping_address';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Adminhtml_Model_Handler_Sales_Order_Address_SetRegionName';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


