<?php

class Df_Adminhtml_Model_Handler_AdjustLabels_Grid extends Df_Core_Model_Handler {



	/**
	 * Метод-обработчик события
	 *
	 * @override
	 * @return void
	 */
	public function handle () {

		if (df_enabled (Df_Core_Feature::LOCALIZATION)) {

			foreach ($this->getBlockAsGrid()->getColumns() as $column) {

				/** @var Varien_Object $column */
				df_assert ($column instanceof Varien_Object);

				$column
					->setData (
						Df_Adminhtml_Const::GRID_COLUMN__PARAM__HEADER
						,
						df_text()->formatCase (
							df_convert_null_to_empty_string (
								$column->getData(
									Df_Adminhtml_Const::GRID_COLUMN__PARAM__HEADER
								)
							)
							,
							df_cfg()->admin()->_interface()->getGridLabelFont()->getLetterCase()
						)
					)
				;

			}
		}

	}




	/**
	 * @return Mage_Adminhtml_Block_Widget_Grid
	 */
	private function getBlockAsGrid () {

		/** @var Mage_Adminhtml_Block_Widget_Grid $result  */
		$result = $this->getEvent()->getBlock();

		df_assert ($result instanceof Mage_Adminhtml_Block_Widget_Grid);

		return $result;
	}





	/**
	 * Объявляем метод заново, чтобы IDE знала настоящий тип объекта-события
	 *
	 * @return Df_Core_Model_Event_CoreBlockAbstract_ToHtml_Before
	 */
	protected function getEvent () {

		/** @var Df_Core_Model_Event_CoreBlockAbstract_ToHtml_Before $result  */
		$result = parent::getEvent();

		df_assert ($result instanceof Df_Core_Model_Event_CoreBlockAbstract_ToHtml_Before);

		return $result;
	}





	/**
	 * Класс события (для валидации события)
	 *
	 * @override
	 * @return string
	 */
	protected function getEventClass () {

		/** @var string $result  */
		$result = Df_Core_Model_Event_CoreBlockAbstract_ToHtml_Before::getClass();

		df_result_string ($result);

		return $result;
	}




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Adminhtml_Model_Handler_AdjustLabels_Grid';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


