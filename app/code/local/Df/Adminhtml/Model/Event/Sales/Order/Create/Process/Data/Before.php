<?php

/**
 * Cообщение:		«adminhtml_sales_order_create_process_data_before»
 * Источник:		Mage_Adminhtml_Sales_Order_CreateController::_processActionData()
 * [code]
	$eventData = array(
		'order_create_model' => $this->_getOrderCreateModel(),
		'request_model'      => $this->getRequest(),
		'session'            => $this->_getSession(),
	);

	Mage::dispatchEvent('adminhtml_sales_order_create_process_data_before', $eventData);
 * [/code]
 *
 */
class Df_Adminhtml_Model_Event_Sales_Order_Create_Process_Data_Before extends Df_Core_Model_Event {


	/**
	 * @return Mage_Core_Controller_Request_Http
	 */
	public function getRequest () {

		/** @var Mage_Core_Controller_Request_Http $result  */
		$result = $this->getEventParam ('request_model');

		df_assert ($result instanceof Mage_Core_Controller_Request_Http);

		return $result;
	}



	/**
	 * @return string
	 */
	protected function getExpectedEventPrefix () {
		return self::EXPECTED_EVENT_PREFIX;
	}


	const EXPECTED_EVENT_PREFIX = 'adminhtml_sales_order_create_process_data_before';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Adminhtml_Model_Event_Sales_Order_Create_Process_Data_Before';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


