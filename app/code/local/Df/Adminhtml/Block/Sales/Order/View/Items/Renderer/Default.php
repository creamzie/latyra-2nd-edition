<?php

class Df_Adminhtml_Block_Sales_Order_View_Items_Renderer_Default extends Mage_Adminhtml_Block_Sales_Order_View_Items_Renderer_Default {

	/**
	 * @override
	 * @return string
	 */
	public function __ () {
		/** @var array $args  */
		$args = func_get_args();

		/** @var string $result  */
		$result = df_helper()->localization()->translation()->translateByParent ($args, $this);

		return $result;
	}



	/**
	 * @override
	 * @param  $basePrice
	 * @param  $price
	 * @param bool $strong
	 * @param string $separator
	 * @return string
	 */
    public function displayPrices($basePrice, $price, $strong = false, $separator = '<br />')
    {
	    return
				(
						df_enabled(Df_Core_Feature::LOCALIZATION)
					&&
						df_area (
							df_cfg ()->localization ()->translation()->frontend()->needHideDecimals()
							,
							df_cfg ()->localization ()->translation()->admin()->needHideDecimals()
						)
				)
			?
				$this->displayPricesDf ($basePrice, $price, $strong, $separator)
			:
				parent::displayPrices ($basePrice, $price, $strong, $separator)
		;
    }



	/**
	 * @param  $basePrice
	 * @param  $price
	 * @param bool $strong
	 * @param string $separator
	 * @return string
	 */
    private function displayPricesDf ($basePrice, $price, $strong = false, $separator = '<br />')
    {
	    return
			$this->displayRoundedPrices(
				$basePrice
				,
				$price
				,
				df_helper()->directory()->currency()->getPrecision ()
				,
				$strong
				,
				$separator
			)
		;
    }

}