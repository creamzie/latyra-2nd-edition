<?php

abstract class Df_Adminhtml_Block_Widget_Form_Container
	extends Mage_Adminhtml_Block_Widget_Form_Container {


	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getEntityClass();


	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getNewEntityTitle();




	/**
	 * @override
	 * @return Df_Adminhtml_Block_Widget_Form_Container
	 */
	public function __construct() {

		/**
		 * Родительский конструктор обязательно должен вызываться перед нашим кодом!
		 */
		parent::__construct();


		$this->_blockGroup = $this->getBlockClassPrefix();

		/**
		 * Используется нами для обозначения типа блока после косой черты, но до "edit".
		 * Для контроллера не используется.
		 */
		$this->_controller = $this->getBlockClassSuffix();

		/**
		 * А вот это используется для контроллера
		 */
		$this
			->setData (
				'form_action_url'
				,
				$this->getUrl (
					'*/*/save'
				)
			)
		;

		$this
			->addButton (
				'save_and_edit_button'
				,
				array (
					'label' => 'сохранить и остаться',
					'onclick' => 'editForm.submit(\''.$this->getSaveAndContinueUrl().'\')',
					'class'   => 'save'
				)
				,
				1
			)
		;
	}







	/**
	 * @override
	 * @return string
	 */
	public function getHeaderText() {

		/** @var string $result */
		$result =
				$this->getEntity()->getId()
			?
				$this->getEntity()->getName()
			:
				$this->getNewEntityTitle()
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	private function getBlockClassPrefix () {

		/** @var string $result  */
		$result =
			strtolower (
				df()->reflection()->getModuleName (
					get_class (
						$this
					)
				)
			)
		;

		df_result_string ($result);

		return $result;
	}





	/**
	 * @return string
	 */
	private function getBlockClassSuffix () {

		/** @var string $result  */
		$result =
			df_trim_suffix (
				df_array_last (
					explode (
						Df_Core_Model_Reflection::MODULE_NAME_SEPARATOR
						,
						df()->reflection()->getModelNameInMagentoFormat (
							get_class (
								$this
							)
						)
					)
				)
				,
				'_edit'
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return Df_Core_Model_Entity
	 */
	private function getEntity () {

		/** @var Df_Core_Model_Entity $result  */
		$result = Mage::registry ($this->getEntityClass());

		df_assert ($result instanceof Df_Core_Model_Entity);

		return $result;
	}




	/**
	 * @return string
	 */
	private function getSaveAndContinueUrl() {
		return
			$this->getUrl (
				'*/*/save'
				,
				array (
					'_current'  => true
					,
					'back' => 1
				)
			)
		;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Warehousing_Block_Admin_Warehouse_Edit';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


