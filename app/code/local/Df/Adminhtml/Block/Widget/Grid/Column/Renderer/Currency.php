<?php

class Df_Adminhtml_Block_Widget_Grid_Column_Renderer_Currency extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Currency {


	/**
	 * @override
	 * @return string
	 */
	public function __ () {
		/** @var array $args  */
		$args = func_get_args();

		/** @var string $result  */
		$result = df_helper()->localization()->translation()->translateByParent ($args, $this);

		return $result;
	}




	/**
	 * @override
	 * @param Varien_Object $row
	 * @return string
	 */
    public function render(Varien_Object $row)
    {
        return
				(
						df_enabled(Df_Core_Feature::LOCALIZATION)
					&&
						df_area (
							df_cfg ()->localization ()->translation()->frontend()->needHideDecimals()
							,
							df_cfg ()->localization ()->translation()->admin()->needHideDecimals()
						)
				)
			?
				$this->renderDf ($row)
			:
				parent::render ($row)
		;
    }




	/**
	 * @param Varien_Object $row
	 * @return string
	 */
    private function renderDf (Varien_Object $row)
    {
        if ($data = $row->getData($this->getColumn()->getIndex())) {
            $currency_code = $this->_getCurrencyCode($row);

            if (!$currency_code) {
                return $data;
            }

            $data = floatval($data) * $this->_getRate($row);
            $sign = (bool)(int)$this->getColumn()->getShowNumberSign() && ($data > 0) ? '+' : '';
            $data = sprintf("%f", $data);
            $data = df_zf_currency($currency_code)->toCurrency(
	            $data
                ,
	            array (
					"precision" => df_helper()->directory()->currency()->getPrecision ()
	            )
            );

            return $sign . $data;
        }
        return $this->getColumn()->getDefault();
    }




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Adminhtml_Block_Widget_Grid_Column_Renderer_Currency';
	}




	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}