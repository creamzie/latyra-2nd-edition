<?php

class Df_Varien_Block_Data_Form_Element_Map extends Df_Core_Block_Template {



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Varien_Block_Data_Form_Element_Map';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


