<?php

class Df_Payment_RedirectController extends Df_Payment_Controller_Abstract {



	/**
	 * Перенаправляет покупателя
	 * на внешнуюю для магазина платёжную страницу  платёжной системы
	 *
	 * @return void
	 */
    public function indexAction() {

		try {
			/**
			 * @todo
			 *
			 * Если покупатель перешёл сюда с сайта платёжной системы , а не со страницы нашего магазина,
			 * то нужно не перенаправлять покупателя назад на сайт платёжной системы,
			 * а позволить покупателю оплатить заказ другим способом.
			 *
			 * Покупатель мог перейти сюда с сайта платёжной системы, нажав кнопку "Назад" в браузере,
			 * или же нажав специализированную кнопку отмены операции на сайте платёжной системы
			 * (например, на платёжной странице LiqPay кнопка "В магазин"
			 * работает как javascript:history.back())/
			 */

			/** @var bool $isRedirected */
			$isRedirected =
				df_parse_boolean (
					/**
					 * Флаг Df_Checkout_Const::SESSION_PARAM__RM__REDIRECTED_TO_PAYMENT_SYSTEM
					 * предназначен для отслеживания возвращения покупателя
					 * с сайта платёжной системы без оплаты.
					 *
					 * Если этот флаг установлен — значит, покупатель был перенаправлен
					 * на сайт платёжной системы.
					 */
					df_helper()->checkout()->sessionSingleton()
						->getData (
							Df_Checkout_Const::SESSION_PARAM__RM__REDIRECTED_TO_PAYMENT_SYSTEM
						)
				)
			;

			if ($isRedirected) {
				$this->cancelOrder ();
				$this->restoreQuote ();

				df_helper()->checkout()->sessionSingleton()
					->unsetData (
						Df_Checkout_Const::SESSION_PARAM__RM__REDIRECTED_TO_PAYMENT_SYSTEM
					)
				;

				$this
					->_redirect (
						Df_Checkout_Const::URL__CHECKOUT
					)
				;
			}
			else {
				df_helper()->checkout()->sessionSingleton()
					->setData (
						Df_Checkout_Const::SESSION_PARAM__RM__REDIRECTED_TO_PAYMENT_SYSTEM
						,
						true
					)
				;

				$this->loadLayout();
				$this->renderLayout();
			}
		}
		catch (Exception $e) {

			/**
			 * Обратите внимание,
			 * что при возвращении на страницу Df_Checkout_Const::URL__CHECKOUT
			 * диагностическое сообщение надо добавлять в df_mage()->core()->session(),
			 * а не в  df_helper()->checkout()->sessionSingleton(),
			 * потому что сообщения сессии checkout
			 * не отображаются в стандартной теме на странице checkout/onepage
			 */
			rm_session()
				->addError (
					$e->getMessage()
				)
			;

			df_log_exception ($e);

			$this->cancelOrder ();
			$this->restoreQuote ();
			$this
				->_redirect (
					Df_Checkout_Const::URL__CHECKOUT
				)
			;
		}
    }


}



