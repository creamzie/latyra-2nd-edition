<?php

class Df_Payment_Model_ConfigManager_Var extends Df_Core_Model_Abstract {



	/**
	 * @return Df_Payment_Model_Method_Base
	 */
	public function getPaymentMethod () {

		/** @var Df_Payment_Model_Method_Base $result  */
		$result = $this->cfg (self::PARAM__PAYMENT_METHOD);

		df_assert ($result instanceof Df_Payment_Model_Method_Base);

		return $result;
	}



	/**
	 * @param string $key
	 * @param mixed $defaultValue [optional]
	 * @return mixed
	 */
	public function getValue ($key, $defaultValue = null) {

		df_param_string ($key, 0);

		/** @var string $result  */
		$result =
			$this->getStore()->getConfig (
				$this->preprocessKey (
					$key
				)
			)
		;

		if (df_empty ($result)) {
			$result = $defaultValue;
		}

		return $result;
	}




	/**
	 * @return Mage_Core_Model_Store
	 */
	protected function getStore () {

		/** @var Mage_Core_Model_Store $result  */
		$result = $this->cfg (self::PARAM__STORE);

		df_assert ($result instanceof Mage_Core_Model_Store);

		return $result;
	}




	/**
	 * @param string $key
	 * @return string
	 */
	protected function preprocessKey ($key) {

		df_param_string ($key, 0);

		/** @var string $result  */
		$result =
			df()->config()->implodeKey (
				array (
					self::KEY__BASE
					,
					$this->getPaymentMethod()->getRmId()
					,
					$key
				)
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM__PAYMENT_METHOD, Df_Payment_Model_Method_Base::getClass()
			)
			->validateClass (
				self::PARAM__STORE, 'Mage_Core_Model_Store'
			)
		;
	}


	const KEY__BASE = 'df_payment';


	const PARAM__PAYMENT_METHOD = 'payment_method';
	const PARAM__STORE = 'store';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Payment_Model_ConfigManager_Var';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


