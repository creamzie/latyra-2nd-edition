<?php

class Df_Payment_Model_Config_Area_Frontend
	extends Df_Payment_Model_Config_Area_Abstract {



	/**
	 * @return string
	 */
	public function getDescription () {

		/** @var string $result  */
		$result =
			$this->getVar (
				self::KEY__VAR__DESCRIPTION
				,
				Df_Core_Const::T_EMPTY
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	public function getTitle () {

		/** @var string $result  */
		$result =
			$this->getVar (
				self::KEY__VAR__TITLE
				,
				Df_Core_Const::T_EMPTY
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @override
	 * @return string
	 */
	protected function getAreaPrefix () {

		/** @var string $result  */
		$result = self::AREA_PREFIX;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @override
	 * @return array
	 */
	protected function getUncertainKeys () {

		/** @var array $result  */
		$result =
			array_merge (
				parent::getUncertainKeys()
				,
				array (
 					'allowspecific'
					,
					'sort_order'
					,
					'specificcountry'
					,
					'title'
				)
			)
		;

		df_result_array ($result);

		return $result;
	}





	const KEY__VAR__DESCRIPTION = 'description';
	const KEY__VAR__TITLE = 'title';

	const AREA_PREFIX = 'frontend';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Payment_Model_Config_Area_Frontend';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


