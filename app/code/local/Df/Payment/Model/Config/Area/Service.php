<?php

class Df_Payment_Model_Config_Area_Service
	extends Df_Payment_Model_Config_Area_Abstract {



	/**
	 * @param Mage_Sales_Model_Order $order
	 * @param double|float|string $amountInOrderCurrency
	 * @return Df_Core_Model_Money
	 */
	public function convertAmountFromOrderCurrencyToServiceCurrency (
		Mage_Sales_Model_Order $order
		,
		$amountInOrderCurrency
	) {

		/** @var double $amountInOrderCurrency  */
		$amountInOrderCurrency = (double)$amountInOrderCurrency;

		/** @var Df_Core_Model_Money $result  */
		/** @var Df_Core_Model_Money $result  */
		$result =
			df_model (
				Df_Core_Model_Money::getNameInMagentoFormat()
				,
				array (
					Df_Core_Model_Money::PARAM__AMOUNT =>
							(
									$order->getOrderCurrencyCode()
								===
									$this->getCurrency()->getCode ()
							)
						?
							$amountInOrderCurrency
						:
							$order->getOrderCurrency ()
								->convert (
									$amountInOrderCurrency
									,
									$this->getCurrency()
								)
				)
			)
		;

		df_assert ($result instanceof Df_Core_Model_Money);

		return $result;
	}



	/**
	 * @return array
	 */
	public function getAllowedCurrenciesAsOptionArray () {
	
		if (!isset ($this->_allowedCurrenciesAsOptionArray)) {


			/** @var array $currenciesAllowedInSystem  */
			$currenciesAllowedInSystem =
				Mage::app()->getLocale()->getOptionCurrencies()
			;

			df_assert_array ($currenciesAllowedInSystem);

	
			/** @var array $result  */
			$result = array ();


			if (!$this->getConstManager()->hasCurrencySetRestriction()) {
				$result = $currenciesAllowedInSystem;
			}
			else {

				foreach ($currenciesAllowedInSystem as $option) {

					/** @var array $option */
					df_assert_array ($option);

					/** @var string $code */
					$code = df_a ($option, Df_Admin_Model_Config_Source::OPTION_KEY__VALUE);

					df_assert_string ($code);

					if (in_array ($code, $this->getConstManager()->getAllowedCurrencyCodes())) {

						$result []= $option;

					}
				}

			}

	
			df_assert_array ($result);


	
			$this->_allowedCurrenciesAsOptionArray = $result;
		}
	
	
		df_result_array ($this->_allowedCurrenciesAsOptionArray);
	
		return $this->_allowedCurrenciesAsOptionArray;
	}
	
	
	/**
	* @var array
	*/
	private $_allowedCurrenciesAsOptionArray;
	
	




	/**
	 * @return array
	 */
	public function getAllowedLocalesAsOptionArray () {

		if (!isset ($this->_allowedLocalesAsOptionArray)) {

			/** @var array $result  */
			$result = array ();


			foreach ($this->getConstManager()->getAllowedLocaleCodes() as $code) {

				$result []=
					array (
						Df_Admin_Model_Config_Source::OPTION_KEY__VALUE => $code
						,
						Df_Admin_Model_Config_Source::OPTION_KEY__LABEL =>
							df_a (
								df_helper()->localization()->getLanguages()
								,
								df_helper()->localization()->locale()
									->getLanguageCodeByLocaleCode (
										$code
									)
							)
					)
				;

			}


			df_assert_array ($result);



			$this->_allowedLocalesAsOptionArray = $result;
		}


		df_result_array ($this->_allowedLocalesAsOptionArray);

		return $this->_allowedLocalesAsOptionArray;

	}


	/**
	* @var array
	*/
	private $_allowedLocalesAsOptionArray;




	
	
	
	/**
	 * Способы оплаты, предоставляемые данной платёжной системой
	 * @return array
	 */
	public function getAvailablePaymentMethodsAsOptionArray () {
	
		/** @var array $result  */
		$result = $this->getConstManager()->getAvailablePaymentMethodsAsOptionArray();

		df_result_array ($result);
	
		return $result;
	}



	/**
	 * @return string
	 */
	public function getCardPaymentAction () {

		/** @var string $result  */
		$result =
			$this->getVar (
				self::KEY__VAR__CARD_PAYMENT_ACTION
			)
		;


		df_result_string ($result);

		return $result;
	}
	

	
	
	
	
	/**
	 * @return Mage_Directory_Model_Currency
	 */
	public function getCurrency () {
	
		if (!isset ($this->_currency)) {
	
			/** @var Mage_Directory_Model_Currency $result  */
			$result = 
				df_model (
					Df_Directory_Const::CURRENCY_CLASS_MF
				)
			;
	
			df_assert ($result instanceof Mage_Directory_Model_Currency);

			$result->load ($this->getCurrencyCode());

	
			$this->_currency = $result;
		}
	
		df_assert ($this->_currency instanceof Mage_Directory_Model_Currency);
	
		return $this->_currency;
	}
	
	
	/**
	* @var Mage_Directory_Model_Currency
	*/
	private $_currency;

	



	/**
	 * @return string
	 */
	public function getCurrencyCode () {

		/** @var string $result  */
		$result =
			$this->getVar (
				self::KEY__VAR__CURRENCY
			)
		;

		df_result_string ($result);
		df_assert (!df_empty ($result));

		return $result;
	}




	/**
	 * @return string
	 */
	public function getCurrencyCodeInServiceFormat () {

		if (!isset ($this->_currencyCodeInServiceFormat)) {

			/** @var string $result  */
			$result =
				$this->translateCurrencyCode (
					$this->getCurrencyCode()
				)
			;

			df_assert_string ($result);

			$this->_currencyCodeInServiceFormat = $result;
		}


		df_result_string ($this->_currencyCodeInServiceFormat);

		return $this->_currencyCodeInServiceFormat;

	}


	/**
	* @var string
	*/
	private $_currencyCodeInServiceFormat;
	
	
	
	
	
	
	/**
	 * @return array
	 */
	public function getDisabledPaymentMethods () {
	
		if (!isset ($this->_disabledPaymentMethods)) {

			/** @var string $resultAsString  */
			$resultAsString =
				$this->getVar (
					self::KEY__VAR__PAYMENT_METHODS
				)
			;

			df_assert_string ($resultAsString);

	
			/** @var array $result  */
			$result =
				array_diff (
					df_column (
						$this->getAvailablePaymentMethodsAsOptionArray()
						,
						Df_Admin_Model_Config_Source::OPTION_KEY__VALUE
					)
					,
					$this->getSelectedPaymentMethods()
				)
			;
	
	
			df_assert_array ($result);
	
			$this->_disabledPaymentMethods = $result;
		}
	
	
		df_result_array ($this->_disabledPaymentMethods);
	
		return $this->_disabledPaymentMethods;
	}
	
	
	/**
	* @var array
	*/
	private $_disabledPaymentMethods;




	/**
	 * @return string
	 */
	public function getFeePayer () {

		if (!isset ($this->_feePayer)) {

			/** @var string $result  */
			$result =
				$this->getVar (
					self::KEY__VAR__FEE_PAYER
				)
			;


			df_assert_string ($result);

			$this->_feePayer = $result;
		}


		df_result_string ($this->_feePayer);

		return $this->_feePayer;

	}


	/**
	* @var string
	*/
	private $_feePayer;






	/**
	 * @return string
	 */
	public function getLocaleCode () {

		/** @var string $result  */
		$result =
			$this->getVar (
				self::KEY__VAR__LOCALE
			)
		;

		df_result_string ($result);
		df_assert (!df_empty ($result));

		return $result;
	}
	
	
	
	
	
	/**
	 * @return string
	 */
	public function getLocaleCodeInServiceFormat () {

		if (!isset ($this->_localeCodeInServiceFormat)) {

			/** @var string $result  */
			$result =
				$this->translateLocaleCode (
					$this->getLocaleCode()
				)
			;

			df_assert_string ($result);

			$this->_localeCodeInServiceFormat = $result;
		}


		df_result_string ($this->_localeCodeInServiceFormat);

		return $this->_localeCodeInServiceFormat;

	}


	/**
	* @var string
	*/
	private $_localeCodeInServiceFormat;




	/**
	 * @param Mage_Sales_Model_Order $order
	 * @return Df_Core_Model_Money
	 */
	public function getOrderAmountInServiceCurrency (Mage_Sales_Model_Order $order) {


		/** @var Df_Core_Model_Money $result  */
		$result =
			$this->convertAmountFromOrderCurrencyToServiceCurrency (
				$order
				,
				(double)(
						/**
						 * Если вызов данного метода происходит
						 * при формировании запроса к платёжной системе,
						 * то поле total_due заказа непусто, и используем его.
						 *
						 * Если же вызов данного метода происходит в других ситуациях
						 * (например, при просмотре формы ПД-4), то поле total_due пусто,
						 * и используем поле grand_total.
						 *
						 * Может, всегда использовать grand_total?
						 */
						!is_null ($order->getTotalDue ())
					?
						$order->getTotalDue ()
					:
						$order->getGrandTotal()
				)
			)
		;

		df_assert ($result instanceof Df_Core_Model_Money);

		return $result;
	}




	/**
	 * @return string
	 */
	public function getRequestPassword () {

		if (!isset ($this->_requestPassword)) {

			/** @var string $result  */
			$result =
				$this->decrypt (
					$this->getVar (
						self::KEY__VAR__REQUEST_PASSWORD
					)
				)
			;


			df_assert_string ($result);

			$this->_requestPassword = $result;
		}


		df_result_string ($this->_requestPassword);

		return $this->_requestPassword;

	}


	/**
	* @var string
	*/
	private $_requestPassword;





	/**
	 * @return string
	 */
	public function getResponsePassword () {

		if (!isset ($this->_responsePassword)) {

			/** @var string $result  */
			$result =
				$this->decrypt (
					$this->getVar (
						self::KEY__VAR__RESPONSE_PASSWORD
					)
				)
			;


			df_assert_string ($result);

			$this->_responsePassword = $result;
		}


		df_result_string ($this->_responsePassword);

		return $this->_responsePassword;

	}


	/**
	* @var string
	*/
	private $_responsePassword;




	/**
	 * @return string|null
	 */
	public function getSelectedPaymentMethod () {

		if (!isset ($this->_selectedPaymentMethod)) {

			/** @var string|null $result  */
			$result = $this->getVar (self::KEY__VAR__PAYMENT_METHOD);

			if (self::KEY__VAR__PAYMENT_METHOD__NO === $result) {
				$result = null;
			}

			if (!is_null ($result)) {
				df_assert_string ($result);
			}

			$this->_selectedPaymentMethod = $result;
		}

		if (!is_null ($this->_selectedPaymentMethod)) {
			df_result_string ($this->_selectedPaymentMethod);
		}

		return $this->_selectedPaymentMethod;

	}


	/**
	* @var string|null
	*/
	private $_selectedPaymentMethod;
	
	
	
	
	
	
	/**
	 * @return string|null
	 */
	public function getSelectedPaymentMethodCode () {
	
		if (!isset ($this->_selectedPaymentMethodCode)) {


			/** @var string|null $result  */
			$result =
				df_a (
					df_a (
						$this->getConstManager()->getAvailablePaymentMethodsAsCanonicalConfigArray ()
						,
						$this->getSelectedPaymentMethod()
						,
						array ()
					)
					,
					'code'
				)
			;

			if (!is_null ($result)) {
				df_assert_string ($result);
			}
	
			$this->_selectedPaymentMethodCode = $result;
		}
	
		if (!is_null ($this->_selectedPaymentMethodCode)) {
			df_result_string ($this->_selectedPaymentMethodCode);
		}
	
		return $this->_selectedPaymentMethodCode;
	}
	
	
	/**
	* @var string|null
	*/
	private $_selectedPaymentMethodCode;	
	
	
	

	
	
	
	/**
	 * @return array
	 */
	public function getSelectedPaymentMethods () {
	
		if (!isset ($this->_selectedPaymentMethods)) {

			/** @var string $resultAsString  */
			$resultAsString =
				$this->getVar (
					self::KEY__VAR__PAYMENT_METHODS
				)
			;

			df_assert_string ($resultAsString);

	
			/** @var array $result  */
			$result =
					(Df_Admin_Model_Config_Form_Element_Multiselect::RM__ALL === $resultAsString)
				?
					df_column (
						$this->getAvailablePaymentMethodsAsOptionArray()
						,
						Df_Admin_Model_Config_Source::OPTION_KEY__VALUE
					)
				:
					df_parse_csv (
						$resultAsString
					)
			;
	
	
			df_assert_array ($result);
	
			$this->_selectedPaymentMethods = $result;
		}
	
	
		df_result_array ($this->_selectedPaymentMethods);
	
		return $this->_selectedPaymentMethods;
	}
	
	
	/**
	* @var array
	*/
	private $_selectedPaymentMethods;





	/**
	 * Возвращает значения поля code способа оплаты.
	 * Данный метод имеет смысл, когда значения поля code — числовые
	 *
	 * @return array
	 */
	public function getSelectedPaymentMethodCodes () {
	
		if (!isset ($this->_selectedPaymentMethodCodes)) {
	
			/** @var array $result  */
			$result =
				df_column (
					array_intersect_key (
						$this->getConstManager()->getAvailablePaymentMethodsAsCanonicalConfigArray ()
						,
						array_flip ($this->getSelectedPaymentMethods ())
					)
					,
					'code'
				)
			;
	
	
			df_assert_array ($result);
	
			$this->_selectedPaymentMethodCodes = $result;
		}
	
	
		df_result_array ($this->_selectedPaymentMethodCodes);
	
		return $this->_selectedPaymentMethodCodes;
	}
	
	
	/**
	* @var array
	*/
	private $_selectedPaymentMethodCodes;
	




	/**
	 * @return string
	 */
	public function getShopId () {

		/** @var string $result  */
		$result =
			$this->getVar (
				self::KEY__VAR__SHOP_ID
			)
		;

		df_result_string ($result);
		df_assert (!df_empty ($result));

		return $result;
	}





	/**
	 * @return string
	 */
	public function getTransactionDescription () {

		/** @var string $result  */
		$result =
			$this->getVar (
				self::KEY__VAR__TRANSACTION_DESCRIPTION
				,
				Df_Core_Const::T_EMPTY
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	public function getUrlPaymentPage () {

		/** @var string $result  */
		$result =
			$this->getConstManager()->getUrl (
				self::KEY__CONST__URL__PAYMENT_PAGE
				,
				true
			)
		;

		df_assert_string ($result);

		return $result;
	}




	/**
	 * Работает ли модуль в тестовом режиме?
	 * Обратите внимание, что если в настройках отсутствует ключ «test»,
	 * то модуль будет всегда находиться в рабочем режиме.
	 *
	 * @return bool
	 */
    public function isTestMode () {

		/** @var bool $result */
        $result =
			$this->getVar (
				self::KEY__VAR__TEST
			)
		;

		$result =
				is_null ($result)
			?
				/**
				 * Eсли в настройках отсутствует ключ «test»,
				 * то модуль будет всегда находиться в рабочем режиме.
				 */
				false
			:
				$this->parseYesNo($result)
		;

		df_result_boolean ($result);

		return $result;
    }



	/**
	 * @override
	 * @return string
	 */
	protected function getAreaPrefix () {

		/** @var string $result  */
		$result = self::AREA_PREFIX;

		df_result_string ($result);

		return $result;
	}



	/**
	 * Переводит код валюты из стандарта Magento в стандарт платёжной системы.
	 *
	 * Обратите внимание, что конкретный платёжный модуль
	 * использует либо метод translateCurrencyCode, либо метод translateCurrencyCodeReversed,
	 * но никак не оба вместе!
	 *
	 * Например, модуль WebMoney использует метод translateCurrencyCodeReversed,
	 * потому что кодам в формате платёжной системы «U» и «D»
	 * соответствует единый код в формате Magento — «USD» — и использование translateCurrencyCode
	 * просто невозможно в силу необдозначности перевода «USD» (неясно, переводить в «U» или в «D»).
	 *
	 * @param string $currencyCodeInMagentoFormat
	 * @return string
	 */
	protected function translateCurrencyCode ($currencyCodeInMagentoFormat) {

		df_param_string ($currencyCodeInMagentoFormat, 0);

		/** @var string $result  */
		$result =
			$this->getConstManager()->translateCurrencyCode (
				$currencyCodeInMagentoFormat
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * Переводит код валюты из стандарта платёжной системы в стандарт Magento.
	 *
	 * Обратите внимание, что конкретный платёжный модуль
	 * использует либо метод translateCurrencyCode, либо метод translateCurrencyCodeReversed,
	 * но никак не оба вместе!
	 *
	 * Например, модуль WebMoney использует метод translateCurrencyCodeReversed,
	 * потому что кодам в формате платёжной системы «U» и «D»
	 * соответствует единый код в формате Magento — «USD» — и использование translateCurrencyCode
	 * просто невозможно в силу необдозначности перевода «USD» (неясно, переводить в «U» или в «D»).
	 *
	 * @param string $currencyCodeInPaymentSystemFormat
	 * @return string
	 */
	protected function translateCurrencyCodeReversed ($currencyCodeInPaymentSystemFormat) {

		df_param_string ($currencyCodeInPaymentSystemFormat, 0);

		/** @var string $result  */
		$result =
			$this->getConstManager()->translateCurrencyCodeReversed (
				$currencyCodeInPaymentSystemFormat
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * Переводит код локали из стандарта Magento в стандарт платёжной системы
	 * @param string $localeCodeInMagentoFormat
	 * @return string
	 */
	private function translateLocaleCode ($localeCodeInMagentoFormat) {

		df_param_string ($localeCodeInMagentoFormat, 0);

		/** @var string $result  */
		$result =
			$this->getConstManager()->translateLocaleCode (
				$localeCodeInMagentoFormat
			)
		;

		df_result_string ($result);

		return $result;
	}



	const KEY__CONST__URL__PAYMENT_PAGE = 'payment_page';


	const KEY__VAR__CARD_PAYMENT_ACTION = 'card_payment_action';
	const KEY__VAR__CURRENCY = 'currency';

	const KEY__VAR__FEE_PAYER = 'fee_payer';

	const KEY__VAR__LOCALE = 'payment_page_locale';

	const KEY__VAR__PAYMENT_METHOD = 'payment_method';
	const KEY__VAR__PAYMENT_METHOD__NO = 'no';

	const KEY__VAR__PAYMENT_METHODS = 'payment_methods';

	const KEY__VAR__REQUEST_PASSWORD = 'request_password';
	const KEY__VAR__RESPONSE_PASSWORD = 'response_password';
	const KEY__VAR__SHOP_ID = 'shop_id';
	const KEY__VAR__TEST = 'test';
	const KEY__VAR__TRANSACTION_DESCRIPTION = 'transaction_description';

	const AREA_PREFIX = 'payment_service';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Payment_Model_Config_Area_Service';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


