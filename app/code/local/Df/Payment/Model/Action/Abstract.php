<?php

abstract class Df_Payment_Model_Action_Abstract extends Df_Core_Model_Controller_Action {


	/**
	 * @abstract
	 * @return Mage_Sales_Model_Order
	 */
	abstract protected function getOrder ();



	/**
	 * @param string $configKey
	 * @param bool $isRequired [optional]
	 * @param string $defaultValue [optional]
	 * @return string
	 */
	protected function getConst (
		$configKey
		,
		$isRequired = true
		,
		$defaultValue = Df_Core_Const::T_EMPTY
	) {

		df_param_string ($configKey, 0);
		df_param_boolean ($isRequired, 1);
		df_param_string ($defaultValue, 2);


		/** @var string $key */
		$key =
			df()->config()->implodeKey (
				array (
					self::CONFIG_BASE
					,
					$configKey
				)
			)
		;

		df_assert_string ($key);


		/** @var string $result  */
		$result =
			$this->getPaymentMethod()->getConst (
				$key
				,
				false
			)
		;

		if (df_empty ($result)) {

			if ($isRequired) {
				df_error (
					sprintf (
						self::T__REQUIRED_KEY_IS_ABSENT
						,
						df()->reflection()->getModuleName (
							get_class (
								/**
								 * Раньше тут стояло $this,
								 * но $this->getPaymentMethod() вроде точнее
								 */
								$this->getPaymentMethod()
							)
						)
						,
						$key
					)
				);
			}

			$result = $defaultValue;
		}

		df_result_string ($result);

		return $result;
	}





	/**
	 * @return Df_Payment_Model_Method_WithRedirect
	 */
	protected function getPaymentMethod () {

		if (!isset ($this->_paymentMethod)) {


			/** @var Mage_Sales_Model_Order_Payment $payment  */
			$payment =  $this->getOrder()->getPayment();

			/**
			 * Не используем тут df_assert, потому что эта функция может быть отключена,
			 * а нам важно показать правильное диагностическое сообщение,
			 * а не «Call to a member function getMethodInstance() on a non-object»
			 */
			if (!($payment instanceof Mage_Sales_Model_Order_Payment)) {
				df_error (
					'Платёжная система прислала сообщение
					относительно заказа №«%s», который не предназначен для оплаты.'
					,
					$this->getOrder()->getIncrementId()
				);
			}


			/** @var Df_Payment_Model_Method_WithRedirect $result  */
			$result = $payment->getMethodInstance();

			df_assert (
				$result instanceof Df_Payment_Model_Method_WithRedirect
				,
				sprintf (
					'Платёжная система прислала сообщение
					относительно заказа №«%s»,
					который не предназначен для оплаты последством данной платёжной системы.'
					,
					$this->getOrder()->getIncrementId()
				)
			);

			$this->_paymentMethod = $result;
		}


		df_assert ($this->_paymentMethod instanceof Df_Payment_Model_Method_WithRedirect);

		return $this->_paymentMethod;

	}


	/**
	* @var Df_Payment_Model_Method_WithRedirect
	*/
	private $_paymentMethod;






	const CONFIG_BASE = 'request/confirmation';

	const T__REQUIRED_KEY_IS_ABSENT =
		'В файле «config.xml» модуля «%s» отсутствует требуемый ключ «%s».'
	;



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Payment_Model_Action_Abstract';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}

