<?php

class Df_Payment_Helper_Data extends Mage_Payment_Helper_Data {



    /**
	 * @param array $args
     * @return string
     */
    public function translateByParent (array $args) {

		/** @var string $result  */
        $result =
			df_helper()->localization()->translation()->translateByModule (
				$args, self::DF_PARENT_MODULE
			)
		;

		df_result_string ($result);

	    return $result;
    }




	/**
	 * @return Df_Payment_Helper_Url
	 */
	public function url () {

		/** @var Df_Payment_Helper_Url $result  */
		$result = Mage::helper (Df_Payment_Helper_Url::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Payment_Helper_Url);

		return $result;
	}






	const DF_PARENT_MODULE = 'Mage_Payment';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Payment_Helper_Data';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}