<?php

class Df_Client_Model_Message_Response_CreateAdminAccount
	extends Df_Core_Model_RemoteControl_Message_Response {



	/**
	 * @return string
	 */
	public function getUrlAdmin () {
		return $this->cfg (self::PARAM__URL__ADMIN);
	}



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__URL__ADMIN, new Df_Zf_Validate_String()
			)
		;
	}



	const PARAM__URL__ADMIN = 'url_admin';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Client_Model_Message_Response_CreateAdminAccount';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


