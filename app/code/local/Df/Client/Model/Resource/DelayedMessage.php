<?php

/**
 * Наследуемся от Mage_Core_Model_Mysql4_Abstract ради совместимости с Magento CE 1.4
 */
class Df_Client_Model_Resource_DelayedMessage extends Mage_Core_Model_Mysql4_Abstract {



	/**
	 * @param Mage_Core_Model_Resource_Setup $setup
	 * @return Df_Client_Model_Resource_Setup
	 */
	public function createTable (Mage_Core_Model_Resource_Setup $setup) {

		/** @var string $tableName */
		$tableName = $this->getTable (self::TABLE_NAME);

		/** @var string $fieldBody */
		$fieldBody = Df_Client_Model_DelayedMessage::PARAM__BODY;

		/** @var string $fieldClassName */
		$fieldClassName = Df_Client_Model_DelayedMessage::PARAM__CLASS_NAME;

		/** @var string $fieldCreationTime */
		$fieldCreationTime = Df_Client_Model_DelayedMessage::PARAM__CREATION_TIME;

		/** @var string $fieldLastRetryTime */
		$fieldLastRetryTime = Df_Client_Model_DelayedMessage::PARAM__LAST_RETRY_TIME;

		/** @var string $fieldMessageId */
		$fieldMessageId = Df_Client_Model_DelayedMessage::PARAM__MESSAGE_ID;

		/** @var string $fieldNumRetries*/
		$fieldNumRetries = Df_Client_Model_DelayedMessage::PARAM__NUM_RETRIES;

		/**
		 * Рад бы использовать $this->getConnection()->newTable(),
		 * но Российская сборка Magento должна быть совместима с Magento CE 1.4,
		 * которая не поддерживает подобный синтаксис.
		 */
		$setup
			->run ("
				CREATE TABLE IF NOT EXISTS `{$tableName}` (

					`{$fieldMessageId}` int(10) unsigned NOT NULL auto_increment

					,
					`{$fieldBody}` VARBINARY(1000) NOT NULL

					,
					`{$fieldClassName}` VARBINARY(100) NOT NULL

					,
					`{$fieldCreationTime}` TIMESTAMP NULL DEFAULT NULL

					,
					`{$fieldLastRetryTime}` TIMESTAMP NULL DEFAULT NULL

					,
					`{$fieldNumRetries}` int(4) unsigned NOT NULL DEFAULT 0

					,
					PRIMARY KEY  (`{$fieldMessageId}`)

				) ENGINE=InnoDB DEFAULT CHARSET=utf8;
			")
		;

		/**
		 * После изменения структуры базы данных надо удалить кэш,
		 * потому что Magento кэширует структуру базы данных
		 */
		Mage::app()->getCache()->clean();

		return $this;
	}




	/**
	 * @override
	 * @return void
	 */
    protected function _construct() {
		/**
		 * Нельзя вызывать parent::_construct(),
		 * потому что это метод в родительском классе — абстрактный.
		 */

        $this->_init (self::TABLE_NAME, Df_Client_Model_DelayedMessage::PARAM__MESSAGE_ID);
    }


	const TABLE_NAME = 'df_client/message';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Client_Model_Resource_DelayedMessage';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		return 'df_client/delayedMessage';
	}



	/**
	 * @return Df_Client_Model_Resource_DelayedMessage
	 */
	public static function i () {

		/** @var Df_Client_Model_Resource_DelayedMessage $result */
		static $result;

		if (!isset ($result)) {

			/** @var string $class */
			$class = get_class();

			/** @var Df_Client_Model_Resource_DelayedMessage $result  */
			$result = new $class;
		}

		return $result;
	}

}


