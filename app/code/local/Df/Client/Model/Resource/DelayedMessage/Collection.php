<?php

class Df_Client_Model_Resource_DelayedMessage_Collection
	extends Mage_Core_Model_Mysql4_Collection_Abstract {



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct();
		$this->_init (Df_Client_Model_DelayedMessage::getNameInMagentoFormat());
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Client_Model_Resource_DelayedMessage_Collection';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		return 'df_client/delayedMessage_collection';
	}
}

