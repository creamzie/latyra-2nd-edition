<?php

class Df_Client_Model_Handler_NotifyServerAboutInstallation extends Df_Core_Model_Handler {



	/**
	 * Метод-обработчик события
	 *
	 * @override
	 * @return void
	 */
	public function handle () {

		/** @var bool $isJustInstalled */
		$isJustInstalled =
			df_parse_boolean (
				df_mage()->core()->session()
					->getData (
						Df_Client_Model_Resource_Setup::FLAG__JUST_INSTALLED
					)
			)
		;

		df_assert_boolean ($isJustInstalled);


		if ($isJustInstalled) {

			/** @var Df_Client_Model_Message_Request_Installed $messageRequest */
			$messageRequest =
				df_model (
					Df_Client_Model_Message_Request_Installed::getNameInMagentoFormat()
				)
			;

			df_assert ($messageRequest instanceof Df_Client_Model_Message_Request_Installed);


			/** @var Df_Core_Model_RemoteControl_Message_Response $messageResponse */
			$messageResponse =
				Df_Client_Model_Request::sendStatic (
					$messageRequest
				)
			;

			df_assert ($messageResponse instanceof Df_Core_Model_RemoteControl_Message_Response);
		}
	}



	/**
	 * Класс события (для валидации события)
	 *
	 * @override
	 * @return string
	 */
	protected function getEventClass () {
		return Df_Core_Model_Event_Controller_Action_Postdispatch::getClass();
	}




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Client_Model_Handler_NotifyServerAboutInstallation';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


