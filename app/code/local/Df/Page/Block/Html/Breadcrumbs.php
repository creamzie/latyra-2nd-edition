<?php

class Df_Page_Block_Html_Breadcrumbs extends Mage_Page_Block_Html_Breadcrumbs {


	/**
	 * @override
	 * @return string
	 */
	public function __ () {
		/** @var array $args  */
		$args = func_get_args();

		/** @var string $result  */
		$result = df_helper()->localization()->translation()->translateByParent ($args, $this);

		return $result;
	}




	/**
	 * @override
	 * @return array
	 */
	public function getCacheKeyInfo() {

		/** @var array $result  */
		$result = parent::getCacheKeyInfo();

		if (
				df_module_enabled (Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->pageHtmlBreadcrumbs()
		) {
			$result =
				array_merge (
					$result
					,
					array (
						get_class ($this)
					)
					,
					$this->calculateCrumbCacheKeys()
				)
			;
		}

		df_result_array ($result);

		return $result;
	}




	/**
	 * @return array
	 */
	private function calculateCrumbCacheKeys () {

		/** @var array $result  */
		$result = array ();

		if (is_array ($this->_crumbs)) {
			$result = array_keys ($this->_crumbs);
		}

		df_result_array ($result);

		return $result;
	}





	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		if (
				df_module_enabled (Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->pageHtmlBreadcrumbs()
		) {
			$this->setData ('cache_lifetime', Df_Core_Block_Template::CACHE_LIFETIME_STANDARD);
		}
	}

}


