<?php

class Df_Page_Block_Html_Notices extends Mage_Page_Block_Html_Notices {


	/**
	 * @override
	 * @return string
	 */
	public function __ () {
		/** @var array $args  */
		$args = func_get_args();

		/** @var string $result  */
		$result = df_helper()->localization()->translation()->translateByParent ($args, $this);

		return $result;
	}



	/**
	 * @override
	 * @return array
	 */
	public function getCacheKeyInfo() {

		/** @var array $result  */
		$result =
			array_merge (
				parent::getCacheKeyInfo()
				,
				array (
					get_class ($this)
					,
					$this->displayNoscriptNotice()
					,
					$this->displayDemoNotice()
				)
			)
		;

		if (@class_exists ('Mage_Core_Helper_Cookie')) {
			$result []= df_mage()->core()->cookie()->isUserNotAllowSaveCookie();
		}

		df_result_array ($result);

		return $result;
	}



	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->setData ('cache_lifetime', Df_Core_Block_Template::CACHE_LIFETIME_STANDARD);
	}
}
