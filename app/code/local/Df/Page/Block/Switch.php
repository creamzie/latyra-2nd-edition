<?php

class Df_Page_Block_Switch extends Mage_Page_Block_Switch {


	/**
	 * @override
	 * @return string
	 */
	public function __ () {
		/** @var array $args  */
		$args = func_get_args();

		/** @var string $result  */
		$result = df_helper()->localization()->translation()->translateByParent ($args, $this);

		return $result;
	}



	/**
	 * @override
	 * @return array
	 */
	public function getCacheKeyInfo() {

		/** @var array $result  */
		$result = parent::getCacheKeyInfo();

		if (
				df_module_enabled (Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->pageSwitch()
			&&
				$this->isTemplateStandard()
		) {

			$result []= get_class ($this);

			if (self::TEMPLATE__STORES === $this->getTemplate()) {
				$result []= $this->getCurrentGroupId();
			}
			else {
				$result []= $this->getCurrentStoreId();
				$result []= $this->getRequest()->getRequestUri();
			}
		}

		df_result_array ($result);

		return $result;
	}




	/**
	 * Используем этот метод вместо установки cache_lifetime в конструкторе,
	 * потому что конструкторе мы ещё не знаем шаблон блока
	 *
	 * @override
	 * @return int|bool
	 */
	public function getCacheLifetime() {

		/** @var int|bool $result  */
		$result =
				(
						df_module_enabled (Df_Core_Module::SPEED)
					&&
						df_cfg()->speed()->blockCaching()->pageSwitch()
					&&
						$this->isTemplateStandard()
				)
			?
				Df_Core_Block_Template::CACHE_LIFETIME_STANDARD
			:
				parent::getCacheLifetime()
		;

		return $result;
	}





	/**
	 * @return bool
	 */
	private function isTemplateStandard () {
	
		if (!isset ($this->_templateStandard)) {
	
			/** @var bool $result  */
			$result = 
				in_array (
					$this->getTemplate()
					,
					array (
						self::TEMPLATE__FLAGS
						,
						self::TEMPLATE__LANGUAGES
						,
						self::TEMPLATE__STORES
					)
				)
			;
	
			df_assert_boolean ($result);
	
			$this->_templateStandard = $result;
		}
	
		df_result_boolean ($this->_templateStandard);
	
		return $this->_templateStandard;
	}
	
	
	/**
	* @var bool
	*/
	private $_templateStandard;		
	



	const TEMPLATE__FLAGS = 'page/switch/flags.phtml';
	const TEMPLATE__LANGUAGES = 'page/switch/languages.phtml';
	const TEMPLATE__STORES = 'page/switch/stores.phtml';

}


