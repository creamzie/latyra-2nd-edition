<?php

class Df_Page_Block_Js_Cookie extends Mage_Page_Block_Js_Cookie {


	/**
	 * @override
	 * @return array
	 */
	public function getCacheKeyInfo() {

		/** @var array $result  */
		$result =
			array_merge (
				parent::getCacheKeyInfo()
				,
				array (
					get_class ($this)
					,
					$this->getDomain()
					,
					$this->getPath()
				)
			)
		;

		df_result_array ($result);

		return $result;
	}



	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();

		$this
			->addData (
				array (
					'cache_lifetime' => Df_Core_Block_Template::CACHE_LIFETIME_STANDARD
				)
			)
		;
	}
}

