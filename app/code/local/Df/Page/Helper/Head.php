<?php

class Df_Page_Helper_Head extends Mage_Core_Helper_Abstract {


	/**
	 * @param string $type
	 * @param string $name
	 * @return bool
	 */
	public function needSkipItem ($type, $name) {

		/** @var bool $jqueryRemoveExtraneous */
		static $jqueryRemoveExtraneous;

		if (!isset ($jqueryRemoveExtraneous)) {
			$jqueryRemoveExtraneous =
					(
							df_is_admin()
						&&
							df_cfg()->admin()->jquery()->needRemoveExtraneous()
						&&
							(
									Df_Admin_Model_Config_Source_JqueryLoadMode::VALUE__NO_LOAD
								!==
									df_cfg()->admin()->jquery()->getLoadMode()
							)
					)
				||
					(
							!df_is_admin()
						&&
							df_cfg()->tweaks()->jquery()->needRemoveExtraneous()
						&&
							(
									Df_Admin_Model_Config_Source_JqueryLoadMode::VALUE__NO_LOAD
								!==
									df_cfg()->tweaks()->jquery()->getLoadMode()
							)
					)

			;
		}

		/** @var bool $result  */
		$result =
				$jqueryRemoveExtraneous
			&&
				('js' === $type)
			&&
				/**
				 * Обратите внимание, что Российская сборка Magento добавляет на страницу
				 * библиотеку jQuery не посредством addItem, а более низкоуровневыми методами
				 */
				(
						$this->isItJQuery ($name)
					||
						$this->isItJQueryNoConflict ($name)
				)
		;

		df_result_boolean ($result);

		return $result;
	}




	/**
	 * @param string $scriptName
	 * @return bool
	 */
	private function isItJQuery ($scriptName) {
		/**
		 * jquery.js
		 * jquery-1.8.3.js
		 * jquery-1.8.3.min.js
		 */

		/** @var string $pattern */
		$pattern = '#jquery(\-\d+\.\d+\.\d+)?(\.min)?\.js#ui';

		/** @var array $matches */
		$matches = array ();

		$result = (1 === preg_match ($pattern, $scriptName, $matches));

		df_result_boolean ($result);

		return $result;
	}


	/**
	 * @param string $scriptName
	 * @return bool
	 */
	private function isItJQueryNoConflict ($scriptName) {
		/**
		 * noconflict.js
		 */
		$result =
			(
					false
				!==
					mb_strpos (
						mb_strtolower ($scriptName)
						,
						mb_strtolower ('noconflict.js')
					)
			)
		;

		df_result_boolean ($result);

		return $result;
	}




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Page_Helper_Head';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}