<?php

class Df_Install_Model_Config extends Mage_Install_Model_Config {


	/**
	 * @return Varien_Object[]
	 */
	public function getWizardSteps () {

		/** @var array $result  */
		$result = parent::getWizardSteps();

		/** @var int[] $indicesToRemove */
		$indicesToRemove = array ();

		foreach ($result as $index => $step) {
			/** @var int $index */
			df_assert_integer ($index);

			/** @var Varien_Object $step */
			df_assert ($step instanceof Varien_Object);

			if ('true' === $step->getData ('remove')) {
				$indicesToRemove []= $index;
			}
		}


		$result = array_diff_key ($result, array_flip ($indicesToRemove));

		df_result_array ($result);

		return $result;
	}
}


