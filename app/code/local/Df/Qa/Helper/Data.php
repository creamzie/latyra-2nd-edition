<?php

class Df_Qa_Helper_Data extends Mage_Core_Helper_Abstract {

	/**
	 * @param mixed $value
	 * @param bool $addQuotes [optional]
	 * @return string
	 */
	public function convertValueToDebugString ($value, $addQuotes = true) {

		/** @var string $result  */
		$result = Df_Core_Const::T_EMPTY;

		if (is_object ($value)) {
			$result =
				sprintf (
					'объект класса %s'
					,
					get_class ($value)
				)
			;
		}
		else if (is_array($value)) {
			$result =
				sprintf (
					'массив с %d элементами'
					,
					count ($value)
				)
			;
		}
		else {
			$result = df_string ($value);
		}

		if ($addQuotes) {
			$result =
				df_quote (
					$result
				)
			;
		}

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return Df_Qa_Helper_Method
	 */
	public function method () {

		/** @var Df_Qa_Helper_Method $result  */
		static $result;

		if (!isset ($result)) {
			$result =
				Mage::helper (
					/**
					 * Обратите внимание, что применение метода getNameInMagentoFormat()
					 * привело бы к рекурсии!
					 */
					'df_qa/method'
				)
			;
		}

		return $result;
	}

	/**
	 * Обратите внимание, что применение метода getNameInMagentoFormat()
	 * привело бы к рекурсии!
	 */
}


