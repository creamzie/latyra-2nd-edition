<?php

abstract class Df_Qa_Model_Message_Failure extends Df_Qa_Model_Message {

	/**
	 * @abstract
	 * @return string
	 */
	abstract public function getFailureMessage();


	/**
	 * @abstract
	 * @return array
	 */
	abstract protected function getTrace();


	
	/**
	 * @return string
	 */
	public function getTraceAsText () {
	
		/** @var string $result  */
		$result =
			implode (
				"\n\n\n\n"
				, 
				$this->getTraceAsArray()
			)
		;
		
		df_result_string ($result);
	
		return $result;
	}



	/**
	 * @return bool
	 */
	private function showCodeContext () {
		return $this->cfg (self::PARAM__SHOW_CODE_CONTEXT, true);
	}




	/**
	 * @return int
	 */
	protected function getStackLevel () {
		return $this->cfg (self::PARAM__STACK_LEVEL, 0);
	}



	/**
	 * @override
	 * @return string
	 */
	protected function getTemplate() {
		return 'df/qa/log/failure.phtml';
	}



	

	/**
	 * @return Df_Qa_Model_Debug_Execution_State[]
	 */
	private function getTraceAsArray () {
	
		if (!isset ($this->_traceAsArray)) {
	
			/** @var Df_Qa_Model_Debug_Execution_State[] $result  */
			$result = array ();

			/** @var array $trace */
			$trace =
				array_slice (
					$this->getTrace()
					,
					$this->getStackLevel()
				)
			;


			/** @var Df_Qa_Model_Debug_Execution_State|null $previousExecutionState  */
			$previousExecutionState = null;

			foreach ($trace as $executionState) {
				/** @var array $executionState */

				/** @var Df_Qa_Model_Debug_Execution_State $state */
				$state =
					df_model (
						Df_Qa_Model_Debug_Execution_State::getNameInMagentoFormat()
						,
						array_merge (
							$executionState
							,
							array (
								Df_Qa_Model_Debug_Execution_State
									::PARAM__SHOW_CODE_CONTEXT => $this->showCodeContext()
								,
								Df_Qa_Model_Debug_Execution_State
									::PARAM__STATE_PREVIOUS => $previousExecutionState
							)
						)
					)
				;

				$result []= $state;

				if (!is_null ($previousExecutionState)) {
					$previousExecutionState
						->setData (
							Df_Qa_Model_Debug_Execution_State::PARAM__STATE_NEXT
							,
							$state
						)
					;
				}

				$previousExecutionState = $state;
			}

			df_assert_array ($result);
	
			$this->_traceAsArray = $result;
		}

		df_result_array ($this->_traceAsArray);
	
		return $this->_traceAsArray;
	}
	
	
	/**
	* @var Df_Qa_Model_Debug_Execution_State[]
	*/
	private $_traceAsArray;	
	



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__SHOW_CODE_CONTEXT, new Df_Zf_Validate_Boolean(), $isRequired = false
			)
			->addValidator (
				self::PARAM__STACK_LEVEL, new Df_Zf_Validate_Int(), $isRequired = false
			)
		;
	}


	const PARAM__SHOW_CODE_CONTEXT = 'show_code_context';
	const PARAM__STACK_LEVEL = 'stack_level';




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Qa_Model_Message_Failure';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}

