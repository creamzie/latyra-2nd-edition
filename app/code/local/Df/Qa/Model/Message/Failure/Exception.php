<?php

class Df_Qa_Model_Message_Failure_Exception extends Df_Qa_Model_Message_Failure {


	/**
	 * @override
	 * @return string
	 */
	public function getFailureMessage () {
		return $this->getException()->getMessage();
	}


	/**
	 * @override
	 * @return array
	 */
	protected function getTrace() {
		return $this->getException()->getTrace();
	}


	/**
	 * @return Exception
	 */
	private function getException () {
		return $this->cfg (self::PARAM__EXCEPTION);
	}



	/**
	 * @override
	 * @return int
	 */
	protected function getStackLevel () {

		/** @var int $result  */
		$result = parent::getStackLevel();

		if ($this->getException() instanceof Df_Core_Exception) {

			/** @var Df_Core_Exception $exception */
			$exception = $this->getException();

			$result = $exception->getStackLevelsCountToSkip();
		}

		df_result_integer ($result);

		return $result;
	}



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM__EXCEPTION, 'Exception'
			)
		;
	}



	const PARAM__EXCEPTION = 'exception';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Qa_Model_Message_Failure_Exception';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}

