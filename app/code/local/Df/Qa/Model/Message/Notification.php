<?php

class Df_Qa_Model_Message_Notification extends Df_Qa_Model_Message {


	/**
	 * @return string
	 */
	public function getNotification () {
		return $this->cfg (self::PARAM__NOTIFICATION);
	}




	/**
	 * @override
	 * @return string
	 */
	protected function getTemplate() {
		return 'df/qa/log/notification.phtml';
	}




	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__NOTIFICATION, new Df_Zf_Validate_String()
			)
		;
	}



	const PARAM__NOTIFICATION = 'notification';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Qa_Model_Message_Notification';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}

