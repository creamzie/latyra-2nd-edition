<?php

class Df_Qa_Model_Shutdown extends Df_Core_Model_Abstract {


	/**
	 */
	public function process () {

		/** @var Df_Qa_Model_Message_Failure_Error $error */
		$error =
			df_model (
				Df_Qa_Model_Message_Failure_Error::getNameInMagentoFormat()
				,
				array (
					Df_Qa_Model_Message_Failure_Error::PARAM__NEED_LOG_TO_FILE => true
					,
					Df_Qa_Model_Message_Failure_Error::PARAM__NEED_NOTIFY_DEVELOPER => true
				)
			)
		;

		df_assert ($error instanceof Df_Qa_Model_Message_Failure_Error);

		if ($error->isFatal()) {
			$error->log();
		}
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Qa_Model_Shutdown';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



	/**
	 * @static
	 */
	public static function processStatic () {

		/** @var Df_Qa_Model_Shutdown $instance */
		$instance = df_model (Df_Qa_Model_Shutdown::getNameInMagentoFormat());

		df_assert ($instance instanceof Df_Qa_Model_Shutdown);

		$instance->process();
	}
}


